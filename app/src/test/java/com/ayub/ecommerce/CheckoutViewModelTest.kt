package com.ayub.ecommerce

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.SavedStateHandle
import com.ayub.core.datasource.local.room.entity.Cart
import com.ayub.core.repository.IRepository
import com.ayub.ecommerce.screens.main.checkout.CheckoutFragment
import com.ayub.ecommerce.screens.main.checkout.CheckoutViewModel
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.kotlin.mock

@RunWith(JUnit4::class)
class CheckoutViewModelTest {
    private lateinit var checkoutViewModel: CheckoutViewModel
    private lateinit var repo: IRepository
    private lateinit var savedStateHandle: SavedStateHandle

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        repo = mock()

        val cartList = arrayListOf(
            dummyDataCart
        )

        savedStateHandle = SavedStateHandle(
            mapOf(
                CheckoutFragment.ARG_DATA to cartList
            )
        )

        checkoutViewModel = CheckoutViewModel(repo, savedStateHandle)
    }

    @Test
    fun getData() = runTest {
        val cartList = arrayListOf(
            dummyDataCart
        )

        checkoutViewModel.setData(cartList)

        checkoutViewModel.listData.observeForever {
            assertEquals(cartList, it)
        }
    }

    companion object {
        val dummyDataCart = Cart(
            productId = "17b4714d-527a-4be2-84e2-e4c37c2b3292",
            productName = "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray",
            productPrice = 24499000,
            image = "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/0a49c399-cf6b-47f5-91c9-8cbd0b86462d.jpg",
            brand = "Asus",
            description = "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray [AMD Ryzen™ 7 6800H / NVIDIA® GeForce RTX™ 3060 / 8G*2 / 512GB / 17.3inch / WIN11 / OHS]\n\nCPU : AMD Ryzen™ 7 6800H Mobile Processor (8-core/16-thread, 20MB cache, up to 4.7 GHz max boost)\nGPU : NVIDIA® GeForce RTX™ 3060 Laptop GPU\nGraphics Memory : 6GB GDDR6\nDiscrete/Optimus : MUX Switch + Optimus\nTGP ROG Boost : 1752MHz* at 140W (1702MHz Boost Clock+50MHz OC, 115W+25W Dynamic Boost)\nPanel : 17.3-inch FHD (1920 x 1080) 16:9 360Hz IPS-level 300nits sRGB % 100.00%",
            store = "AsusStore",
            sale = 12,
            stock = 2,
            totalRating = 7,
            totalReview = 5,
            totalSatisfaction = 100,
            productRating = (5.0).toFloat(),
            variantName = "RAM 32GB",
            variantPrice = 1000000
        )
    }
}
