package com.ayub.ecommerce

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.liveData
import com.ayub.core.datasource.network.request_response.BaseResponse
import com.ayub.core.datasource.network.request_response.PaymentTypeItem
import com.ayub.core.repository.IRepository
import com.ayub.ecommerce.screens.main.payment.PaymentViewModel
import junit.framework.TestCase
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class PaymentViewModelTest {
    private lateinit var paymentViewModel: PaymentViewModel
    private lateinit var repo: IRepository

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        repo = mock()
        paymentViewModel = PaymentViewModel(repo)
    }

    @Test
    fun getPayment() {
        val expectedResult = liveData<BaseResponse<List<PaymentTypeItem?>>> {
            BaseResponse.Success(
                dummyPaymentMethod
            )
        }

        whenever(repo.getPayment()).thenReturn(expectedResult)

        val actualResult = repo.getPayment()
        TestCase.assertEquals(expectedResult, actualResult)
    }

    companion object {
        val dummyPaymentMethod = listOf(
            PaymentTypeItem(
                title = "Transfer Virtual Account",
                item = listOf(
                    PaymentTypeItem.PaymentItem(
                        label = "BNI Virtual Account",
                        image = "bni.com",
                        status = true
                    ),
                    PaymentTypeItem.PaymentItem(
                        label = "BRI Virtual Account",
                        image = "bri.com",
                        status = true
                    ),
                )
            ),
            PaymentTypeItem(
                title = "Transfer Bank",
                item = listOf(
                    PaymentTypeItem.PaymentItem(
                        label = "Bank BCA",
                        image = "bca.com",
                        status = true
                    ),
                    PaymentTypeItem.PaymentItem(
                        label = "Bank BNI",
                        image = "bni.com",
                        status = true
                    ),
                )
            ),
            PaymentTypeItem(
                title = "Pembayaran Instan",
                item = listOf(
                    PaymentTypeItem.PaymentItem(
                        label = "GoPay",
                        image = "gopay.com",
                        status = true
                    ),
                    PaymentTypeItem.PaymentItem(
                        label = "OVO",
                        image = "ovo.com",
                        status = true
                    ),
                )
            ),
        )
    }
}
