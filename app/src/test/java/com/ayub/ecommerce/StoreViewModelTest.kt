package com.ayub.ecommerce

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.ayub.core.datasource.network.request_response.ProductRequest
import com.ayub.core.repository.IRepository
import com.ayub.ecommerce.screens.main.store.StoreViewModel
import junit.framework.TestCase.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.kotlin.mock

@RunWith(JUnit4::class)
class StoreViewModelTest {
    private lateinit var storeViewModel: StoreViewModel
    private lateinit var repo: IRepository

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        repo = mock()
        storeViewModel = StoreViewModel(repo)
    }

    @Test
    fun updateFilter() {
        val brand = "SomeBrand"
        val lowest = "100"
        val highest = "500"
        val sort = "ASC"

        val expected =
            ProductRequest(sort = sort, brand = brand, lowest = lowest, highest = highest)

        storeViewModel.updateFilter(sort, brand, lowest, highest)

        assertEquals(expected, storeViewModel.filteredProduct.value)
    }

    @Test
    fun updateSearch() {
        val search = ""
        val expected = ProductRequest(search)

        storeViewModel.updateSearch(search)

        assertEquals(expected, storeViewModel.filteredProduct.value)
    }

    @Test
    fun resetParam() {
        storeViewModel.resetParam()

        val expected = ProductRequest()
        assertEquals(expected, storeViewModel.filteredProduct.value)
    }
}
