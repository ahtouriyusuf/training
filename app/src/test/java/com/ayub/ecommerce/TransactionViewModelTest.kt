package com.ayub.ecommerce

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.liveData
import com.ayub.core.datasource.network.request_response.BaseResponse
import com.ayub.core.datasource.network.request_response.ItemsItem
import com.ayub.core.datasource.network.request_response.TransactionData
import com.ayub.core.repository.IRepository
import com.ayub.ecommerce.screens.main.transaction.TransactionViewModel
import com.ayub.ecommerce.utils.MainDispatcherRule
import junit.framework.TestCase
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class TransactionViewModelTest {

    private lateinit var transactionViewModel: TransactionViewModel
    private lateinit var repo: IRepository

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Before
    fun setup() {
        repo = mock()
        transactionViewModel = TransactionViewModel(repo)
    }

    @Test
    fun getTransaction() = runTest {
        val expectedResult = liveData<BaseResponse<List<TransactionData?>>> {
            BaseResponse.Success(
                listOf(dummyTransactionData)
            )
        }

        whenever(repo.transaction()).thenReturn(expectedResult)

        val actualResult = repo.transaction()
        assertEquals(expectedResult, actualResult)
    }

    companion object {
        val dummyTransactionData = TransactionData(
            "09 Jun 2023",
            "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/0a49c399-cf6b-47f5-91c9-8cbd0b86462d.jpg",
            48998000,
            "LGTM",
            4,
            "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray",
            "8cad85b1-a28f-42d8-9479-72ce4b7f3c7d",
            "Bank BCA",
            "09:05",
            listOf(
                ItemsItem(
                    2,
                    "bee98108-660c-4ac0-97d3-63cdc1492f53",
                    "RAM 16GB"
                )
            ),
            true,
        )
    }
}
