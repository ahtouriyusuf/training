package com.ayub.ecommerce

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ayub.core.datasource.local.room.entity.WishlistProduct
import com.ayub.core.helper.toCart
import com.ayub.core.repository.IRepository
import com.ayub.ecommerce.screens.main.wishlist.WishlistViewModel
import com.ayub.ecommerce.utils.MainDispatcherRule
import junit.framework.TestCase
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class WishlistViewModelTest {

    private lateinit var wishlistViewModel: WishlistViewModel
    private lateinit var repo: IRepository
    private lateinit var wishlistData: LiveData<List<WishlistProduct>>

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        repo = mock()
        wishlistViewModel = WishlistViewModel(repo)
    }

    @Test
    fun getWishlistData() = runTest {
        wishlistData = MutableLiveData()

        val expectedWishlist: List<WishlistProduct> = listOf(dummyDataWishlist)

        whenever(repo.getWishlistProducts()).thenReturn(wishlistData)

        val actualWishlistData = repo.getWishlistProducts()

        (wishlistData as MutableLiveData<List<WishlistProduct>>).value = expectedWishlist

        val actualWishlist = actualWishlistData.value

        TestCase.assertEquals(expectedWishlist, actualWishlist)
    }

    @Test
    fun deleteWishlist() = runTest {
        whenever(repo.deleteFromWishlist(dummyDataWishlist)).thenReturn(Unit)
        val actual = wishlistViewModel.deleteWishlist(dummyDataWishlist)
        TestCase.assertEquals(Unit, actual)
    }

    @Test
    fun addToCart() = runTest {
        whenever(repo.addToCart(dummyDataWishlist.toCart())).thenReturn(Unit)
        val actual = wishlistViewModel.addToCart(dummyDataWishlist)
        TestCase.assertEquals(Unit, actual)
    }

    @Test
    fun getProductFromCart() = runTest {
        whenever(repo.getCartProductById(dummyDataWishlist.productId)).thenReturn(dummyDataWishlist.toCart())
        val actual = wishlistViewModel.getProductFromCart(dummyDataWishlist.productId)
        TestCase.assertEquals(dummyDataWishlist.toCart(), actual)
    }

    @Test
    fun updateProductQuantity() = runTest {
        whenever(repo.updateCartItemQuantity(dummyDataWishlist.productId, 2)).thenReturn(Unit)
        val actual = wishlistViewModel.updateProductQuantity(dummyDataWishlist.productId, 2)
        TestCase.assertEquals(Unit, actual)
    }

    companion object {
        val dummyDataWishlist = WishlistProduct(
            productId = "17b4714d-527a-4be2-84e2-e4c37c2b3292",
            productName = "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray",
            productPrice = 24499000,
            image = "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/0a49c399-cf6b-47f5-91c9-8cbd0b86462d.jpg",
            brand = "Asus",
            description = "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray [AMD Ryzen™ 7 6800H / NVIDIA® GeForce RTX™ 3060 / 8G*2 / 512GB / 17.3inch / WIN11 / OHS]\n\nCPU : AMD Ryzen™ 7 6800H Mobile Processor (8-core/16-thread, 20MB cache, up to 4.7 GHz max boost)\nGPU : NVIDIA® GeForce RTX™ 3060 Laptop GPU\nGraphics Memory : 6GB GDDR6\nDiscrete/Optimus : MUX Switch + Optimus\nTGP ROG Boost : 1752MHz* at 140W (1702MHz Boost Clock+50MHz OC, 115W+25W Dynamic Boost)\nPanel : 17.3-inch FHD (1920 x 1080) 16:9 360Hz IPS-level 300nits sRGB % 100.00%",
            store = "AsusStore",
            sale = 12,
            stock = 2,
            totalRating = 7,
            totalReview = 5,
            totalSatisfaction = 100,
            productRating = (5.0).toFloat(),
            variantName = "RAM 32GB",
            variantPrice = 1000000
        )
    }
}
