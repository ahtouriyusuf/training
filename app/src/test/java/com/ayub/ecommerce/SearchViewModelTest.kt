package com.ayub.ecommerce

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.ayub.core.datasource.network.request_response.BaseResponse
import com.ayub.core.datasource.network.request_response.SearchProductResponse
import com.ayub.core.repository.IRepository
import com.ayub.ecommerce.screens.main.store.search.SearchViewModel
import junit.framework.TestCase
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class SearchViewModelTest {
    private lateinit var searchViewModel: SearchViewModel
    private lateinit var repo: IRepository

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        repo = mock()
        searchViewModel = SearchViewModel(repo)
    }

    @Test
    fun getSearchData() {
        val liveData = MutableLiveData<BaseResponse<SearchProductResponse>>()

        whenever(repo.searchProducts("")).thenReturn(liveData)

        val mockedResponse = BaseResponse.Success(dummySearchResponse)
        liveData.value = mockedResponse

        searchViewModel.searchProducts("")

        val observedValue = searchViewModel.searchProducts("").value
        val expectedValue = mockedResponse

        TestCase.assertEquals(expectedValue, observedValue)
    }

    companion object {
        val dummySearchResponse = SearchProductResponse(
            200,
            message = "OK",
            data = listOf(
                "Lenovo Legion 3",
                "Lenovo Legion 5",
                "Lenovo Legion 7",
                "Lenovo Ideapad 3",
                "Lenovo Ideapad 5",
                "Lenovo Ideapad 7"
            )
        )
    }
}
