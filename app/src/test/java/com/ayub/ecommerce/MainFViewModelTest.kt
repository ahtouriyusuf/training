package com.ayub.ecommerce

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.SavedStateHandle
import com.ayub.core.repository.IRepository
import com.ayub.ecommerce.screens.main.MainFViewModel
import com.ayub.ecommerce.screens.main.MainFragment
import com.ayub.ecommerce.utils.MainDispatcherRule
import junit.framework.TestCase
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class MainFViewModelTest {
    private lateinit var mainFViewModel: MainFViewModel
    private lateinit var repo: IRepository
    private lateinit var savedStateHandle: SavedStateHandle

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        repo = mock()
        savedStateHandle = SavedStateHandle(mapOf(MainFragment.IS_NEW_DATA_KEY to false))
        mainFViewModel = MainFViewModel(repo, savedStateHandle)
    }

    @Test
    fun getUsername() = runTest {
        whenever(repo.getUsername()).thenReturn("Ayub")
        val actual = mainFViewModel.getUsername()
        TestCase.assertEquals("Ayub", actual)
    }
}
