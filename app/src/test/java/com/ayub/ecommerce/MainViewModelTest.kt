package com.ayub.ecommerce

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.ayub.core.repository.IRepository
import com.ayub.ecommerce.screens.MainViewModel
import com.ayub.ecommerce.utils.MainDispatcherRule
import junit.framework.TestCase
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class MainViewModelTest {

    private lateinit var mainViewModel: MainViewModel
    private lateinit var repo: IRepository

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        repo = mock()
        mainViewModel = MainViewModel(repo)
    }

    @Test
    fun getAccessToken() = runTest {
        whenever(repo.getAccessToken()).thenReturn("accessToken")
        val actual = mainViewModel.getAccessToken()
        TestCase.assertEquals("accessToken", actual)
    }
}
