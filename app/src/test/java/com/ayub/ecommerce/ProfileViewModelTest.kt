package com.ayub.ecommerce

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.liveData
import com.ayub.core.datasource.network.request_response.BaseResponse
import com.ayub.core.datasource.network.request_response.ProfileData
import com.ayub.core.repository.IRepository
import com.ayub.ecommerce.screens.profile.ProfileViewModel
import com.ayub.ecommerce.utils.MainDispatcherRule
import junit.framework.TestCase
import kotlinx.coroutines.test.runTest
import okhttp3.MultipartBody
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@Suppress("UNUSED_EXPRESSION")
@RunWith(JUnit4::class)
class ProfileViewModelTest {

    private lateinit var profileViewModel: ProfileViewModel
    private lateinit var repo: IRepository

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        repo = mock()
        profileViewModel = ProfileViewModel(repo)
    }

    @Test
    fun saveProfile() = runTest {
        val name = MultipartBody.Part.createFormData("userName", "Ayub")
        val image = MultipartBody.Part.createFormData("userImage", "imageAyub.jpg")

        val expected = BaseResponse.Success(
            ProfileData(
                userName = "ayub",
                userImage = "image_ayub",
            )
        )

        whenever(repo.saveProfile(name, image)).thenReturn(liveData { expected })

        profileViewModel.saveProfile(name, image).observeForever {
            TestCase.assertEquals(expected, it)
        }
    }
}
