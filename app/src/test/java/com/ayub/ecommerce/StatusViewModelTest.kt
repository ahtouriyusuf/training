package com.ayub.ecommerce

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.liveData
import com.ayub.core.datasource.network.request_response.BaseResponse
import com.ayub.core.datasource.network.request_response.FulfillmentData
import com.ayub.core.repository.IRepository
import com.ayub.ecommerce.screens.main.status.StatusFragment
import com.ayub.ecommerce.screens.main.status.StatusViewModel
import com.ayub.ecommerce.utils.MainDispatcherRule
import junit.framework.TestCase
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class StatusViewModelTest {
    private lateinit var repo: IRepository
    private lateinit var savedStateHandle: SavedStateHandle
    private lateinit var statusViewModel: StatusViewModel

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Before
    fun setup() {
        repo = mock()

        val fulfillmentData = dummyFulfillmentData
        val rating = 4
        val review = "Great service!"
        val originFragment = "OriginFragment"

        savedStateHandle = SavedStateHandle(
            mapOf(
                StatusFragment.DATA_BUNDLE_KEY to fulfillmentData,
                StatusFragment.RATING_BUNDLE_KEY to rating,
                StatusFragment.REVIEW_BUNDLE_KEY to review,
                StatusFragment.ORIGIN_FRAGMENT_KEY to originFragment
            )
        )

        statusViewModel = StatusViewModel(repo, savedStateHandle)
    }

    @Test
    fun sendRating() = runTest {
        val invoiceId = "yourInvoiceId"
        val rating = 4
        val review = "Great service!"

        val ratingResponse = liveData<BaseResponse<Boolean>> {
            BaseResponse.Success(true)
        }

        whenever(repo.rating(invoiceId, rating, review)).thenReturn(ratingResponse)

        repo.rating(invoiceId, rating, review).observeForever {
            TestCase.assertEquals(ratingResponse.value, it)
        }
    }

    companion object {
        val dummyFulfillmentData = FulfillmentData(
            invoiceId = "ba47402c-d263-49d3-a1f8-759ae59fa4a1",
            status = true,
            date = "09 Jun 2023",
            time = "08:53",
            payment = "Bank BCA",
            total = 48998000
        )
    }
}
