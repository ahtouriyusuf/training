package com.ayub.ecommerce

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.liveData
import com.ayub.core.datasource.network.request_response.BaseResponse
import com.ayub.core.datasource.network.request_response.ReviewData
import com.ayub.core.repository.IRepository
import com.ayub.ecommerce.screens.main.store.review.ReviewFragment
import com.ayub.ecommerce.screens.main.store.review.ReviewViewModel
import junit.framework.TestCase
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class ReviewViewModelTest {
    private lateinit var reviewViewModel: ReviewViewModel
    private lateinit var repo: IRepository
    private lateinit var savedStateHandle: SavedStateHandle

    @Before
    fun setup() {
        repo = mock()
        savedStateHandle =
            SavedStateHandle(mapOf(ReviewFragment.BUNDLE_PRODUCT_ID_KEY to "productId"))
        reviewViewModel = ReviewViewModel(repo, savedStateHandle)
    }

    @Test
    fun getReview() = runTest {
        val idProduct = "productId"
        val expectedResult = liveData<BaseResponse<List<ReviewData?>>> {
            BaseResponse.Success(
                dummyReviewProduct
            )
        }

        whenever(repo.getProductReview(idProduct)).thenReturn(expectedResult)

        val actualResult = repo.getProductReview(idProduct)
        TestCase.assertEquals(expectedResult, actualResult)
    }

    companion object {
        val dummyReviewProduct = listOf(
            ReviewData(
                userName = "John",
                userImage = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTQM4VpzpVw8mR2j9_gDajEthwY3KCOWJ1tOhcv47-H9o1a-s9GRPxdb_6G9YZdGfv0HIg&usqp=CAU",
                userRating = 4,
                userReview = "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
            ),
            ReviewData(
                userName = "Doe",
                userImage = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTR3Z6PN8QNVhH0e7rEINu_XJS0qHIFpDT3nwF5WSkcYmr3znhY7LOTkc8puJ68Bts-TMc&usqp=CAU",
                userRating = 5,
                userReview = "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
            )
        )
    }
}
