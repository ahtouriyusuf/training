package com.ayub.ecommerce

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ayub.core.datasource.local.room.entity.Notification
import com.ayub.core.repository.IRepository
import com.ayub.ecommerce.screens.main.notification.NotificationViewModel
import com.ayub.ecommerce.utils.MainDispatcherRule
import junit.framework.TestCase
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class NotificationViewModelTest {
    private lateinit var notificationViewModel: NotificationViewModel
    private lateinit var repo: IRepository
    private lateinit var notificationData: LiveData<List<Notification>>

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        repo = mock()
        notificationViewModel = NotificationViewModel(repo)
    }

    @Test
    fun getAllNotification() = runTest {
        notificationData = MutableLiveData()

        val expectedNotification: List<Notification> = listOf(dummyNotification)

        whenever(repo.getAllNotification()).thenReturn(notificationData)

        val actualNotificationData = repo.getAllNotification()

        (notificationData as MutableLiveData<List<Notification>>).value = expectedNotification

        val actualNotification = actualNotificationData?.value

        TestCase.assertEquals(expectedNotification, actualNotification)
    }

    @Test
    fun updateNotification() = runTest {
        whenever(repo.updateNotification(dummyNotification.copy(isRead = true))).thenReturn(Unit)
        val actual = notificationViewModel.updateNotification(dummyNotification.copy(isRead = true))
        TestCase.assertEquals(Unit, actual)
    }

    companion object {
        val dummyNotification = Notification(
            "dummyId",
            "dummyType",
            "dummyDate",
            "dummyTime",
            "dummyTitle",
            "Dummy",
            "dummyImage",
            false,
        )
    }
}
