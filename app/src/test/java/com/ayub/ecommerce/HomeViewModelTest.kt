package com.ayub.ecommerce

import com.ayub.core.repository.IRepository
import com.ayub.ecommerce.screens.main.home.HomeViewModel
import com.ayub.ecommerce.utils.MainDispatcherRule
import junit.framework.TestCase
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class HomeViewModelTest {
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var repo: IRepository

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Before
    fun setup() {
        repo = mock()
        homeViewModel = HomeViewModel(repo)
    }

    @Test
    fun logout() = runTest {
        homeViewModel.logout()
        verify(repo).logout()
    }

    @Test
    fun getLang() = runTest {
        whenever(repo.isLangId()).thenReturn(true)
        val actual = homeViewModel.getLang()
        TestCase.assertEquals(true, actual)
    }

    @Test
    fun setLang() = runTest {
        homeViewModel.setLang(true)
        verify(repo).setLangId(true)
    }

    @Test
    fun getDarkMode() = runTest {
        whenever(repo.isDarkModeEnabled()).thenReturn(true)
        val actual = homeViewModel.getDarkMode()
        TestCase.assertEquals(true, actual)
    }

    @Test
    fun setDarkMode() = runTest {
        homeViewModel.setDarkMode(true)
        verify(repo).setDarkMode(true)
    }
}
