package com.ayub.ecommerce

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.liveData
import com.ayub.core.datasource.network.request_response.BaseResponse
import com.ayub.core.datasource.network.request_response.LoginData
import com.ayub.core.repository.IRepository
import com.ayub.ecommerce.screens.login.LoginViewModel
import com.ayub.ecommerce.utils.MainDispatcherRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@Suppress("UNUSED_EXPRESSION")
@RunWith(JUnit4::class)
class LoginViewModelTest {

    private lateinit var loginViewModel: LoginViewModel
    private lateinit var repo: IRepository

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        repo = mock()
        loginViewModel = LoginViewModel(repo)
    }

    @Test
    fun login() = runTest {
        val email = "ayub@gmail.com"
        val password = "12345678"
        val firebaseToken = "1q2w3e"

        val expected = BaseResponse.Success(
            LoginData(
                userName = "ayub",
                userImage = "image_ayub",
                accessToken = "qwerty",
                refreshToken = "asdfgh",
                expiresAt = 600
            )
        )

        whenever(repo.login(email, password, firebaseToken)).thenReturn(
            liveData {
                expected
            }
        )
        loginViewModel.login(email, password, firebaseToken)

        loginViewModel.loginResult.observeForever {
            assertEquals(expected, it)
        }
    }

    @Test
    fun isFirstLaunch() = runTest {
        whenever(repo.isFirstLaunch()).thenReturn(true)
        val actual = loginViewModel.isFirstLaunch()
        assertEquals(true, actual)
    }

    @Test
    fun getLang() = runTest {
        whenever(repo.isLangId()).thenReturn(true)
        val actual = loginViewModel.getLang()
        assertEquals(true, actual)
    }
}
