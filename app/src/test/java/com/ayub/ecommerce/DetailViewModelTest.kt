package com.ayub.ecommerce

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.liveData
import com.ayub.core.datasource.network.request_response.BaseResponse
import com.ayub.core.datasource.network.request_response.DetailProductData
import com.ayub.core.datasource.network.request_response.ProductVariantItem
import com.ayub.core.helper.toCart
import com.ayub.core.helper.toWishlist
import com.ayub.core.repository.IRepository
import com.ayub.ecommerce.screens.main.store.detail.DetailFragment
import com.ayub.ecommerce.screens.main.store.detail.DetailViewModel
import com.ayub.ecommerce.utils.MainDispatcherRule
import junit.framework.TestCase
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class DetailViewModelTest {
    private lateinit var detailViewModel: DetailViewModel
    private lateinit var repo: IRepository
    private lateinit var savedStateHandle: SavedStateHandle

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Before
    fun setup() {
        repo = mock()
        savedStateHandle =
            SavedStateHandle(mapOf(DetailFragment.BUNDLE_PRODUCT_ID_KEY to "productId"))
        detailViewModel = DetailViewModel(repo, savedStateHandle)
    }

    @Test
    fun getDetailProduct() = runTest {
        val idProduct = dummyDetailProduct.productId
        val expectedDetail = liveData<BaseResponse<DetailProductData>> {
            BaseResponse.Success(
                dummyDetailProduct
            )
        }

        whenever(repo.getDetailProduct(idProduct)).thenReturn(expectedDetail)

        val actualDetail = repo.getDetailProduct(idProduct)
        TestCase.assertEquals(expectedDetail, actualDetail)
    }

    @Test
    fun insertWishlist() = runTest {
        whenever(repo.saveToWishlist(dummyDetailProduct.toWishlist(productVariant!!))).thenReturn(
            Unit
        )
        val actual = detailViewModel.insertWishlist()
        TestCase.assertEquals(Unit, actual)
    }

    @Test
    fun insertWishlistCompose() = runTest {
        whenever(repo.saveToWishlist(dummyDetailProduct.toWishlist(productVariant!!))).thenReturn(
            Unit
        )
        val actual = detailViewModel.insertWishlistCompose(productVariant)
        TestCase.assertEquals(Unit, actual)
    }

    @Test
    fun deleteFromWishlist() = runTest {
        whenever(repo.deleteFromWishlist(dummyDetailProduct.toWishlist(productVariant!!))).thenReturn(
            Unit
        )
        val actual = detailViewModel.deleteFromWishlist()
        TestCase.assertEquals(Unit, actual)
    }

    @Test
    fun deleteFromWishlistCompose() = runTest {
        whenever(repo.deleteFromWishlist(dummyDetailProduct.toWishlist(productVariant!!))).thenReturn(
            Unit
        )
        val actual = detailViewModel.deleteFromWishlistCompose(productVariant)
        TestCase.assertEquals(Unit, actual)
    }

    @Test
    fun checkProductInWishlist() = runTest {
        whenever(repo.checkProductInWishlist(dummyDetailProduct.productId!!)).thenReturn(true)
        detailViewModel.idProduct = dummyDetailProduct.productId
        val actual = detailViewModel.checkProductInWishlist()
        TestCase.assertEquals(true, actual)
    }

    @Test
    fun addToCart() = runTest {
        whenever(repo.addToCart(dummyDetailProduct.toCart(dummyDetailProduct.productVariant?.first()!!)))
            .thenReturn(Unit)
        val actual = detailViewModel.addToCart(dummyDetailProduct.productVariant?.first()!!)
        TestCase.assertEquals(Unit, actual)
    }

    @Test
    fun getProductFromCart() = runTest {
        val productId = dummyDetailProduct.productId!!
        val expectedCart = dummyDetailProduct.toCart(dummyDetailProduct.productVariant?.first()!!)

        whenever(repo.getCartProductById(productId)).thenReturn(expectedCart)

        val actual = detailViewModel.getProductFromCart(productId)
        TestCase.assertEquals(expectedCart, actual)
    }

    @Test
    fun updateProductQuantity() = runTest {
        val productId = dummyDetailProduct.productId!!

        whenever(repo.updateCartItemQuantity(productId, 2)).thenReturn(Unit)

        val actual = detailViewModel.updateProductQuantity(productId, 2)
        TestCase.assertEquals(Unit, actual)
    }

    @Test
    fun checkProductInCart() = runTest {
        whenever(repo.checkProductInCart(dummyDetailProduct.productId!!)).thenReturn(true)
        detailViewModel.idProduct = dummyDetailProduct.productId
        val actual = detailViewModel.checkProductInCart()
        TestCase.assertEquals(true, actual)
    }

    companion object {
        val dummyDetailProduct = DetailProductData(
            productId = "17b4714d-527a-4be2-84e2-e4c37c2b3292",
            productName = "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray",
            productPrice = 24499000,
            image = listOf(
                "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/0a49c399-cf6b-47f5-91c9-8cbd0b86462d.jpg",
                "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/0cc3d06c-b09d-4294-8c3f-1c37e60631a6.jpg",
                "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/33a06657-9f88-4108-8676-7adafaa94921.jpg"
            ),
            brand = "Asus",
            description = "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray [AMD Ryzen™ 7 6800H / NVIDIA® GeForce RTX™ 3060 / 8G*2 / 512GB / 17.3inch / WIN11 / OHS]\n\nCPU : AMD Ryzen™ 7 6800H Mobile Processor (8-core/16-thread, 20MB cache, up to 4.7 GHz max boost)\nGPU : NVIDIA® GeForce RTX™ 3060 Laptop GPU\nGraphics Memory : 6GB GDDR6\nDiscrete/Optimus : MUX Switch + Optimus\nTGP ROG Boost : 1752MHz* at 140W (1702MHz Boost Clock+50MHz OC, 115W+25W Dynamic Boost)\nPanel : 17.3-inch FHD (1920 x 1080) 16:9 360Hz IPS-level 300nits sRGB % 100.00%",
            store = "AsusStore",
            sale = 12,
            stock = 2,
            totalRating = 7,
            totalReview = 5,
            totalSatisfaction = 100,
            productRating = (5.0).toFloat(),
            productVariant = listOf(
                ProductVariantItem(
                    variantName = "RAM 16GB",
                    variantPrice = 0
                ),
                ProductVariantItem(
                    variantName = "RAM 32GB",
                    variantPrice = 1000000
                )
            )
        )

        val productVariant = dummyDetailProduct.productVariant?.get(0)
    }
}
