package com.ayub.ecommerce

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.ayub.core.repository.IRepository
import com.ayub.ecommerce.screens.onboarding.OnboardingViewModel
import com.ayub.ecommerce.utils.MainDispatcherRule
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

@RunWith(JUnit4::class)
class OnboardingViewModelTest {

    private lateinit var onboardingViewModel: OnboardingViewModel
    private lateinit var repo: IRepository

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        repo = mock()
        onboardingViewModel = OnboardingViewModel(repo)
    }

    @Test
    fun setIsFirstLaunchToFalse() = runTest {
        onboardingViewModel.setIsFirstLaunchToFalse()
        verify(repo).setIsFirstLaunchToFalse()
    }
}
