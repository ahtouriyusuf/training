package com.ayub.ecommerce

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.liveData
import com.ayub.core.datasource.network.request_response.BaseResponse
import com.ayub.core.datasource.network.request_response.RegisterData
import com.ayub.core.repository.IRepository
import com.ayub.ecommerce.screens.register.RegisterViewModel
import com.ayub.ecommerce.utils.MainDispatcherRule
import junit.framework.TestCase
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@Suppress("UNUSED_EXPRESSION")
@RunWith(JUnit4::class)
class RegisterViewModelTest {

    private lateinit var registerViewModel: RegisterViewModel
    private lateinit var repo: IRepository

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        repo = mock()
        registerViewModel = RegisterViewModel(repo)
    }

    @Test
    fun register() = runTest {
        val email = "ayub@gmail.com"
        val password = "12345678"
        val firebaseToken = "1q2w3e"

        val expected = BaseResponse.Success(
            RegisterData(
                accessToken = "qwerty",
                refreshToken = "asdfgh",
                expiresAt = 600
            )
        )

        whenever(repo.register(email, password, firebaseToken)).thenReturn(
            liveData {
                expected
            }
        )

        registerViewModel.register(email, password, firebaseToken)

        registerViewModel.registerResult.observeForever {
            TestCase.assertEquals(expected, it)
        }
    }
}
