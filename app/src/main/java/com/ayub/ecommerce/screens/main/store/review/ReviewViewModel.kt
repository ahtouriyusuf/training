package com.ayub.ecommerce.screens.main.store.review

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ayub.core.datasource.network.request_response.BaseResponse
import com.ayub.core.datasource.network.request_response.ReviewData
import com.ayub.core.repository.IRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ReviewViewModel @Inject constructor(
    private val repo: IRepository,
    savedStateHandle: SavedStateHandle
) :
    ViewModel() {
    var idProduct = savedStateHandle.get<String>(ReviewFragment.BUNDLE_PRODUCT_ID_KEY)
    private val _reviewProductData = MutableLiveData<BaseResponse<List<ReviewData?>>>(null)
    val reviewProductData: LiveData<BaseResponse<List<ReviewData?>>> = _reviewProductData

    val getProductReview = repo.getProductReview(idProduct)

    init {
        getReviewProduct()
    }

    fun getReviewProduct() {
        viewModelScope.launch {
            repo.getProductReview(idProduct).observeForever {
                _reviewProductData.value = it
            }
        }
    }
}
