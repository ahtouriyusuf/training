package com.ayub.ecommerce.screens.main.notification

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.ayub.core.datasource.local.room.entity.Notification
import com.ayub.core.repository.IRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class NotificationViewModel @Inject constructor(private val repo: IRepository) : ViewModel() {
    fun getAllNotification(): LiveData<List<Notification>>? {
        return repo.getAllNotification()
    }

    suspend fun updateNotification(notification: Notification) {
        return repo.updateNotification(notification)
    }
}
