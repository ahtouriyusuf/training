package com.ayub.ecommerce.screens.main.checkout

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ayub.core.datasource.local.room.entity.Cart
import com.ayub.ecommerce.R
import com.ayub.ecommerce.databinding.ItemCheckoutBinding
import com.ayub.ecommerce.utils.CartComparator
import com.ayub.ecommerce.utils.toCurrencyFormat
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.runBlocking

class CheckoutAdapter(private val viewModel: CheckoutViewModel) :
    ListAdapter<Cart, CheckoutAdapter.ViewHolder>(CartComparator) {
    private val firebaseAnalytics: FirebaseAnalytics = Firebase.analytics
    private var onItemClickCallback: OnItemClickCallback? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val binding = ItemCheckoutBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(binding, onItemClickCallback)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    interface OnItemClickCallback {
        fun onItemClick(label: List<Cart>)
    }

    inner class ViewHolder(
        private val binding: ItemCheckoutBinding,
        private val onItemClickCallback: OnItemClickCallback?
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Cart) {
            with(binding) {
                var counterQuantity = data.quantity

                Glide.with(binding.root.context)
                    .load(data.image)
                    .fitCenter()
                    .into(ivProduct)
                tvProductName.text = data.productName
                tvVariant.text = data.variantName
                tvStock.text = data.stock.toString()
                getStockText(data.stock!!)
                tvPrice.text = data.productPrice.plus(data.variantPrice!!).toCurrencyFormat()
                quantity.tvCounter.text = counterQuantity.toString()

                quantity.btnIncrease.setOnClickListener {
                    firebaseAnalytics.logEvent("btn_add_quantity", null)
                    if (data.stock!! <= counterQuantity) {
                        val contextView = binding.root
                        Snackbar.make(
                            contextView,
                            R.string.item_quantity_maxed_txt,
                            Snackbar.LENGTH_SHORT
                        ).show()
                    } else {
                        counterQuantity += 1
                        binding.quantity.tvCounter.text = counterQuantity.toString()
                        runBlocking {
                            viewModel.updateProductQuantity(data, counterQuantity)
                        }

                        onItemClickCallback?.onItemClick(
                            listOf(
                                data.copy(quantity = counterQuantity)
                            )
                        )

                        quantity.btnIncrease.isSelected = false
                    }
                }

                quantity.btnDecrease.setOnClickListener {
                    firebaseAnalytics.logEvent("btn_reduce_quantity", null)
                    if (counterQuantity <= 1) {
                        val contextView = binding.root
                        Snackbar.make(
                            contextView,
                            R.string.item_quantity_not_empty_txt,
                            Snackbar.LENGTH_SHORT
                        ).show()
                    } else {
                        counterQuantity -= 1
                        binding.quantity.tvCounter.text = counterQuantity.toString()
                        runBlocking {
                            viewModel.updateProductQuantity(data, counterQuantity)
                        }

                        onItemClickCallback?.onItemClick(
                            listOf(
                                data.copy(quantity = counterQuantity)
                            )
                        )

                        quantity.btnDecrease.isSelected = false
                    }
                }
            }
        }

        private fun getStockText(stock: Int) {
            if (stock >= 10) {
                binding.tvStock.setTextColor(Color.BLACK)
                binding.stockTitle.setTextColor(Color.BLACK)
            } else {
                binding.tvStock.setTextColor(Color.RED)
                binding.stockTitle.setTextColor(Color.RED)
            }
        }
    }
}
