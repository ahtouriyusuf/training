package com.ayub.ecommerce.screens.main.payment

import androidx.lifecycle.ViewModel
import com.ayub.core.repository.IRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PaymentViewModel @Inject constructor(private val repo: IRepository) : ViewModel() {
    val getPaymentMethod = repo.getPayment()
}
