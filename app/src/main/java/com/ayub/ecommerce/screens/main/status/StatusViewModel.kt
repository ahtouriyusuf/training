package com.ayub.ecommerce.screens.main.status

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.ayub.core.datasource.network.request_response.FulfillmentData
import com.ayub.core.repository.IRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class StatusViewModel @Inject constructor(
    private val repo: IRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    val detailTransaction = savedStateHandle.get<FulfillmentData>(StatusFragment.DATA_BUNDLE_KEY)
    val rating = savedStateHandle.get<Int>(StatusFragment.RATING_BUNDLE_KEY)
    val review = savedStateHandle.get<String>(StatusFragment.REVIEW_BUNDLE_KEY)
    val originFragment = savedStateHandle.get<String>(StatusFragment.ORIGIN_FRAGMENT_KEY)

    fun sendRating(invoiceId: String, rating: Int?, review: String?) =
        repo.rating(invoiceId, rating, review)
}
