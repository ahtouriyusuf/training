package com.ayub.ecommerce.screens.main.cart

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.ayub.core.datasource.local.room.entity.Cart
import com.ayub.ecommerce.R
import com.ayub.ecommerce.databinding.FragmentCartBinding
import com.ayub.ecommerce.screens.MainActivity
import com.ayub.ecommerce.screens.main.checkout.CheckoutFragment
import com.ayub.ecommerce.utils.toCurrencyFormat
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CartFragment : Fragment() {
    private val firebaseAnalytics: FirebaseAnalytics = Firebase.analytics
    private var _binding: FragmentCartBinding? = null
    private val binding get() = _binding!!
    private val viewModel: CartViewModel by viewModels()
    private lateinit var adapter: CartAdapter
    lateinit var selectedIds: List<String>
    lateinit var selectedCart: List<Cart>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentCartBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = CartAdapter()
        binding.rvCart.adapter = adapter
        binding.rvCart.layoutManager = LinearLayoutManager(context)

        observeCart()
        itemActions()
        binding.totalPriceTV.text = viewModel.totalPrice.toCurrencyFormat()
    }

    private fun itemActions() {
        adapter.setOnItemClickCallback(object : CartAdapter.OnItemClickCallback {
            override fun onItemClicked(data: Cart) {
                val bundle = bundleOf("id" to data.productId)
                (requireActivity() as MainActivity).cartToDetail(bundle)
                firebaseAnalytics.logEvent("btn_toDetailProduct", null)
            }

            override fun deleteCallback(data: Cart) {
                viewModel.deleteProduct(data)
                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.REMOVE_FROM_CART) {
                    param(FirebaseAnalytics.Param.ITEM_ID, data.productId)
                    param(FirebaseAnalytics.Param.ITEM_NAME, data.productName)
                }
            }

            override fun increaseQuantity(data: Cart) {
                var quantity = data.quantity
                if (data.quantity < data.stock!!) {
                    quantity += 1
                    viewModel.updateProductQuantity(data.productId, quantity)
                    firebaseAnalytics.logEvent("btn_add_quantity", null)
                }
            }

            override fun decreaseQuantity(data: Cart) {
                var quantity = data.quantity
                if (data.quantity > 1) {
                    quantity -= 1
                    viewModel.updateProductQuantity(data.productId, quantity)
                    firebaseAnalytics.logEvent("btn_reduce_quantity", null)
                }
            }

            override fun selectProduct(data: Cart) {
                var selected = data.selected
                selected = !selected
                viewModel.updateProductIsSelected(listOf(data.productId), selected)
                firebaseAnalytics.logEvent("btn_select_product", null)
            }
        })

        binding.topAppBar.setOnClickListener {
            findNavController().navigateUp()
            firebaseAnalytics.logEvent("btn_back_from_cart", null)
        }

        binding.selectAll.setOnClickListener {
            val isAllChecked = binding.selectAll.isChecked
            selectAllItems(isAllChecked)
            firebaseAnalytics.logEvent("btn_select_all_product", null)
        }

        binding.deleteAllBtn.setOnClickListener {
            viewModel.deleteAllProductInCart(selectedIds)
            var bundle = arrayOf(bundleOf())
            selectedIds.forEach {
                bundle += bundleOf(
                    FirebaseAnalytics.Param.ITEM_ID to it
                )
            }
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.REMOVE_FROM_CART) {
                param(FirebaseAnalytics.Param.ITEMS, bundle)
            }
        }

        binding.buyBtn.setOnClickListener {
            val bundle = bundleOf(CheckoutFragment.ARG_DATA to selectedCart)
            findNavController().navigate(R.id.action_cartFragment_to_checkoutFragment, bundle)
            firebaseAnalytics.logEvent("btn_toCheckout", null)
        }
    }

    fun observeCart() {
        viewModel.cartData.observe(viewLifecycleOwner) {
            adapter.submitList(it)
            setView(it)
            selectedCart = it.filter { it.selected }
            selectedIds = selectedCart.map { it.productId }

            if (it.isEmpty()) {
                binding.linearErrorLayout.visibility = View.VISIBLE
                binding.rvCart.visibility = View.GONE
                binding.checkLayout.visibility = View.GONE
                binding.linearLayout.visibility = View.GONE
                binding.materialDivider2.visibility = View.GONE
                binding.materialDivider3.visibility = View.GONE
            } else {
                binding.linearErrorLayout.visibility = View.GONE
                binding.rvCart.visibility = View.VISIBLE
                binding.checkLayout.visibility = View.VISIBLE
                binding.linearLayout.visibility = View.VISIBLE
                binding.materialDivider2.visibility = View.VISIBLE
                binding.materialDivider3.visibility = View.VISIBLE
            }
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM_LIST) {
                val bundle = ArrayList<Bundle>()
                it.map {
                    val itemBundle = Bundle().apply {
                        putString("item_id", it.productId)
                        putString("item_name", it.productName)
                        putString("item_variant", it.variantName)
                    }
                    bundle.add(itemBundle)
                }
                param(FirebaseAnalytics.Param.ITEMS, bundle.toTypedArray())
            }
        }
    }

    private fun setView(listCart: List<Cart>) {
        binding.selectAll.isChecked = listCart.all {
            it.selected
        }
        binding.deleteAllBtn.isVisible = listCart.any { it.selected }
        binding.buyBtn.isEnabled = listCart.any { it.selected }
        viewModel.totalPrice = listCart.filter { it.selected }.sumOf {
            (it.productPrice + it.variantPrice!!) * it.quantity
        }
        binding.totalPriceTV.text = viewModel.totalPrice.toCurrencyFormat()
    }

    private fun selectAllItems(isSelected: Boolean) {
        binding?.apply {
            val cartItems = adapter.currentList
            val selectedIds = mutableListOf<String>()

            for (item in cartItems) {
                item.selected = isSelected
                selectedIds.add(item.productId)
            }
            viewModel.updateProductIsSelected(selectedIds, isSelected)
            adapter.notifyDataSetChanged()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
