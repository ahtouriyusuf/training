package com.ayub.ecommerce.screens.main.checkout

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.ayub.core.datasource.local.room.entity.Cart
import com.ayub.core.datasource.network.request_response.PaymentTypeItem
import com.ayub.core.repository.IRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class CheckoutViewModel @Inject constructor(
    private val repo: IRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {
    var totalPrice = 0
    var paymentItem: PaymentTypeItem.PaymentItem? = null

    private val _listData: MutableLiveData<List<Cart>> = MutableLiveData(ArrayList())
    val listData: LiveData<List<Cart>> = _listData

    init {
        setData(savedStateHandle.get<ArrayList<Cart>>(CheckoutFragment.ARG_DATA) ?: ArrayList())
    }

    fun setData(data: ArrayList<Cart>) {
        _listData.value = data.map { it.copy(quantity = it.quantity) }
    }

    suspend fun updateProductQuantity(cart: Cart, newQuantity: Int) {
        repo.updateCartItemQuantity(cart.productId, newQuantity)
    }

    fun makePayment(payment: String, items: List<Cart>) = repo.fulfillment(payment, items)
}
