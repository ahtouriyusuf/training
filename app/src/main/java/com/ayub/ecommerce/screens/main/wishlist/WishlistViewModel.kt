package com.ayub.ecommerce.screens.main.wishlist

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ayub.core.datasource.local.room.entity.Cart
import com.ayub.core.datasource.local.room.entity.WishlistProduct
import com.ayub.core.helper.toCart
import com.ayub.core.repository.IRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WishlistViewModel @Inject constructor(private val repo: IRepository) : ViewModel() {
    var recyclerViewType = WishlistAdapter.ONE_COLUMN_VIEW_TYPE
    var isInCart: Boolean = false

    val getWishlistData: LiveData<List<WishlistProduct>> = repo.getWishlistProducts()

    fun deleteWishlist(wishlist: WishlistProduct) {
        viewModelScope.launch {
            repo.deleteFromWishlist(wishlist)
        }
    }

    fun addToCart(wishlist: WishlistProduct) {
        viewModelScope.launch {
            repo.addToCart(wishlist.toCart())
        }
        isInCart = true
    }

    suspend fun getProductFromCart(id: String): Cart? {
        return repo.getCartProductById(id)
    }

    fun updateProductQuantity(id: String, newQuantity: Int) {
        viewModelScope.launch {
            repo.updateCartItemQuantity(id, newQuantity)
        }
    }
}
