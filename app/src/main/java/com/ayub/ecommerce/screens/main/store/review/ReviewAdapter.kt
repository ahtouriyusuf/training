package com.ayub.ecommerce.screens.main.store.review

import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.ayub.core.datasource.network.request_response.ReviewData
import com.ayub.ecommerce.R
import com.ayub.ecommerce.databinding.ItemReviewBinding
import com.bumptech.glide.Glide

class ReviewAdapter(private val data: List<ReviewData?>?) :
    RecyclerView.Adapter<ReviewAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewAdapter.ViewHolder {
        val binding = ItemReviewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ReviewAdapter.ViewHolder, position: Int) {
        holder.bind(data!![position])
        holder.itemView.startAnimation(
            AnimationUtils.loadAnimation(
                holder.itemView.context,
                R.anim.grid_item_animation
            )
        )
    }

    override fun getItemCount(): Int {
        return data!!.size
    }

    inner class ViewHolder(var binding: ItemReviewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ReviewData?) {
            binding.apply {
                Glide.with(binding.root.context)
                    .load(data?.userImage)
                    .fitCenter()
                    .circleCrop()
                    .into(binding.reviewerImg)
                reviewerNameTV.text = data?.userName
                ratingBar.rating = data?.userRating!!.toFloat()
                reviewTV.text = data.userReview
            }
        }
    }
}
