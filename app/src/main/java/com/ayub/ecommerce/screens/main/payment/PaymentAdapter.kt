package com.ayub.ecommerce.screens.main.payment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ayub.core.datasource.network.request_response.PaymentTypeItem
import com.ayub.ecommerce.databinding.ItemPaymentBinding
import com.ayub.ecommerce.utils.PaymentComparator

class PaymentAdapter(private val dataList: List<PaymentTypeItem?>) :
    ListAdapter<PaymentTypeItem, PaymentAdapter.ViewHolder>(PaymentComparator) {

    private var itemClickListener: PaymentMethodItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemPaymentBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = dataList[position]
        holder.bind(item)
    }

    override fun getItemCount() = dataList.size

    interface PaymentMethodItemClickListener {
        fun onItemClick(label: PaymentTypeItem.PaymentItem)
    }

    fun setItemClickListener(listener: PaymentMethodItemClickListener) {
        itemClickListener = listener
    }

    inner class ViewHolder(
        var binding: ItemPaymentBinding
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(result: PaymentTypeItem?) {
            val childPaymentAdapter =
                PaymentItemAdapter(
                    result?.item!!,
                    object : PaymentItemAdapter.PaymentItemClickListener {
                        override fun onItemClick(item: PaymentTypeItem.PaymentItem) {
                            if (item.status!!) {
                                itemClickListener?.onItemClick(item)
                            } else {
                                binding.root.setOnClickListener(null)
                            }
                        }
                    }
                )
            childPaymentAdapter.submitList(result.item)
            binding.tvPaymentTitle.text = result.title
            binding.rvPaymentItem.layoutManager = object : LinearLayoutManager(itemView.context) {
                override fun canScrollVertically(): Boolean = false
            }
            binding.rvPaymentItem.adapter = childPaymentAdapter
        }
    }
}
