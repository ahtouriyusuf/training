package com.ayub.ecommerce.screens.main.home

import androidx.lifecycle.ViewModel
import com.ayub.core.repository.IRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val repo: IRepository) : ViewModel() {
    fun logout() = repo.logout()

    fun getLang(): Boolean = repo.isLangId()

    fun setLang(lang: Boolean) = repo.setLangId(lang)

    fun getDarkMode(): Boolean = repo.isDarkModeEnabled()

    fun setDarkMode(darkMode: Boolean) = repo.setDarkMode(darkMode)
}
