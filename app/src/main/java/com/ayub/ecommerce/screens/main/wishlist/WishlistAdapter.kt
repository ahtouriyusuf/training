package com.ayub.ecommerce.screens.main.wishlist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.ayub.core.datasource.local.room.entity.WishlistProduct
import com.ayub.ecommerce.databinding.ItemWishlistGridBinding
import com.ayub.ecommerce.databinding.ItemWishlistLinearBinding
import com.ayub.ecommerce.screens.main.store.StoreAdapter
import com.ayub.ecommerce.utils.WishlistComparator
import com.ayub.ecommerce.utils.toCurrencyFormat
import com.bumptech.glide.Glide

class WishlistAdapter :
    ListAdapter<WishlistProduct, WishlistAdapter.ViewHolder<ViewBinding>>(WishlistComparator) {
    var viewType = ONE_COLUMN_VIEW_TYPE
    private var onItemClickCallback: OnItemClickCallback? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder<ViewBinding> {
        if (viewType == ONE_COLUMN_VIEW_TYPE) {
            val binding =
                ItemWishlistLinearBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            return ViewHolder(binding, viewType, onItemClickCallback)
        } else {
            val binding =
                ItemWishlistGridBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            return ViewHolder(binding, viewType, onItemClickCallback)
        }
    }

    override fun onBindViewHolder(holder: ViewHolder<ViewBinding>, position: Int) {
        holder.bind(getItem(position))
    }

    override fun getItemViewType(position: Int): Int {
        return viewType
    }

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    interface OnItemClickCallback {
        fun onItemClicked(data: WishlistProduct)
        fun deleteCallback(data: WishlistProduct)
        fun addToCart(data: WishlistProduct)
    }

    class ViewHolder<T : ViewBinding>(
        var binding: T,
        private val viewType: Int,
        private val onItemClickCallback: OnItemClickCallback?
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: WishlistProduct) {
            itemView.setOnClickListener {
                onItemClickCallback?.onItemClicked(data)
            }
            if (viewType == StoreAdapter.ONE_COLUMN_VIEW_TYPE) {
                with(binding as ItemWishlistLinearBinding) {
                    Glide.with(binding.root.context)
                        .load(data.image)
                        .fitCenter()
                        .into(ivProduct)
                    tvProductName.text = data.productName
                    tvProductPrice.text =
                        data.productPrice?.plus(data.variantPrice ?: 0)?.toCurrencyFormat()
                    tvStore.text = data.store
                    tvRating.text = data.productRating.toString()
                    tvSell.text = data.sale.toString()
                    deleteBtn.setOnClickListener { onItemClickCallback?.deleteCallback(data) }
                    addCartBtn.setOnClickListener { onItemClickCallback?.addToCart(data) }
                }
            } else {
                with(binding as ItemWishlistGridBinding) {
                    Glide.with(binding.root.context)
                        .load(data.image)
                        .fitCenter()
                        .into(ivProduct)
                    tvProductName.text = data.productName
                    tvProductPrice.text =
                        data.productPrice?.plus(data.variantPrice ?: 0)?.toCurrencyFormat()
                    tvStore.text = data.store
                    tvRating.text = data.productRating.toString()
                    tvSell.text = data.sale.toString()
                    deleteBtn.setOnClickListener { onItemClickCallback?.deleteCallback(data) }
                    addCartBtn.setOnClickListener { onItemClickCallback?.addToCart(data) }
                }
            }
        }
    }

    companion object {
        const val ONE_COLUMN_VIEW_TYPE = 1
        const val MORE_COLUMN_VIEW_TYPE = 2
    }
}
