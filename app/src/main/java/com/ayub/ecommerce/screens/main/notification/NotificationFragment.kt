package com.ayub.ecommerce.screens.main.notification

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.ayub.core.datasource.local.room.entity.Notification
import com.ayub.ecommerce.databinding.FragmentNotificationBinding
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.runBlocking

@AndroidEntryPoint
class NotificationFragment : Fragment() {
    private var _binding: FragmentNotificationBinding? = null
    private val binding get() = _binding
    private val viewModel: NotificationViewModel by viewModels()
    private val adapter: NotificationAdapter by lazy { NotificationAdapter() }
    private val firebaseAnalytics: FirebaseAnalytics = Firebase.analytics

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentNotificationBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.topAppBar?.setNavigationOnClickListener {
            findNavController().navigateUp()
        }

        val linearLayout = LinearLayoutManager(requireContext())
        binding?.rvNotification?.layoutManager = linearLayout
        linearLayout.reverseLayout = true
        linearLayout.stackFromEnd = true
        binding?.rvNotification?.adapter = adapter
        viewModel.getAllNotification()?.observe(viewLifecycleOwner) {
            if (it.isNotEmpty()) {
                adapter.submitList(it)
            } else {
                binding?.linearErrorLayout?.visibility = View.VISIBLE
                binding?.rvNotification?.visibility = View.GONE
            }
        }

        adapter.setOnItemClickCallback(object : NotificationAdapter.OnItemClickCallback {
            override fun onItemClick(item: Notification) {
                runBlocking {
                    viewModel.updateNotification(
                        Notification(
                            item.id,
                            item.type,
                            item.date,
                            item.time,
                            item.title,
                            item.body,
                            item.image,
                            true
                        )
                    )
                }
                firebaseAnalytics.logEvent("btn_notification_is_read", null)
            }
        })
    }
}
