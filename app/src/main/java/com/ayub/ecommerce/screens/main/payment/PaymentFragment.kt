package com.ayub.ecommerce.screens.main.payment

import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.ayub.core.datasource.network.request_response.PaymentResponse
import com.ayub.core.datasource.network.request_response.PaymentTypeItem
import com.ayub.ecommerce.R
import com.ayub.ecommerce.databinding.FragmentPaymentBinding
import com.google.firebase.Firebase
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.remoteconfig.ConfigUpdate
import com.google.firebase.remoteconfig.ConfigUpdateListener
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigException
import com.google.firebase.remoteconfig.get
import com.google.firebase.remoteconfig.remoteConfig
import com.google.firebase.remoteconfig.remoteConfigSettings
import com.google.gson.GsonBuilder
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PaymentFragment : Fragment() {
    private val firebaseAnalytics: FirebaseAnalytics = Firebase.analytics
    private var _binding: FragmentPaymentBinding? = null
    private val binding get() = _binding!!
    private lateinit var adapter: PaymentAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentPaymentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getDataPayment()
    }

    private fun getDataPayment() {
        val remoteConfig: FirebaseRemoteConfig = Firebase.remoteConfig
        val configSettings = remoteConfigSettings {
            minimumFetchIntervalInSeconds = 3600
        }

        remoteConfig.setConfigSettingsAsync(configSettings)
        remoteConfig.setDefaultsAsync(R.xml.remote_config_payment)

        remoteConfig.fetchAndActivate()
            .addOnCompleteListener(requireActivity()) {
                getData()
            }

        remoteConfig.addOnConfigUpdateListener(object : ConfigUpdateListener {
            override fun onUpdate(configUpdate: ConfigUpdate) {
                Log.d(TAG, "Updated keys: " + configUpdate.updatedKeys)
                if (configUpdate.updatedKeys.contains(BUNDLE_PAYMENT_KEY)) {
                    remoteConfig.activate().addOnCompleteListener {
                        getData()
                    }
                }
            }

            override fun onError(error: FirebaseRemoteConfigException) {
                Log.w(TAG, "Config update error with code: " + error.code, error)
            }
        })
    }

    private fun getData() {
        val remoteConfig = Firebase.remoteConfig
        val dataPayment = GsonBuilder().create().fromJson(
            remoteConfig[BUNDLE_PAYMENT_KEY].asString(),
            PaymentResponse::class.java
        )

        adapter = PaymentAdapter(dataPayment.data!!)
        binding.rvPayment.adapter = adapter
        binding.rvPayment.layoutManager = LinearLayoutManager(requireContext())

        adapter.setItemClickListener(object :
            PaymentAdapter.PaymentMethodItemClickListener {
            override fun onItemClick(label: PaymentTypeItem.PaymentItem) {
                val bundle = Bundle().apply {
                    putParcelable(BUNDLE_PAYMENT_KEY, label)
                }
                setFragmentResult(REQUEST_KEY, bundle)
                findNavController().navigateUp()
                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_PAYMENT_INFO) {
                    param(FirebaseAnalytics.Param.PAYMENT_TYPE, label.label!!)
                }
            }
        })
    }

    companion object {
        const val REQUEST_KEY = "payment_request_key"
        const val BUNDLE_PAYMENT_KEY = "payment"
    }
}
