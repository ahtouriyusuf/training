package com.ayub.ecommerce.screens.main.store.search

import android.app.Dialog
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.activity.OnBackPressedCallback
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.ayub.core.datasource.network.request_response.BaseResponse
import com.ayub.ecommerce.databinding.FragmentSearchDialogBinding
import com.ayub.ecommerce.screens.main.store.bottomsheet.BottomSheetDialog
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SearchDialogFragment : DialogFragment() {
    private var _binding: FragmentSearchDialogBinding? = null
    private val binding get() = _binding!!
    private val viewModel: SearchViewModel by viewModels()
    private lateinit var adapter: SearchAdapter
    private var search: String? = ""
    private val firebaseAnalytics: FirebaseAnalytics = Firebase.analytics

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSearchDialogBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onBack()
        setSearch()

        binding.searchInput.requestFocus()
        val imm = requireContext().getSystemService(InputMethodManager::class.java)
        imm.showSoftInput(binding.searchInput, InputMethodManager.SHOW_IMPLICIT)
    }

    private fun setSearch() {
        adapter = SearchAdapter()
        binding.rvSearch.apply {
            this.layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = this@SearchDialogFragment.adapter
        }

        search = arguments?.getString(SEARCH)
        if (search.isNullOrEmpty() || search == "null") {
            binding.searchInput.setText("")
        } else {
            binding.searchInput.setText(search)
        }

        binding.searchInput.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH ||
                event.action == KeyEvent.ACTION_DOWN &&
                actionId == KeyEvent.KEYCODE_ENTER
            ) {
                lifecycleScope.launch {
                    delay(1000)
                    if (v != null) {
                        setData(v.text.toString())
                    } else {
                        setData("")
                    }
                    dismiss()
                }
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }

        binding.searchInput.doOnTextChanged { text, _, _, _ ->
            val job: Job? = null
            job?.cancel()
            lifecycleScope.launch {
                delay(1000)
                viewModel.searchProducts(text.toString()).observe(viewLifecycleOwner) {
                    when (it) {
                        is BaseResponse.Loading -> {
                            binding.progressIndicator.visibility = View.VISIBLE
                        }

                        is BaseResponse.Success -> {
                            adapter.setSearchProduct(it.data!!.data)
                            binding.progressIndicator.visibility = View.GONE
                        }

                        is BaseResponse.Error -> {
                            binding.progressIndicator.visibility = View.GONE
                            Snackbar.make(
                                requireView(),
                                it.error?.message.toString(),
                                Snackbar.LENGTH_SHORT
                            ).show()
                        }
                    }
                }
                adapter.setOnItemClickCallBack(object : SearchAdapter.OnItemClickCallback {
                    override fun onItemClicked(data: String) {
                        setData(data)
                        dismiss()
                        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SEARCH) {
                            param(FirebaseAnalytics.Param.SEARCH_TERM, data)
                        }
                    }
                })
            }
        }
    }

    private fun setData(data: String) {
        val bundle = Bundle().apply {
            putString(SEARCH, data)
        }
        requireActivity().supportFragmentManager.setFragmentResult(
            BottomSheetDialog.FILTER,
            bundle
        )
    }

    private fun onBack() {
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    dismiss()
                }
            }
        )
    }

    companion object {
        const val TAG = "SearchDialogFragment"
        const val SEARCH = "search"

        @JvmStatic
        fun newInstance(
            search: String?,
        ): SearchDialogFragment {
            val myFragment = SearchDialogFragment()

            val args = Bundle().apply {
                putString(SEARCH, search)
            }
            myFragment.arguments = args

            return myFragment
        }
    }
}
