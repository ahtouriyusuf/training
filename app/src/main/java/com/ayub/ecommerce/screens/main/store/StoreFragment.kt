package com.ayub.ecommerce.screens.main.store

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ayub.core.datasource.local.SharedPref
import com.ayub.core.datasource.network.request_response.Items
import com.ayub.ecommerce.R
import com.ayub.ecommerce.databinding.FragmentStoreBinding
import com.ayub.ecommerce.screens.MainActivity
import com.ayub.ecommerce.screens.main.store.bottomsheet.BottomSheetDialog
import com.ayub.ecommerce.screens.main.store.bottomsheet.BottomSheetDialog.Companion.FILTER
import com.ayub.ecommerce.screens.main.store.bottomsheet.BottomSheetDialog.Companion.TAG
import com.ayub.ecommerce.screens.main.store.search.SearchDialogFragment
import com.ayub.ecommerce.screens.main.store.search.SearchDialogFragment.Companion.SEARCH
import com.ayub.ecommerce.utils.toCurrencyFormat
import com.google.android.material.chip.Chip
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import dagger.hilt.android.AndroidEntryPoint
import retrofit2.HttpException
import java.io.IOException
import java.util.UUID

@AndroidEntryPoint
class StoreFragment : Fragment() {
    private val firebaseAnalytics: FirebaseAnalytics = Firebase.analytics
    private var _binding: FragmentStoreBinding? = null
    private lateinit var sharedPref: SharedPref
    private val binding get() = _binding!!
    private val viewModel: StoreViewModel by viewModels()
    private lateinit var adapter: StoreAdapter
    private var sort: String? = null
    private var brand: String? = null
    private var lowest: String? = null
    private var highest: String? = null
    private var search: String? = null
    private var dataChip = mutableListOf<String>()
    private val footerAdapter = LoadStateAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedPref = SharedPref(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentStoreBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        adapter = StoreAdapter()
        binding.rvProduct.adapter = adapter.withLoadStateFooter(footer = footerAdapter)
        observeProducts()
        getFilterResult()
        loadDataState()
        setRecyclerView()
        setImageBtnChangeRecyclerView()
        setSearch()

        binding.ivLayout.setOnClickListener {
            actionChangeRecyclerView()
            firebaseAnalytics.logEvent("btn_change_store_layout", null)
        }

        binding.swipeRefresh.setOnRefreshListener {
            adapter.refresh()
            binding.swipeRefresh.isRefreshing = false
            firebaseAnalytics.logEvent("btn_refresh_store", null)
        }

        binding.errorButton.setOnClickListener {
            firebaseAnalytics.logEvent("btn_error_store_action", null)
            refreshAfterError()
        }

        binding.searchInput.setOnClickListener {
            if (search == null) {
                viewModel.filteredProduct.observe(viewLifecycleOwner) {
                    search = it.search
                }
            }

            val fragmentManager = requireActivity().supportFragmentManager
            val newFragment = SearchDialogFragment.newInstance(search.toString())
            val transaction = fragmentManager.beginTransaction()
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            transaction.add(android.R.id.content, newFragment)
                .addToBackStack(null)
                .commit()
            newFragment.show(parentFragmentManager, SearchDialogFragment.TAG)
            firebaseAnalytics.logEvent("btn_toSearchProduct", null)
        }

        binding.chipFilter.setOnClickListener {
            val bottomSheet = BottomSheetDialog.instance(
                sort = translateChip(sort ?: "") ?: sort,
                brand = translateChip(brand ?: "") ?: brand,
                highest = translateChip(highest ?: "") ?: highest,
                lowest = translateChip(lowest ?: "") ?: lowest
            )
            bottomSheet.show(parentFragmentManager, TAG)
            firebaseAnalytics.logEvent("btn_toFilterProduct", null)
        }

        adapter.setOnItemClickCallback(object : StoreAdapter.OnItemClickCallback {
            override fun onItemClicked(data: Items) {
                val bundle = bundleOf("id" to data.productId)
                (requireActivity() as MainActivity).toDetailFragment(bundle)
                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
                    param(FirebaseAnalytics.Param.ITEM_ID, data.productId!!)
                    param(FirebaseAnalytics.Param.ITEM_NAME, data.productName!!)
                }
            }
        })
    }

    private fun observeProducts() {
        viewModel.product.observe(viewLifecycleOwner) { paging ->
            adapter.submitData(viewLifecycleOwner.lifecycle, paging)

            adapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver(){
                override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                    super.onItemRangeInserted(positionStart, itemCount)
                    val items = adapter.snapshot().items.mapIndexed { index, product ->
                        val bundle = Bundle().apply {
                            putString(FirebaseAnalytics.Param.ITEM_ID, product.productId)
                            putString(FirebaseAnalytics.Param.ITEM_NAME, product.productName)
                            putString(FirebaseAnalytics.Param.ITEM_BRAND, product.brand)
                            putDouble(FirebaseAnalytics.Param.PRICE, product.productPrice!!.toDouble())
                        }
                        val bundleWithIndex = Bundle(bundle).apply {
                            putLong(FirebaseAnalytics.Param.INDEX, (index + 1).toLong())
                        }
                        bundleWithIndex
                    }.toTypedArray()
                    firebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM_LIST) {
                        param(FirebaseAnalytics.Param.ITEMS, items)
                    }
                }
            })
        }
    }

    private fun getFilterResult() {
        binding.apply {
            setFragmentResultListener(FILTER) { _, bundle ->
                sort = bundle.getString(BottomSheetDialog.CHIP_SORT)
                brand = bundle.getString(BottomSheetDialog.CHIP_BRAND)
                lowest = bundle.getString(BottomSheetDialog.LOWEST)
                highest = bundle.getString(BottomSheetDialog.HIGHEST)
                viewModel.updateFilter(
                    sort = sort,
                    brand = brand,
                    lowest = lowest,
                    highest = highest
                )
            }
            viewModel.filteredProduct.observe(viewLifecycleOwner) {
                it.let {
                    sort = it.sort
                    brand = it.brand
                    lowest = it.lowest
                    highest = it.highest
                    dataChip.clear()
                    brand?.let { dataChip.add(it) }
                    sort?.let { translateSort ->
                        translateChip(translateSort)?.let { translatedText ->
                            dataChip.add(translatedText)
                        } ?: dataChip.add(translateSort)
                    }
                    if (lowest != "") {
                        lowest?.let { dataChip.add("> " + priceFormating(it)) }
                    }
                    if (highest != "") {
                        highest?.let { dataChip.add("< " + priceFormating(it)) }
                    }
                    setChip(dataChip)
                }
            }
        }
    }

    private fun priceFormating(number: String): String {
        return number.toInt().toCurrencyFormat()
    }

    private fun setChip(names: List<String>) {
        binding.apply {
            chipGroup.removeAllViewsInLayout()
            for (name in names) {
                val chip = Chip(requireContext())
                chip.apply {
                    text = name
                    isChipIconVisible = false
                    isCloseIconVisible = false
                    isCheckable = false
                    isChecked = false
                }
                chipGroup.addView(chip as View)
            }
        }
    }

    private fun setSearch() {
        requireActivity().supportFragmentManager.setFragmentResultListener(
            FILTER,
            viewLifecycleOwner
        ) { _, bundle ->
            search = bundle.getString(SEARCH)
            binding.searchInput.setText(search)
            viewModel.updateSearch(search)
        }
    }

    private fun actionChangeRecyclerView() {
        viewModel.recyclerViewType =
            if (viewModel.recyclerViewType == StoreAdapter.ONE_COLUMN_VIEW_TYPE) {
                StoreAdapter.MORE_COLUMN_VIEW_TYPE
            } else {
                StoreAdapter.ONE_COLUMN_VIEW_TYPE
            }
        setRecyclerView()
        setImageBtnChangeRecyclerView()
    }

    private fun setImageBtnChangeRecyclerView() {
        binding.ivLayout.setImageResource(
            if (viewModel.recyclerViewType == StoreAdapter.ONE_COLUMN_VIEW_TYPE) {
                R.drawable.ic_linear_view
            } else {
                R.drawable.ic_grid_view
            }
        )
    }

    private fun setRecyclerView() {
        if (viewModel.recyclerViewType == StoreAdapter.ONE_COLUMN_VIEW_TYPE) {
            adapter.viewType = StoreAdapter.ONE_COLUMN_VIEW_TYPE
            binding.rvProduct.layoutManager = LinearLayoutManager(requireContext())
        } else {
            adapter.viewType = StoreAdapter.MORE_COLUMN_VIEW_TYPE
            binding.rvProduct.layoutManager = GridLayoutManager(requireContext(), 1)
            (binding.rvProduct.layoutManager as GridLayoutManager).spanCount = 2
            (binding.rvProduct.layoutManager as GridLayoutManager).spanSizeLookup =
                object : GridLayoutManager.SpanSizeLookup() {
                    override fun getSpanSize(position: Int): Int {
                        return if (position == adapter.itemCount && footerAdapter.itemCount > 0) {
                            2
                        } else {
                            1
                        }
                    }
                }
        }
    }

    private fun loadDataState() {
        adapter.addLoadStateListener {
            when (it.refresh) {
                is LoadState.Loading -> {
                    setShimmerLayout()
                    binding.shimmerFilter.visibility = View.VISIBLE
                    binding.swipeRefresh.visibility = View.INVISIBLE
                    binding.chipsLayout.visibility = View.INVISIBLE
                }

                is LoadState.Error -> {
                    setOffShimmerLayout()
                    binding.shimmerFilter.visibility = View.GONE
                    binding.linearErrorLayout.visibility = View.VISIBLE
                    val error = (it.refresh as LoadState.Error).error
                    setErrorMessage(error)
                }

                is LoadState.NotLoading -> {
                    setOffShimmerLayout()
                    binding.shimmerFilter.visibility = View.GONE
                    binding.swipeRefresh.visibility = View.VISIBLE
                    binding.chipsLayout.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun setShimmerLayout() {
        if (viewModel.recyclerViewType == StoreAdapter.ONE_COLUMN_VIEW_TYPE) {
            binding.shimmerLinear.visibility = View.VISIBLE
        } else if (viewModel.recyclerViewType == StoreAdapter.MORE_COLUMN_VIEW_TYPE) {
            binding.shimmerGrid.visibility = View.VISIBLE
        }
    }

    private fun setOffShimmerLayout() {
        if (viewModel.recyclerViewType == StoreAdapter.ONE_COLUMN_VIEW_TYPE) {
            binding.shimmerLinear.visibility = View.GONE
        } else if (viewModel.recyclerViewType == StoreAdapter.MORE_COLUMN_VIEW_TYPE) {
            binding.shimmerGrid.visibility = View.GONE
        }
    }

    private fun setErrorMessage(error: Throwable) {
        when (error) {
            is HttpException -> {
                if (error.response()?.code() == 404) {
                    binding.errorTypeText.text = getString(R.string.empty_list_state)
                    binding.errorTypeInfo.text = getString(R.string.empty_list_description)
                    binding.errorButton.text = getString(R.string.reset_txt_btn)
                } else if (error.response()?.code() == 401) {
                    viewModel.logout()
                    (requireActivity() as MainActivity).logout()
                    Firebase.messaging.unsubscribeFromTopic("promo")

                    binding.errorTypeText.text = error.response()?.code().toString()
                    binding.errorTypeInfo.text = error.response()?.message()
                    binding.errorButton.text = getString(R.string.retry_txt_btn)
                } else {
                    binding.errorTypeText.text = error.response()?.code().toString()
                    binding.errorTypeInfo.text = error.response()?.message()
                    binding.errorButton.text = getString(R.string.refresh_txt_btn)
                }
            }

            is IOException -> {
                binding.errorTypeText.text = getString(R.string.connection_error_state)
                binding.errorTypeInfo.text = getString(R.string.connection_state_description)
                binding.errorButton.text = getString(R.string.refresh_txt_btn)
            }

            else -> {
                binding.errorTypeText.text = "Error"
                binding.errorTypeInfo.text = error.message
                binding.errorButton.text = getString(R.string.refresh_txt_btn)
            }
        }
    }

    private fun refreshAfterError() {
        binding.linearErrorLayout.visibility = View.INVISIBLE
        if (binding.errorButton.text == getString(R.string.reset_txt_btn)) {
            binding.chipGroup.removeAllViews()
            viewModel.resetParam()
            binding.searchInput.text?.clear()
        } else if (binding.errorButton.text == getString(R.string.retry_txt_btn)) {
            viewModel.logout()
            (requireActivity() as MainActivity).logout()
            Firebase.messaging.unsubscribeFromTopic("promo")
        }
        adapter.refresh()
        setShimmerLayout()
        binding.shimmerFilter.visibility = View.VISIBLE
        binding.swipeRefresh.visibility = View.INVISIBLE
        binding.chipsLayout.visibility = View.INVISIBLE
    }

    private fun translateChip(chipId: String): String? {
        val translateMapEn = mapOf(
            "Ulasan" to "Review",
            "Penjualan" to "Sale",
            "Harga Terendah" to "Lowest Price",
            "Harga Tertinggi" to "Highest Price"
        )

        val translateMapId = mapOf(
            "Review" to "Ulasan",
            "Sale" to "Penjualan",
            "Lowest Price" to "Harga Terendah",
            "Highest Price" to "Harga Tertinggi"
        )

        val isLangId = sharedPref.isLangID()
        val translateMap = if (isLangId) translateMapId else translateMapEn
        return translateMap[chipId]
    }
}
