package com.ayub.ecommerce.screens

import android.Manifest
import android.animation.ObjectAnimator
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.animation.OvershootInterpolator
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.animation.doOnEnd
import androidx.core.content.ContextCompat
import androidx.core.os.LocaleListCompat
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.navigation.fragment.NavHostFragment
import com.ayub.core.datasource.local.SharedPref
import com.ayub.ecommerce.R
import com.ayub.ecommerce.databinding.ActivityMainBinding
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var sharedPref: SharedPref
    private val viewModel: MainViewModel by viewModels()

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    override fun onCreate(savedInstanceState: Bundle?) {
        installSplashScreen().apply {
            setOnExitAnimationListener { screen ->
                val zoomX = ObjectAnimator.ofFloat(
                    screen.iconView,
                    View.SCALE_X,
                    0.4f,
                    0.0f
                )
                zoomX.interpolator = OvershootInterpolator()
                zoomX.duration = 500L
                zoomX.doOnEnd { screen.remove() }

                val zoomY = ObjectAnimator.ofFloat(
                    screen.iconView,
                    View.SCALE_Y,
                    0.4f,
                    0.0f
                )
                zoomY.interpolator = OvershootInterpolator()
                zoomY.duration = 500L
                zoomY.doOnEnd { screen.remove() }

                zoomX.start()
                zoomY.start()
            }
        }
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        sharedPref = SharedPref(this)

        isLoggedIn()
        checkDarkMode()
        checkLanguage()

        if (!isNotificationPermissionsGranted()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                pushNotificationPermissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS)
            }
        }
    }

    private fun isLoggedIn() {
        if (viewModel.getAccessToken().isNullOrEmpty()) {
            val navHostFragment =
                supportFragmentManager.findFragmentById(R.id.fcv_main) as NavHostFragment
            val navController = navHostFragment.navController
            navController.navigate(R.id.action_global_prelogin_navigation)
        }
    }

    private fun checkDarkMode() {
        AppCompatDelegate.setDefaultNightMode(
            if (sharedPref.isDarkModeEnabled()) AppCompatDelegate.MODE_NIGHT_YES else AppCompatDelegate.MODE_NIGHT_NO
        )
    }

    // check language running first
    private fun checkLanguage() {
        val isLanguageIn = sharedPref.isLangID()
        val languageToSet = if (isLanguageIn) "in" else "en"
        val languageToApply: LocaleListCompat = LocaleListCompat.forLanguageTags(languageToSet)

        AppCompatDelegate.setApplicationLocales(languageToApply)
    }

    private val pushNotificationPermissionLauncher = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { granted ->
        if (granted) {
            Snackbar.make(binding.root, "Permintaan izin diterima", Snackbar.LENGTH_SHORT)
                .show()
        } else {
            Snackbar.make(binding.root, "Permintaan izin ditolak", Snackbar.LENGTH_SHORT).show()
        }
    }

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    private fun isNotificationPermissionsGranted() =
        ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.POST_NOTIFICATIONS
        ) == PackageManager.PERMISSION_GRANTED

    fun logout() {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.fcv_main) as NavHostFragment
        val navController = navHostFragment.navController
        navController.navigate(R.id.action_global_prelogin_navigation)
    }

    fun toDetailFragment(bundle: Bundle) {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.fcv_main) as NavHostFragment
        val navController = navHostFragment.navController
        // navController.navigate(R.id.action_mainFragment_to_detailFragment, bundle)
        navController.navigate(R.id.action_mainFragment_to_detailFragmentCompose, bundle)
    }

    fun cartToDetail(bundle: Bundle) {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.fcv_main) as NavHostFragment
        val navController = navHostFragment.navController
        // navController.navigate(R.id.action_cartFragment_to_detailFragment, bundle)
        navController.navigate(R.id.action_cartFragment_to_detailFragmentCompose, bundle)
    }

    fun toStatus(bundle: Bundle) {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.fcv_main) as NavHostFragment
        val navController = navHostFragment.navController
        navController.navigate(R.id.action_mainFragment_to_statusFragment, bundle)
    }
}
