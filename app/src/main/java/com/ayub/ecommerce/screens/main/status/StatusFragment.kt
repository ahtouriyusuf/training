package com.ayub.ecommerce.screens.main.status

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.ayub.core.datasource.network.request_response.BaseResponse
import com.ayub.ecommerce.R
import com.ayub.ecommerce.databinding.FragmentStatusBinding
import com.ayub.ecommerce.screens.main.MainFragment
import com.ayub.ecommerce.utils.toCurrencyFormat
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class StatusFragment : Fragment() {
    private var _binding: FragmentStatusBinding? = null
    private val binding get() = _binding!!
    private val viewModel: StatusViewModel by viewModels()
    private val firebaseAnalytics: FirebaseAnalytics = Firebase.analytics

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    navigateWithoutRating()
                }
            }
        )
        // Inflate the layout for this fragment
        _binding = FragmentStatusBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setView()

        binding.btnFinish.setOnClickListener {
            sendRating()
            firebaseAnalytics.logEvent("btn_send_rating", null)
        }
    }

    private fun setView() {
        binding.apply {
            tvTransactionId.text = viewModel.detailTransaction?.invoiceId
            tvTransactionStatus.text =
                getString(
                    if (viewModel.detailTransaction?.status!!) {
                        R.string.transaction_status_success
                    } else {
                        R.string.transaction_status_fail
                    }
                )
            tvTransactionDate.text = viewModel.detailTransaction?.date
            tvTransactionTime.text = viewModel.detailTransaction?.time
            tvTransactionPaymentMethod.text = viewModel.detailTransaction?.payment
            tvTransactionTotalPayment.text = viewModel.detailTransaction?.total?.toCurrencyFormat()
            ratingBarScore.rating = viewModel.rating?.toFloat() ?: 0F
            editTextReview.setText(viewModel.review)
        }
    }

    private fun sendRating() {
        val rating: Int? = with(binding.ratingBarScore.rating.toInt()) {
            if (this == 0) {
                null
            } else {
                this
            }
        }
        val review: String? = with(binding.editTextReview.text.toString()) {
            this.ifEmpty { null }
        }
        val invoiceId = viewModel.detailTransaction?.invoiceId!!
        viewModel.sendRating(invoiceId, rating, review).observe(viewLifecycleOwner) {
            when (it) {
                is BaseResponse.Loading -> {
                    binding.btnFinish.visibility = View.INVISIBLE
                    binding.loadingRating.visibility = View.VISIBLE
                }

                is BaseResponse.Success -> {
                    binding.btnFinish.visibility = View.VISIBLE
                    binding.loadingRating.visibility = View.GONE
                    navigateNextPage()
                }

                is BaseResponse.Error -> {
                    binding.btnFinish.visibility = View.VISIBLE
                    binding.loadingRating.visibility = View.GONE
                    val message = it.error?.message.toString()
                    Snackbar.make(requireView(), message, Snackbar.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun navigateNextPage() {
        if (viewModel.originFragment != null) {
            val isNewData = bundleOf(MainFragment.IS_NEW_DATA_KEY to true)
            findNavController().navigate(R.id.action_statusFragment_to_mainFragment, isNewData)
        } else {
            findNavController().navigate(R.id.action_statusFragment_to_mainFragment)
        }
    }

    private fun navigateWithoutRating() {
        if (viewModel.originFragment != null) {
            findNavController().navigateUp()
        } else {
            findNavController().navigate(R.id.action_statusFragment_to_mainFragment)
        }
    }

    companion object {
        const val RATING_BUNDLE_KEY = "rating_bundle_key"
        const val REVIEW_BUNDLE_KEY = "review_bundle_key"
        const val DATA_BUNDLE_KEY = "data_bundle_key"
        const val ORIGIN_FRAGMENT_KEY = "origin_fragment_key"
    }
}
