package com.ayub.ecommerce.screens.main

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.ayub.core.repository.IRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainFViewModel @Inject constructor(
    private val repo: IRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {
    var isNewData = savedStateHandle.get<Boolean>(MainFragment.IS_NEW_DATA_KEY) ?: false

    fun getUsername() = repo.getUsername()
}
