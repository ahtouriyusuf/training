package com.ayub.ecommerce.screens.main.checkout

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.ayub.core.datasource.local.room.entity.Cart
import com.ayub.core.datasource.network.request_response.BaseResponse
import com.ayub.ecommerce.R
import com.ayub.ecommerce.databinding.FragmentCheckoutBinding
import com.ayub.ecommerce.screens.main.payment.PaymentFragment
import com.ayub.ecommerce.screens.main.status.StatusFragment
import com.ayub.ecommerce.utils.toCurrencyFormat
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CheckoutFragment : Fragment() {
    private val firebaseAnalytics: FirebaseAnalytics = Firebase.analytics
    private var _binding: FragmentCheckoutBinding? = null
    private val binding get() = _binding!!
    private val viewModel: CheckoutViewModel by viewModels()

    private var dataProduct = listOf<Cart>()
    private lateinit var adapter: CheckoutAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCheckoutBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = CheckoutAdapter(viewModel)
        binding.rvCheckout.adapter = adapter
        binding.rvCheckout.layoutManager = LinearLayoutManager(context)

        observeData()
        binding.totalPriceTV.text = viewModel.totalPrice.toCurrencyFormat()
    }

    private fun observeData() {
        viewModel.listData.observe(viewLifecycleOwner) {
            dataProduct = it
            adapter.submitList(dataProduct)
            setView(dataProduct)
            itemActions()
        }

        setFragmentResultListener(PaymentFragment.REQUEST_KEY) { _, bundle ->
            viewModel.paymentItem =
                bundle.getParcelable(PaymentFragment.BUNDLE_PAYMENT_KEY)
            setPayment()
        }
    }

    private fun setPayment() {
        if (viewModel.paymentItem != null) {
            Glide.with(binding.root.context)
                .load(viewModel.paymentItem!!.image)
                .fitCenter()
                .into(binding.ivCheckoutPayment)
            binding.tvCheckoutPayment.text = viewModel.paymentItem!!.label
            binding.payBtn.isEnabled = true
        }
    }

    private fun itemActions() {
        binding.topAppBar.setOnClickListener {
            findNavController().navigateUp()
            firebaseAnalytics.logEvent("btn_back_from_checkout", null)
        }

        binding.cardPayment.setOnClickListener {
            findNavController().navigate(R.id.action_checkoutFragment_to_paymentFragment)
            firebaseAnalytics.logEvent("btn_toPaymentMethod", null)
        }

        adapter.setOnItemClickCallback(object : CheckoutAdapter.OnItemClickCallback {
            override fun onItemClick(label: List<Cart>) {
                label.forEach { checkoutItem ->
                    val productToUpdate = dataProduct.find {
                        it.productId == checkoutItem.productId
                    }
                    productToUpdate?.quantity = checkoutItem.quantity
                }
                setView(viewModel.listData.value!!)
            }
        })

        binding.payBtn.setOnClickListener {
            actionPay()
        }
    }

    private fun setView(listCart: List<Cart>) {
        viewModel.totalPrice = listCart.sumOf {
            (it.productPrice + it.variantPrice!!) * it.quantity
        }

        binding.totalPriceTV.text = viewModel.totalPrice.toCurrencyFormat()
    }

    private fun actionPay() {
        val payment = viewModel.paymentItem?.label
        val checkoutProductId = viewModel.listData.value

        viewModel.makePayment(payment!!, checkoutProductId!!).observe(viewLifecycleOwner) {
            var bundle = arrayOf(bundleOf())

            viewModel.listData.value?.forEach {
                bundle += bundleOf(
                    FirebaseAnalytics.Param.ITEM_ID to it.productId,
                    FirebaseAnalytics.Param.ITEM_NAME to it.productName,
                    FirebaseAnalytics.Param.ITEM_VARIANT to it.variantName,
                    FirebaseAnalytics.Param.PRICE to it.productPrice,
                    FirebaseAnalytics.Param.CURRENCY to "IDR"
                )
            }

            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.BEGIN_CHECKOUT) {
                param(FirebaseAnalytics.Param.ITEMS, bundle)
            }

            when (it) {
                is BaseResponse.Loading -> {
                    binding.apply {
                        payBtn.visibility = View.GONE
                        progressIndicator.visibility = View.VISIBLE
                    }
                }

                is BaseResponse.Success -> {
                    binding.apply {
                        payBtn.visibility = View.VISIBLE
                        progressIndicator.visibility = View.GONE
                    }
                    val invoiceId = it.data?.invoiceId
                    firebaseAnalytics.logEvent(FirebaseAnalytics.Event.PURCHASE) {
                        param(FirebaseAnalytics.Param.TRANSACTION_ID, invoiceId.toString())
                        param(FirebaseAnalytics.Param.ITEMS, bundle)
                        param(
                            FirebaseAnalytics.Param.CURRENCY,
                            it.data?.total!!.toCurrencyFormat()
                        )
                    }

                    val transactionData = bundleOf(StatusFragment.DATA_BUNDLE_KEY to it.data)
                    findNavController().navigate(
                        R.id.action_checkoutFragment_to_statusFragment,
                        transactionData
                    )
                }

                is BaseResponse.Error -> {
                    binding.apply {
                        payBtn.visibility = View.VISIBLE
                        progressIndicator.visibility = View.GONE
                    }
                    Snackbar.make(
                        requireView(),
                        it.error?.message.toString(),
                        Snackbar.LENGTH_SHORT
                    )
                        .show()
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    companion object {
        const val ARG_DATA = "ListData"
    }
}
