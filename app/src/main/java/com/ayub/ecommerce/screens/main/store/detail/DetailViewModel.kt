package com.ayub.ecommerce.screens.main.store.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ayub.core.datasource.local.room.entity.Cart
import com.ayub.core.datasource.network.request_response.BaseResponse
import com.ayub.core.datasource.network.request_response.DetailProductData
import com.ayub.core.datasource.network.request_response.ProductVariantItem
import com.ayub.core.helper.toCart
import com.ayub.core.helper.toWishlist
import com.ayub.core.repository.IRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(
    private val repo: IRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {
    var productVariant: ProductVariantItem? = null
    var detailProduct: DetailProductData? = null
    var productName: String? = null
    var productPrice: String? = null
    var isWishlist: Boolean = false
    var isInCart: Boolean = false
    var idProduct = savedStateHandle.get<String>(DetailFragmentCompose.BUNDLE_PRODUCT_ID_KEY)

    private val _detailProductData = MutableLiveData<BaseResponse<DetailProductData>>(null)
    val detailProductData: LiveData<BaseResponse<DetailProductData>> = _detailProductData

    val productDetail = repo.getDetailProduct(idProduct)

    init {
        getDetailProduct()
    }

    fun getDetailProduct() {
        viewModelScope.launch {
            repo.getDetailProduct(idProduct).observeForever {
                _detailProductData.value = it
            }
        }
    }

    fun insertWishlist() {
        if (detailProduct != null) {
            viewModelScope.launch {
                repo.saveToWishlist(detailProduct!!.toWishlist(productVariant!!))
            }
            isWishlist = true
        }
    }

    fun insertWishlistCompose(variantItem: ProductVariantItem?) {
        if (detailProduct != null) {
            viewModelScope.launch {
                repo.saveToWishlist(detailProduct!!.toWishlist(variantItem!!))
            }
            isWishlist = true
        }
    }

    fun deleteFromWishlist() {
        if (detailProduct != null) {
            viewModelScope.launch {
                repo.deleteFromWishlist(detailProduct!!.toWishlist(productVariant!!))
            }
            isWishlist = false
        }
    }

    fun deleteFromWishlistCompose(variantItem: ProductVariantItem?) {
        if (detailProduct != null) {
            viewModelScope.launch {
                repo.deleteFromWishlist(detailProduct!!.toWishlist(variantItem!!))
            }
            isWishlist = false
        }
    }

    suspend fun checkProductInWishlist(): Boolean {
        return repo.checkProductInWishlist(idProduct.toString())
    }

    fun addToCart(variantItem: ProductVariantItem) {
        if (detailProduct != null) {
            viewModelScope.launch {
                repo.addToCart(detailProduct!!.toCart(variantItem))
            }
            isInCart = true
        }
    }

    suspend fun getProductFromCart(id: String): Cart? {
        return repo.getCartProductById(id)
    }

    fun updateProductQuantity(id: String, newQuantity: Int) {
        viewModelScope.launch {
            repo.updateCartItemQuantity(id, newQuantity)
        }
    }

    suspend fun checkProductInCart(): Boolean {
        return repo.checkProductInCart(idProduct.toString())
    }

    fun logout() = repo.logout()
}
