package com.ayub.ecommerce.screens.main.store

import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.ayub.core.datasource.network.request_response.Items
import com.ayub.ecommerce.R
import com.ayub.ecommerce.databinding.ItemProductGridBinding
import com.ayub.ecommerce.databinding.ItemProductLinearBinding
import com.ayub.ecommerce.utils.ProductComparator
import com.ayub.ecommerce.utils.toCurrencyFormat
import com.bumptech.glide.Glide

class StoreAdapter : PagingDataAdapter<Items, RecyclerView.ViewHolder>(ProductComparator) {

    var viewType = ONE_COLUMN_VIEW_TYPE
    private var onItemClickCallback: OnItemClickCallback? = null

    override fun getItemViewType(position: Int): Int {
        return viewType
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        getItem(position)?.let {
            (holder as ProductViewHolder<*>).bind(it)
        }
        if (viewType == ONE_COLUMN_VIEW_TYPE){
            holder.itemView.startAnimation(
                AnimationUtils.loadAnimation(
                    holder.itemView.context,
                    R.anim.list_item_animation
                )
            )
        } else {
            holder.itemView.startAnimation(
                AnimationUtils.loadAnimation(
                    holder.itemView.context,
                    R.anim.grid_item_animation
                )
            )
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == ONE_COLUMN_VIEW_TYPE) {
            val binding =
                ItemProductLinearBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            return ProductViewHolder(binding, viewType, onItemClickCallback)
        } else {
            val binding =
                ItemProductGridBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            return ProductViewHolder(binding, viewType, onItemClickCallback)
        }
    }

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    interface OnItemClickCallback {
        fun onItemClicked(data: Items)
    }

    class ProductViewHolder<T : ViewBinding>(
        private val binding: T,
        private val viewType: Int,
        private val onItemClickCallback: OnItemClickCallback?
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Items) {
            itemView.setOnClickListener {
                onItemClickCallback?.onItemClicked(data)
            }
            if (viewType == ONE_COLUMN_VIEW_TYPE) {
                with(binding as ItemProductLinearBinding) {
                    Glide.with(binding.root.context)
                        .load(data.image)
                        .fitCenter()
                        .into(binding.ivProduct)
                    tvProductName.text = data.productName
                    tvProductPrice.text = data.productPrice?.toCurrencyFormat()
                    tvStore.text = data.store
                    tvRating.text = data.productRating.toString()
                    tvSell.text = data.sale.toString()
                }
            } else {
                with(binding as ItemProductGridBinding) {
                    Glide.with(binding.root.context)
                        .load(data.image)
                        .fitCenter()
                        .into(binding.ivProduct)
                    tvProductName.text = data.productName
                    tvProductPrice.text = data.productPrice?.toCurrencyFormat()
                    tvStore.text = data.store
                    tvRating.text = data.productRating.toString()
                    tvSell.text = data.sale.toString()
                }
            }
        }
    }

    companion object {
        const val ONE_COLUMN_VIEW_TYPE = 1
        const val MORE_COLUMN_VIEW_TYPE = 2
    }
}
