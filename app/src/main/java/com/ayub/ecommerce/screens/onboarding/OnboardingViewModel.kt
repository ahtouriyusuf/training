package com.ayub.ecommerce.screens.onboarding

import androidx.lifecycle.ViewModel
import com.ayub.core.repository.IRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@HiltViewModel
class OnboardingViewModel @Inject constructor(private val repo: IRepository) : ViewModel() {

    fun setIsFirstLaunchToFalse() {
        runBlocking {
            repo.setIsFirstLaunchToFalse()
        }
    }
}
