package com.ayub.ecommerce.screens.profile

import android.Manifest
import android.net.Uri
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.ColorInt
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.ayub.core.datasource.network.request_response.BaseResponse
import com.ayub.ecommerce.R
import com.ayub.ecommerce.databinding.FragmentProfileBinding
import com.ayub.ecommerce.utils.PhotoManager
import com.ayub.ecommerce.utils.PhotoManager.reduceFileImage
import com.ayub.ecommerce.utils.PhotoManager.uriToFile
import com.ayub.ecommerce.utils.getApiErrorMessage
import com.ayub.ecommerce.utils.hideKeyboard
import com.bumptech.glide.Glide
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File

@AndroidEntryPoint
class ProfileFragment : Fragment() {
    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!
    private val viewModel: ProfileViewModel by viewModels()
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private lateinit var currentImageUri: Uri
    private var file: File? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        firebaseAnalytics = Firebase.analytics

        changeSpannedTextColor()

        binding.nameValueTI.doOnTextChanged { _, _, _, _ -> enablesubmitButton() }

        binding.profileBg.setOnClickListener {
            firebaseAnalytics.logEvent("btn_add_image_action", null)
            val options = arrayOf("Kamera", "Galeri")
            MaterialAlertDialogBuilder(
                requireContext(),
            )
                .setTitle("Pilih Gambar")
                .setBackground(
                    ContextCompat.getDrawable(
                        requireActivity(),
                        R.drawable.bg_alert_dialog
                    )
                )
                .setItems(options) { _, which ->
                    if (options[which] == "Kamera") {
                        if (!PhotoManager.isCameraPermissionsGranted(requireActivity())) {
                            requestCameraPermissionLauncher.launch(Manifest.permission.CAMERA)
                        } else {
                            startCamera()
                        }
                    } else {
                        startGalery()
                    }
                }
                .show()
        }

        binding.submitBtn.setOnClickListener {
            activity?.hideKeyboard(it)
            saveProfile(file)
            firebaseAnalytics.logEvent("btn_submit_profile_action", null)
        }

        enablesubmitButton()
    }

    private fun enablesubmitButton() {
        val name = binding.nameValueTI.text.toString()
        binding.submitBtn.isEnabled = name.isNotEmpty()
    }

    private fun changeSpannedTextColor() {
        val typedValue = TypedValue()
        val theme = requireContext().theme
        theme.resolveAttribute(androidx.appcompat.R.attr.colorPrimary, typedValue, true)
        @ColorInt val color = typedValue.data

        val spannable = SpannableString(binding.termsTv.text)

        spannable.setSpan(
            ForegroundColorSpan(color),
            38,
            55,
            Spannable.SPAN_EXCLUSIVE_INCLUSIVE
        )

        spannable.setSpan(
            ForegroundColorSpan(color),
            63,
            79,
            Spannable.SPAN_EXCLUSIVE_INCLUSIVE
        )

        binding.termsTv.text = spannable
    }

    private fun startCamera() {
        currentImageUri = PhotoManager.getImageUri(requireActivity())
        cameraIntentLauncher.launch(currentImageUri)
    }

    private val cameraIntentLauncher = registerForActivityResult(
        ActivityResultContracts.TakePicture()
    ) { isSuccess ->
        if (isSuccess) {
            showImage()
            val file = uriToFile(currentImageUri, requireContext())
            val reducedFile = reduceFileImage(file)
            this.file = reducedFile
        } else {
            Log.d("Take Camera", "Photo not taken")
        }
    }

    private val requestCameraPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { isGranted: Boolean ->
            if (isGranted) {
                Snackbar.make(binding.root, "Permintaan izin diterima", Snackbar.LENGTH_SHORT)
                    .show()
                startCamera()
            } else {
                Snackbar.make(binding.root, "Permintaan izin ditolak", Snackbar.LENGTH_SHORT).show()
            }
        }

    private fun startGalery() {
        launcherGallery.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly))
    }

    val launcherGallery = registerForActivityResult(
        ActivityResultContracts.PickVisualMedia()
    ) { uri: Uri? ->
        if (uri != null) {
            val file = uriToFile(uri, requireContext())
            val reducedFile = reduceFileImage(file)
            this.file = reducedFile
            currentImageUri = uri
            showImage()
        } else {
            Log.d("Photo Picker", "No media selected")
        }
    }

    private fun showImage() {
        currentImageUri.let {
            Log.d("Image URI", "showImage: $it")
            binding.profileIcon.isVisible = false
            Glide.with(binding.root.context)
                .load(it)
                .fitCenter()
                .circleCrop()
                .into(binding.profileBg)
        }
    }

    private fun saveProfile(file: File?) {
        binding.apply {
            progressIndicator.visibility = View.VISIBLE
            val requestUser =
                MultipartBody.Part.createFormData("userName", binding.nameValueTI.text.toString())
            val requestImage = file?.let {
                MultipartBody.Part.createFormData(
                    "userImage",
                    file.name,
                    file.asRequestBody("image/jpeg".toMediaTypeOrNull())
                )
            }
            viewModel.saveProfile(requestUser, requestImage).observe(viewLifecycleOwner) {
                when (it) {
                    is BaseResponse.Success -> {
                        progressIndicator.visibility = View.INVISIBLE
                        submitBtn.isEnabled = true
                        findNavController().navigate(R.id.action_profileFragment_to_mainFragment)
                    }

                    is BaseResponse.Error -> {
                        progressIndicator.visibility = View.INVISIBLE
                        submitBtn.isEnabled = true
                        processError(getApiErrorMessage(it.error!!) ?: "Unknown Error")
                    }

                    is BaseResponse.Loading -> {
                        progressIndicator.visibility = View.VISIBLE
                        submitBtn.isEnabled = false
                    }
                }
            }
        }
    }

    private fun processError(msg: CharSequence) {
        Snackbar.make(requireView(), msg, Snackbar.LENGTH_SHORT).show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
