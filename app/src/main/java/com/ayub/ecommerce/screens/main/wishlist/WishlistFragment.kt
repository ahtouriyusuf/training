package com.ayub.ecommerce.screens.main.wishlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.ayub.core.datasource.local.room.entity.WishlistProduct
import com.ayub.ecommerce.R
import com.ayub.ecommerce.databinding.FragmentWishlistBinding
import com.ayub.ecommerce.screens.MainActivity
import com.ayub.ecommerce.screens.main.store.StoreAdapter
import com.ayub.ecommerce.utils.toCurrencyFormat
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.runBlocking

@AndroidEntryPoint
class WishlistFragment : Fragment() {
    private var _binding: FragmentWishlistBinding? = null
    private val binding get() = _binding!!
    private val viewModel: WishlistViewModel by viewModels()
    private lateinit var adapter: WishlistAdapter
    private val firebaseAnalytics: FirebaseAnalytics = Firebase.analytics

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentWishlistBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = WishlistAdapter()
        binding.rvWishlist.adapter = adapter
        observeWishlist()
        setImageBtnChangeRecyclerView()

        binding.ivLayout.setOnClickListener {
            actionChangeRecyclerView()
        }

        adapter.setOnItemClickCallback(object : WishlistAdapter.OnItemClickCallback {
            override fun onItemClicked(data: WishlistProduct) {
                val bundle = bundleOf("id" to data.productId)
                (requireActivity() as MainActivity).toDetailFragment(bundle)
                firebaseAnalytics.logEvent("btn_toDetailProduct", null)
            }

            override fun deleteCallback(data: WishlistProduct) {
                viewModel.deleteWishlist(data)
                firebaseAnalytics.logEvent("btn_remove_product_from_wishlist", null)
            }

            override fun addToCart(data: WishlistProduct) {
                runBlocking {
                    actionCart(data)
                }
            }
        })
    }

    private fun observeWishlist() {
        viewModel.getWishlistData.observe(viewLifecycleOwner) {
            adapter.submitList(it)

            if (it.isEmpty()) {
                binding.linearErrorLayout.visibility = View.VISIBLE
                binding.rvWishlist.visibility = View.GONE
                binding.itemCountLayout.visibility = View.GONE
            } else {
                binding.linearErrorLayout.visibility = View.GONE
                binding.itemCountLayout.visibility = View.VISIBLE
                binding.rvWishlist.visibility = View.VISIBLE
                binding.wishlistItemCount.text = it.size.toString()
                setRecyclerView()
            }
        }
    }

    private fun actionChangeRecyclerView() {
        viewModel.recyclerViewType =
            if (viewModel.recyclerViewType == StoreAdapter.ONE_COLUMN_VIEW_TYPE) {
                StoreAdapter.MORE_COLUMN_VIEW_TYPE
            } else {
                StoreAdapter.ONE_COLUMN_VIEW_TYPE
            }
        setRecyclerView()
        setImageBtnChangeRecyclerView()
    }

    private fun setImageBtnChangeRecyclerView() {
        binding.ivLayout.setImageResource(
            if (viewModel.recyclerViewType == StoreAdapter.ONE_COLUMN_VIEW_TYPE) {
                R.drawable.ic_linear_view
            } else {
                R.drawable.ic_grid_view
            }
        )
    }

    private fun setRecyclerView() {
        if (viewModel.recyclerViewType == WishlistAdapter.ONE_COLUMN_VIEW_TYPE) {
            adapter.viewType = WishlistAdapter.ONE_COLUMN_VIEW_TYPE
            binding.rvWishlist.layoutManager = LinearLayoutManager(requireContext())
        } else {
            adapter.viewType = WishlistAdapter.MORE_COLUMN_VIEW_TYPE
            binding.rvWishlist.layoutManager = GridLayoutManager(requireContext(), 2)
        }
    }

    private suspend fun actionCart(data: WishlistProduct) {
        val checkProductId = viewModel.getProductFromCart(data.productId)?.productId
        if (checkProductId != null) {
            var quantity = viewModel.getProductFromCart(checkProductId)?.quantity ?: 0
            val stock = viewModel.getProductFromCart(checkProductId)?.stock ?: 0
            if (quantity < stock) {
                quantity++
                viewModel.updateProductQuantity(checkProductId, quantity)
                Snackbar.make(
                    requireView(),
                    getString(R.string.add_quantity_success_txt),
                    Snackbar.LENGTH_SHORT
                ).show()
            } else {
                Snackbar.make(
                    requireView(),
                    getString(R.string.item_quantity_maxed_txt),
                    Snackbar.LENGTH_SHORT
                ).show()
            }
        } else {
            viewModel.addToCart(data)
            Snackbar.make(
                requireView(),
                getString(R.string.add_cart_success),
                Snackbar.LENGTH_SHORT
            ).show()
        }
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_TO_CART) {
            val bundle = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_ID, data.productId)
                putString(FirebaseAnalytics.Param.ITEM_NAME, data.productName)
                param(FirebaseAnalytics.Param.CURRENCY, data.productPrice!!.toCurrencyFormat())
                putString(
                    FirebaseAnalytics.Param.ITEM_VARIANT,
                    data.variantName
                )
            }
            param(FirebaseAnalytics.Param.ITEMS, bundle)
        }
    }
}
