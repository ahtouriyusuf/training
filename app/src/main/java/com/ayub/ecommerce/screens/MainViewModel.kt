package com.ayub.ecommerce.screens

import androidx.lifecycle.ViewModel
import com.ayub.core.repository.IRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val repo: IRepository) : ViewModel() {
    fun getAccessToken() = repo.getAccessToken()
}
