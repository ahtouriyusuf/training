package com.ayub.ecommerce.screens.main.transaction

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.ayub.core.datasource.network.request_response.BaseResponse
import com.ayub.core.datasource.network.request_response.TransactionData
import com.ayub.core.helper.toFulfillmentData
import com.ayub.ecommerce.R
import com.ayub.ecommerce.databinding.FragmentTransactionBinding
import com.ayub.ecommerce.screens.MainActivity
import com.ayub.ecommerce.screens.main.status.StatusFragment
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import dagger.hilt.android.AndroidEntryPoint
import retrofit2.HttpException
import java.io.IOException

@AndroidEntryPoint
class TransactionFragment : Fragment() {
    private var _binding: FragmentTransactionBinding? = null
    private val binding get() = _binding!!
    private val viewModel: TransactionViewModel by viewModels()
    private lateinit var adapter: TransactionAdapter
    private val firebaseAnalytics: FirebaseAnalytics = Firebase.analytics

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentTransactionBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeTransaction()
    }

    private fun itemAction() {
        adapter.setOnItemClickCallback(object : TransactionAdapter.OnItemClickCallback {
            override fun toReview(data: TransactionData) {
                val bundle = Bundle().apply {
                    putParcelable(StatusFragment.DATA_BUNDLE_KEY, data.toFulfillmentData())
                    putInt(StatusFragment.RATING_BUNDLE_KEY, data.rating ?: 0)
                    putString(StatusFragment.REVIEW_BUNDLE_KEY, data.review)
                    putString(StatusFragment.ORIGIN_FRAGMENT_KEY, "transaction")
                }
                (requireActivity() as MainActivity).toStatus(bundle)
                firebaseAnalytics.logEvent("btn_toRating", null)
            }
        })
    }

    private fun observeTransaction() {
        viewModel.transactionData.observe(viewLifecycleOwner) {
            when (it) {
                is BaseResponse.Loading -> {
                    binding.apply {
                        loadingIndicator.visibility = View.VISIBLE
                        rvTransaction.visibility = View.GONE
                        linearErrorLayout.visibility = View.GONE
                    }
                }

                is BaseResponse.Success -> {
                    adapter = TransactionAdapter(it.data)
                    binding.rvTransaction.adapter = adapter
                    binding.rvTransaction.layoutManager = LinearLayoutManager(requireContext())
                    (binding.rvTransaction.layoutManager as LinearLayoutManager).reverseLayout =
                        true
                    (binding.rvTransaction.layoutManager as LinearLayoutManager).stackFromEnd = true
                    binding.apply {
                        loadingIndicator.visibility = View.GONE
                        rvTransaction.visibility = View.VISIBLE
                        linearErrorLayout.visibility = View.GONE
                    }
                    itemAction()
                }

                is BaseResponse.Error -> {
                    binding.apply {
                        loadingIndicator.visibility = View.GONE
                        rvTransaction.visibility = View.GONE
                        linearErrorLayout.visibility = View.VISIBLE
                    }
                    val error = it.error!!
                    setErrorMessage(error)

                    binding.errorButton.setOnClickListener {
                        viewModel.getTransactionData()
                    }
                }
            }
        }
    }

    private fun setErrorMessage(error: Throwable) {
        when (error) {
            is HttpException -> {
                if (error.response()?.code() == 404) {
                    binding.errorTypeText.text = getString(R.string.empty_list_state)
                    binding.errorTypeInfo.text = getString(R.string.empty_list_description)
                    binding.errorButton.text = getString(R.string.refresh_txt_btn)
                } else if (error.response()?.code() == 401) {
                    viewModel.logout()
                    (requireActivity() as MainActivity).logout()
                    Firebase.messaging.unsubscribeFromTopic("promo")

                    binding.errorTypeText.text = error.response()?.code().toString()
                    binding.errorTypeInfo.text = error.response()?.message()
                    binding.errorButton.text = getString(R.string.retry_txt_btn)
                } else {
                    binding.errorTypeText.text = error.response()?.code().toString()
                    binding.errorTypeInfo.text = error.response()?.message()
                    binding.errorButton.text = getString(R.string.refresh_txt_btn)
                }
            }

            is IOException -> {
                binding.errorTypeText.text = getString(R.string.connection_error_state)
                binding.errorTypeInfo.text = getString(R.string.connection_state_description)
                binding.errorButton.text = getString(R.string.refresh_txt_btn)
            }

            else -> {
                binding.errorTypeText.text = "Error"
                binding.errorTypeInfo.text = error.message
                binding.errorButton.text = getString(R.string.refresh_txt_btn)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
