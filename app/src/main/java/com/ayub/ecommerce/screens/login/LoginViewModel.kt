package com.ayub.ecommerce.screens.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import com.ayub.core.datasource.network.request_response.BaseResponse
import com.ayub.core.datasource.network.request_response.LoginData
import com.ayub.core.repository.IRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(private val repo: IRepository) : ViewModel() {

    private val _loginResult: MutableLiveData<Map<String, String>> = MutableLiveData()
    val loginResult: LiveData<BaseResponse<LoginData>>
        get() = _loginResult.switchMap { map ->
            repo.login(map["email"]!!, map["password"]!!, map["firebase_token"]!!)
        }

    fun login(email: String, pwd: String, firebaseToken: String) {
        _loginResult.value =
            mapOf("email" to email, "password" to pwd, "firebase_token" to firebaseToken)
    }

    fun isFirstLaunch() = repo.isFirstLaunch()

    fun getLang(): Boolean = repo.isLangId()
}
