package com.ayub.ecommerce.screens.main.store.detail

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FilterChip
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.PlatformTextStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.ayub.core.datasource.network.request_response.BaseResponse
import com.ayub.core.datasource.network.request_response.DetailProductData
import com.ayub.core.datasource.network.request_response.ProductVariantItem
import com.ayub.core.helper.toCart
import com.ayub.ecommerce.R
import com.ayub.ecommerce.screens.MainActivity
import com.ayub.ecommerce.screens.main.checkout.CheckoutFragment
import com.ayub.ecommerce.screens.main.store.detail.DetailFragmentCompose.Companion.productId
import com.ayub.ecommerce.screens.main.store.detail.DetailFragmentCompose.Companion.productName
import com.ayub.ecommerce.screens.main.store.review.ReviewFragmentCompose
import com.ayub.ecommerce.utils.compose.EcommerceTheme
import com.ayub.ecommerce.utils.compose.poppinsFamily
import com.ayub.ecommerce.utils.toCurrencyFormat
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.runBlocking
import retrofit2.HttpException
import java.io.IOException

@AndroidEntryPoint
class DetailFragmentCompose : Fragment() {
    private lateinit var composeView: ComposeView
    private val viewModel: DetailViewModel by viewModels()
    private val firebaseAnalytics: FirebaseAnalytics = Firebase.analytics

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        return ComposeView(requireContext()).also {
            composeView = it
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        composeView.setContent {
            EcommerceTheme {
                val detailState by viewModel.detailProductData.observeAsState(initial = BaseResponse.Loading)
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    DetailPage(
                        detailState = detailState,
                        viewModel = viewModel,
                        firebaseAnalytics = firebaseAnalytics,
                        actionBack = {
                            findNavController().navigateUp()
                            firebaseAnalytics.logEvent("btn_back_to_store", null)
                        },
                        toAllReview = { toReviewProduct() },
                        shareAction = { shareProduct() },
                        wishlistSnackbar = { showSnackbar() },
                        cartAction = { runBlocking { actionCart() } },
                        checkoutAction = { actionCheckout() },
                        logoutAction = { logout() }
                    )
                }
            }
        }
    }

    private fun toReviewProduct() {
        val bundle = bundleOf(
            ReviewFragmentCompose.BUNDLE_PRODUCT_ID_KEY to viewModel.idProduct
        )
        findNavController().navigate(
            R.id.action_detailFragmentCompose_to_reviewFragmentCompose,
            bundle
        )
        firebaseAnalytics.logEvent("btn_toReviewProduct", null)
    }

    private fun shareProduct() {
        val sendIntent: Intent = Intent().apply {
            val text = getString(
                R.string.share_product_txt,
                viewModel.productName,
                viewModel.productPrice,
                viewModel.idProduct
            ).trimIndent()
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, text)
            type = "text/plain"
        }

        val shareIntent = Intent.createChooser(sendIntent, null)
        startActivity(shareIntent)
        firebaseAnalytics.logEvent("btn_share_product", null)
    }

    private fun showSnackbar() {
        Snackbar.make(
            requireView(),
            getString(
                if (isChecked) R.string.add_to_wishlist_success else R.string.delete_from_wishlist_success
            ),
            Snackbar.LENGTH_SHORT
        ).show()
    }

    private suspend fun actionCart() {
        if (viewModel.isInCart) {
            var quantity = viewModel.getProductFromCart(productId!!)?.quantity ?: 0
            val stock = viewModel.getProductFromCart(productId!!)?.stock
            if (quantity < stock!!) {
                quantity++
                viewModel.updateProductQuantity(productId!!, quantity)
                Snackbar.make(
                    requireView(),
                    getString(R.string.add_quantity_success_txt),
                    Snackbar.LENGTH_SHORT
                ).show()
            } else {
                Snackbar.make(
                    requireView(),
                    getString(R.string.item_quantity_maxed_txt),
                    Snackbar.LENGTH_SHORT
                ).show()
            }
        } else {
            viewModel.addToCart(selectedVariant!!)
            Snackbar.make(
                requireView(),
                getString(R.string.add_cart_success),
                Snackbar.LENGTH_SHORT
            ).show()
        }
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_TO_CART) {
            val bundle = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_ID, productId)
                putString(FirebaseAnalytics.Param.ITEM_NAME, productName)
                param(FirebaseAnalytics.Param.CURRENCY, viewModel.productPrice!!)
                putString(
                    FirebaseAnalytics.Param.ITEM_VARIANT,
                    selectedVariant?.variantName
                )
            }
            param(FirebaseAnalytics.Param.ITEMS, bundle)
        }
    }

    private fun actionCheckout() {
        if (viewModel.detailProduct != null && selectedVariant != null) {
            val data = viewModel.detailProduct!!.toCart(selectedVariant!!)
            val bundle = Bundle().apply {
                putParcelableArrayList(CheckoutFragment.ARG_DATA, arrayListOf(data))
            }
            findNavController().navigate(
                R.id.action_detailFragmentCompose_to_checkoutFragment,
                bundle
            )
            firebaseAnalytics.logEvent("btn_send_product_to_checkout", null)
        }
    }

    private fun logout() {
        viewModel.logout()
        (requireActivity() as MainActivity).logout()
        Firebase.messaging.unsubscribeFromTopic("promo")
    }

    companion object {
        const val BUNDLE_PRODUCT_ID_KEY = "id"
        var isChecked: Boolean = false
        var productId: String? = null
        var productName: String? = null
        var selectedVariant: ProductVariantItem? = null
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DetailPage(
    detailState: BaseResponse<DetailProductData?>,
    viewModel: DetailViewModel,
    firebaseAnalytics: FirebaseAnalytics,
    actionBack: () -> Unit,
    toAllReview: () -> Unit,
    shareAction: () -> Unit,
    wishlistSnackbar: () -> Unit,
    cartAction: () -> Unit,
    checkoutAction: () -> Unit,
    logoutAction: () -> Unit,
) {
    Scaffold(
        topBar = {
            Column {
                TopAppBar(
                    title = {
                        Text(
                            text = stringResource(R.string.detail_product_title),
                            style = TextStyle(
                                fontFamily = poppinsFamily,
                                fontWeight = FontWeight.Normal,
                                fontSize = 22.sp,
                                platformStyle = PlatformTextStyle(
                                    includeFontPadding = false
                                )
                            ),
                        )
                    },
                    navigationIcon = {
                        IconButton(
                            onClick = actionBack
                        ) {
                            Icon(
                                painter = painterResource(id = R.drawable.ic_back),
                                contentDescription = "Back to Detail Fragment"
                            )
                        }
                    }
                )
                Divider(Modifier.height(1.dp))
            }
        },
        bottomBar = {
            if (detailState is BaseResponse.Success) {
                Column(
                    Modifier
                        .fillMaxWidth()
                        .wrapContentHeight()
                ) {
                    Divider(Modifier.height(1.dp))
                    Row(
                        Modifier
                            .padding(16.dp, 8.dp, 16.dp, 8.dp)
                            .fillMaxWidth(),
                        horizontalArrangement = Arrangement.spacedBy(16.dp)
                    ) {
                        OutlinedButton(
                            onClick = checkoutAction,
                            modifier = Modifier
                                .fillMaxWidth()
                                .weight(1f),

                        ) {
                            Text(stringResource(id = R.string.buy_now_btn))
                        }
                        Button(
                            onClick = cartAction,
                            modifier = Modifier
                                .fillMaxWidth()
                                .weight(1f),

                        ) {
                            Text(stringResource(id = R.string.add_cart_btn))
                        }
                    }
                }
            }
        },
        content = {
            Column(
                Modifier
                    .padding(it)
                    .fillMaxSize()
                    .background(Color.Transparent)
            ) {
                when (detailState) {
                    is BaseResponse.Loading -> LoadingScreen()
                    is BaseResponse.Success -> {
                        val checkProductInWishlist =
                            runBlocking { viewModel.checkProductInWishlist() }
                        DetailFragmentCompose.isChecked = checkProductInWishlist == true
                        viewModel.isInCart = runBlocking { viewModel.checkProductInCart() }
                        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM) {
                            param(
                                FirebaseAnalytics.Param.ITEM_ID,
                                detailState.data?.productId.toString()
                            )
                            param(
                                FirebaseAnalytics.Param.ITEM_NAME,
                                detailState.data?.productName.toString()
                            )
                        }
                        DetailProductContent(
                            detailState.data,
                            viewModel,
                            firebaseAnalytics,
                            toAllReview,
                            shareAction,
                            wishlistSnackbar
                        )
                    }

                    is BaseResponse.Error -> ErrorScreen(detailState.error, viewModel, logoutAction)
                }
            }
        }
    )
}

@Composable
fun LoadingScreen() {
    Column(
        Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        CircularProgressIndicator()
    }
}

@Composable
fun ErrorScreen(t: Throwable?, viewModel: DetailViewModel, logoutAction: () -> Unit) {
    Column(
        Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Column(
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                painter = painterResource(id = R.drawable.error_img),
                contentDescription = "error state",
                modifier = Modifier.size(128.dp)
            )
            when (t) {
                is HttpException -> {
                    if (t.response()?.code() == 404) {
                        ErrorContent(
                            code = stringResource(id = R.string.empty_list_state),
                            message = stringResource(
                                id = R.string.empty_list_description
                            ),
                            buttonText = stringResource(id = R.string.reset_txt_btn),
                            viewModel
                        )
                    } else if (t.response()?.code() == 401) {
                        logoutAction()
                    } else {
                        ErrorContent(
                            code = t.response()?.code().toString(),
                            message = t.response()?.message().toString(),
                            buttonText = stringResource(id = R.string.refresh_txt_btn),
                            viewModel
                        )
                    }
                }

                is IOException -> {
                    ErrorContent(
                        code = stringResource(id = R.string.connection_error_state),
                        message = stringResource(
                            id = R.string.connection_state_description
                        ),
                        buttonText = stringResource(id = R.string.refresh_txt_btn),
                        viewModel
                    )
                }

                else -> {
                    ErrorContent(
                        code = "Error",
                        message = t?.message.toString(),
                        buttonText = stringResource(
                            id = R.string.refresh_txt_btn
                        ),
                        viewModel
                    )
                }
            }
        }
    }
}

@Composable
fun ErrorContent(code: String, message: String, buttonText: String, viewModel: DetailViewModel) {
    Text(
        text = code,
        modifier = Modifier
            .padding(top = 8.dp),
        style = TextStyle(
            fontFamily = poppinsFamily,
            fontWeight = FontWeight.Medium,
            fontSize = 32.sp,
            platformStyle = PlatformTextStyle(
                includeFontPadding = false
            )
        )
    )
    Text(
        text = message,
        modifier = Modifier
            .padding(top = 4.dp),
        style = TextStyle(
            fontFamily = poppinsFamily,
            fontWeight = FontWeight.Normal,
            fontSize = 16.sp,
            platformStyle = PlatformTextStyle(
                includeFontPadding = false
            )
        )
    )
    Button(
        onClick = {
            viewModel.getDetailProduct()
                  },
        modifier = Modifier.padding(top = 8.dp)
    ) {
        Text(
            text = buttonText
        )
    }
}

@Composable
fun DetailProductContent(
    data: DetailProductData?,
    viewModel: DetailViewModel,
    firebaseAnalytics: FirebaseAnalytics,
    toAllReview: () -> Unit,
    shareAction: () -> Unit,
    wishlistSnackbar: () -> Unit,
) {
    productId = data?.productId
    productName = data?.productName
    viewModel.productName = data?.productName
    viewModel.productPrice = data?.productPrice?.toCurrencyFormat()
    viewModel.detailProduct = data
    var currentVariantPrice by remember { mutableStateOf(0) }
    Column(
        Modifier.verticalScroll(rememberScrollState())
    ) {
        ImageSection(data?.image!!)
        TitleSection(
            data.productName,
            data.productPrice,
            data.sale,
            data.productRating,
            data.totalRating,
            viewModel,
            firebaseAnalytics,
            shareAction,
            wishlistSnackbar,
            currentVariantPrice
        )
        Divider()
        VariantSection(
            data.productVariant,
            onVariantPriceSelected = { price -> currentVariantPrice = price ?: 0 },
        )
        Divider()
        DescriptionSection(data.description)
        Divider()
        ReviewSection(
            data.productRating,
            data.totalSatisfaction,
            data.totalRating,
            data.totalReview,
            toAllReview
        )
    }
}

@Composable
fun ReviewSection(
    rating: Float?,
    totalSatisfaction: Int?,
    totalRating: Int?,
    totalReview: Int?,
    toAllReview: () -> Unit
) {
    Column(
        Modifier.padding(horizontal = 16.dp, vertical = 12.dp)
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = stringResource(id = R.string.customers_review_txt),
                style = TextStyle(
                    fontFamily = poppinsFamily,
                    fontWeight = FontWeight.Medium,
                    fontSize = 16.sp,
                    platformStyle = PlatformTextStyle(
                        includeFontPadding = false
                    )
                ),
                modifier = Modifier.weight(1f)
            )
            TextButton(
                onClick = toAllReview,
                contentPadding = PaddingValues(0.dp),
                modifier = Modifier.height(22.dp)
            ) {
                Text(
                    text = stringResource(id = R.string.see_all_txt_btn),
                    style = TextStyle(
                        fontFamily = poppinsFamily,
                        fontWeight = FontWeight.Medium,
                        fontSize = 12.sp,
                        platformStyle = PlatformTextStyle(
                            includeFontPadding = false
                        )
                    ),
                )
            }
        }
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.padding(top = 8.dp)
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Center
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_star),
                    contentDescription = "Review icon",
                    modifier = Modifier.padding(end = 4.dp)
                )
                Text(
                    text = "$rating",
                    style = TextStyle(
                        fontFamily = poppinsFamily,
                        fontWeight = FontWeight.SemiBold,
                        fontSize = 20.sp,
                        platformStyle = PlatformTextStyle(
                            includeFontPadding = false
                        )
                    )
                )
                Text(
                    text = "/5.0",
                    style = TextStyle(
                        fontFamily = poppinsFamily,
                        fontWeight = FontWeight.Normal,
                        fontSize = 10.sp,
                    ),
                    modifier = Modifier.align(Alignment.Bottom)
                )
            }
            Column(
                Modifier.padding(start = 32.dp)
            ) {
                Text(
                    text = "$totalSatisfaction${stringResource(id = R.string.satistfied_customer_txt)}",
                    style = TextStyle(
                        fontFamily = poppinsFamily,
                        fontWeight = FontWeight.SemiBold,
                        fontSize = 12.sp,
                        platformStyle = PlatformTextStyle(
                            includeFontPadding = false
                        )
                    )
                )
                Text(
                    text = "$totalRating rating · $totalReview ${stringResource(id = R.string.detail_review_txt)}",
                    style = TextStyle(
                        fontFamily = poppinsFamily,
                        fontWeight = FontWeight.Normal,
                        fontSize = 12.sp,
                        platformStyle = PlatformTextStyle(
                            includeFontPadding = false
                        )
                    )
                )
            }
        }
    }
}

@Composable
fun DescriptionSection(description: String?) {
    Column(
        Modifier.padding(horizontal = 16.dp, vertical = 12.dp)
    ) {
        Text(
            text = stringResource(id = R.string.product_description_txt),
            style = TextStyle(
                fontFamily = poppinsFamily,
                fontWeight = FontWeight.Medium,
                fontSize = 16.sp,
                platformStyle = PlatformTextStyle(
                    includeFontPadding = false
                )
            ),
        )
        Text(
            text = description ?: "",
            style = TextStyle(
                fontFamily = poppinsFamily,
                lineHeight = 20.sp,
                platformStyle = PlatformTextStyle(
                    includeFontPadding = false
                )
            ),
            modifier = Modifier.padding(top = 8.dp)
        )
    }
}

@OptIn(ExperimentalMaterial3Api::class, ExperimentalLayoutApi::class)
@Composable
fun VariantSection(
    variants: List<ProductVariantItem?>?,
    onVariantPriceSelected: (Int?) -> Unit,
) {
    var selectedVariant by remember {
        mutableStateOf(variants?.first())
    }
    DetailFragmentCompose.selectedVariant = selectedVariant

    Column(
        Modifier.padding(horizontal = 16.dp, vertical = 12.dp)
    ) {
        Text(
            text = stringResource(id = R.string.choose_variant_txt),
            style = TextStyle(
                fontFamily = poppinsFamily,
                fontSize = 16.sp,
                fontWeight = FontWeight.Medium,
                platformStyle = PlatformTextStyle(
                    includeFontPadding = false
                )
            ),
            modifier = Modifier.padding(bottom = 8.dp)
        )
        FlowRow(
            horizontalArrangement = Arrangement.spacedBy(8.dp)
        ) {
            variants?.forEachIndexed { index, variant ->
                FilterChip(
                    selected = variant == selectedVariant,
                    onClick = {
                        selectedVariant = variants[index]
                        DetailFragmentCompose.selectedVariant = variant
                        onVariantPriceSelected(variant?.variantPrice)
                    },
                    label = {
                        Text(
                            text = variant?.variantName.toString(),
                            style = TextStyle(
                                fontFamily = poppinsFamily,
                                fontWeight = FontWeight.Medium,
                                platformStyle = PlatformTextStyle(
                                    includeFontPadding = false
                                )
                            )
                        )
                    }
                )
            }
        }
    }
}

@Composable
fun TitleSection(
    title: String?,
    price: Int?,
    sell: Int?,
    rating: Float?,
    totalRating: Int?,
    viewModel: DetailViewModel,
    firebaseAnalytics: FirebaseAnalytics,
    shareAction: () -> Unit,
    wishlistSnackbar: () -> Unit,
    selectedVariantPrice: Int,
) {
    Column(
        Modifier.padding(horizontal = 16.dp, vertical = 12.dp)
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            val totalPrice = price?.plus(selectedVariantPrice)
            Text(
                text = totalPrice?.toCurrencyFormat() ?: "",
                style = TextStyle(
                    fontFamily = poppinsFamily,
                    fontWeight = FontWeight.SemiBold,
                    fontSize = 20.sp,
                    platformStyle = PlatformTextStyle(
                        includeFontPadding = false
                    )
                ),
                modifier = Modifier.weight(1f)
            )
            IconButton(
                onClick = shareAction,
                modifier = Modifier.size(24.dp)
            ) {
                Icon(
                    painterResource(
                        id = R.drawable.ic_share
                    ),
                    contentDescription = "Share Button"
                )
            }
            Spacer(modifier = Modifier.width(16.dp))
            var icon by remember {
                mutableStateOf(
                    if (DetailFragmentCompose.isChecked) {
                        R.drawable.ic_favorite_product
                    } else {
                        R.drawable.ic_favorite_border
                    }
                )
            }
            IconButton(
                onClick = {
                    DetailFragmentCompose.isChecked = !DetailFragmentCompose.isChecked
                    if (DetailFragmentCompose.isChecked) {
                        viewModel.insertWishlistCompose(DetailFragmentCompose.selectedVariant)
                        wishlistSnackbar()
                        icon = R.drawable.ic_favorite_product
                    } else {
                        viewModel.deleteFromWishlistCompose(DetailFragmentCompose.selectedVariant)
                        wishlistSnackbar()
                        icon = R.drawable.ic_favorite_border
                        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_TO_WISHLIST) {
                            param(FirebaseAnalytics.Param.ITEM_ID, productId!!)
                            param(FirebaseAnalytics.Param.ITEM_NAME, productName!!)
                        }
                    }
                },
                modifier = Modifier.size(24.dp)
            ) {
                Icon(
                    painterResource(
                        id = icon
                    ),
                    contentDescription = "Wishlist Button"
                )
            }
        }
        Text(
            text = title ?: "",
            style = TextStyle(
                fontFamily = poppinsFamily,
                fontWeight = FontWeight.Normal,
                platformStyle = PlatformTextStyle(
                    includeFontPadding = false
                )
            ),
            modifier = Modifier.padding(top = 10.dp)
        )
        Row(
            Modifier.padding(top = 8.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = "${stringResource(id = R.string.detail_sold_txt)} $sell",
                style = TextStyle(
                    fontFamily = poppinsFamily,
                    fontWeight = FontWeight.Normal,
                    fontSize = 12.sp,
                    platformStyle = PlatformTextStyle(
                        includeFontPadding = false
                    )
                ),
                modifier = Modifier.padding(end = 8.dp)
            )
            Row(
                Modifier
                    .border(
                        border = BorderStroke(1.dp, MaterialTheme.colorScheme.outline),
                        shape = RoundedCornerShape(4.dp)
                    )
                    .padding(start = 4.dp, end = 8.dp, top = 2.dp, bottom = 2.dp),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Center
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_star_15),
                    contentDescription = "Review icon small",
                    modifier = Modifier.padding(end = 4.dp)
                )
                Text(
                    text = "$rating ($totalRating)",
                    style = TextStyle(
                        fontFamily = poppinsFamily,
                        fontWeight = FontWeight.Normal,
                        fontSize = 12.sp,
                        platformStyle = PlatformTextStyle(
                            includeFontPadding = false
                        )
                    )
                )
            }
        }
    }
}

@OptIn(ExperimentalFoundationApi::class, ExperimentalGlideComposeApi::class)
@Composable
fun ImageSection(images: List<String?>) {
    val pagerState = rememberPagerState(pageCount = {
        images.size
    })
    Box {
        HorizontalPager(state = pagerState) {
            GlideImage(
                model = images[it],
                contentDescription = "Product Images",
                contentScale = ContentScale.FillBounds,
                alignment = Alignment.Center,
                modifier = Modifier.height(309.dp)
            )
        }
        Row(
            modifier = Modifier
                .wrapContentHeight()
                .align(Alignment.BottomCenter)
                .padding(bottom = 16.dp),
            horizontalArrangement = Arrangement.Center
        ) {
            repeat(pagerState.pageCount) {
                val color =
                    if (pagerState.currentPage == it) {
                        MaterialTheme.colorScheme.primary
                    } else {
                        MaterialTheme.colorScheme.outlineVariant
                    }
                if (pagerState.pageCount > 1) {
                    Box(
                        modifier = Modifier
                            .padding(horizontal = 8.dp)
                            .clip(CircleShape)
                            .background(color)
                            .size(8.dp)
                    )
                }
            }
        }
    }
}
