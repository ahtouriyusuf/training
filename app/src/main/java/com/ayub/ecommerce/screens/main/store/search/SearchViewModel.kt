package com.ayub.ecommerce.screens.main.store.search

import androidx.lifecycle.ViewModel
import com.ayub.core.repository.IRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(private val repo: IRepository) : ViewModel() {
    fun searchProducts(query: String) = repo.searchProducts(query)
}
