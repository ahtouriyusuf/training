package com.ayub.ecommerce.screens.register

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import com.ayub.core.datasource.network.request_response.BaseResponse
import com.ayub.core.datasource.network.request_response.RegisterData
import com.ayub.core.repository.IRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(private val repo: IRepository) : ViewModel() {
    private val _registerResult: MutableLiveData<Map<String, String>> = MutableLiveData()

    val registerResult: LiveData<BaseResponse<RegisterData>>
        get() = _registerResult.switchMap { map ->
            repo.register(map["email"]!!, map["password"]!!, map["firebase_token"]!!)
        }

    fun register(email: String, pwd: String, firebaseToken: String) {
        _registerResult.value =
            mapOf("email" to email, "password" to pwd, "firebase_token" to firebaseToken)
    }
}
