package com.ayub.ecommerce.screens.onboarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.ayub.ecommerce.R
import com.ayub.ecommerce.databinding.FragmentOnboardingBinding
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OnboardingFragment : Fragment() {
    private var _binding: FragmentOnboardingBinding? = null
    private val binding get() = _binding!!
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private val viewModel: OnboardingViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentOnboardingBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        firebaseAnalytics = Firebase.analytics
        val itemList = listOf(
            R.drawable.onboarding_1,
            R.drawable.onboarding_2,
            R.drawable.onboarding_3
        )

        val adapter = ViewPagerAdapter(
            itemList
        )

        val viewPager = binding.viewPager2
        viewPager.adapter = adapter

        val indicator = binding.springDotsIndicator
        indicator.attachTo(viewPager)

        var currentPage = 0

        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                when (position) {
                    0 -> {
                        binding.nextBtn.visibility = View.VISIBLE
                        currentPage = 0
                    }

                    1 -> {
                        binding.nextBtn.visibility = View.VISIBLE
                        currentPage = 1
                    }

                    2 -> {
                        binding.nextBtn.visibility = View.INVISIBLE
                        currentPage = 2
                    }
                }
                super.onPageSelected(position)
            }
        })

        binding.nextBtn.setOnClickListener {
            firebaseAnalytics.logEvent("btn_next_page", null)
            if (currentPage < 2) {
                currentPage++
                when (currentPage) {
                    0 -> {
                        viewPager.currentItem = 0
                        binding.nextBtn.visibility = View.VISIBLE
                    }

                    1 -> {
                        viewPager.currentItem = 1
                        binding.nextBtn.visibility = View.VISIBLE
                    }

                    2 -> {
                        viewPager.currentItem = 2
                        binding.nextBtn.visibility = View.INVISIBLE
                    }
                }
            }
        }

        binding.joinBtn.setOnClickListener {
            findNavController().navigate(R.id.action_onboardingFragment_to_registerFragment)
            viewModel.setIsFirstLaunchToFalse()
            firebaseAnalytics.logEvent("btn_onboarding_toRegister", null)
        }

        binding.skipBtn.setOnClickListener {
            findNavController().navigate(R.id.action_onboardingFragment_to_loginFragment)
            viewModel.setIsFirstLaunchToFalse()
            firebaseAnalytics.logEvent("btn_onboarding_toLogin", null)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
