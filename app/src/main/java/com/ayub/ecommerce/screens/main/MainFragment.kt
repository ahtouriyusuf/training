package com.ayub.ecommerce.screens.main

import android.annotation.SuppressLint
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import androidx.window.layout.WindowMetricsCalculator
import com.ayub.ecommerce.R
import com.ayub.ecommerce.databinding.FragmentMainBinding
import com.ayub.ecommerce.screens.main.cart.CartViewModel
import com.ayub.ecommerce.screens.main.notification.NotificationViewModel
import com.ayub.ecommerce.screens.main.wishlist.WishlistViewModel
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.badge.BadgeUtils.attachBadgeDrawable
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.runBlocking

@AndroidEntryPoint
class MainFragment : Fragment() {
    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding
    private val viewModel: MainFViewModel by viewModels()
    private val wishListViewModel: WishlistViewModel by viewModels()
    private val cartViewModel: CartViewModel by viewModels()
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    // private var isNewData: Boolean? = false
    private val notificationViewModel: NotificationViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (viewModel.getUsername().isNullOrEmpty()) {
            findNavController().navigate(R.id.action_mainFragment_to_profileFragment)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        firebaseAnalytics = Firebase.analytics

        setNavigation()
        setUsername()
        setWishlistBadge()
        setCartBadge()
        setNotificationBadge()
        appBarActions()
    }

    private fun setNavigation() {
        val navHostFragment =
            childFragmentManager.findFragmentById(R.id.fcv_main_fragment) as NavHostFragment
        val navController = navHostFragment.navController

        binding?.bottomNav?.setupWithNavController(navController)
        binding?.navRail?.setupWithNavController(navController)
        binding?.navView?.setupWithNavController(navController)

        val container: ViewGroup = binding?.container!!
        container.addView(object : View(requireActivity()) {
            override fun onConfigurationChanged(newConfig: Configuration?) {
                super.onConfigurationChanged(newConfig)
                WindowMetricsCalculator.getOrCreate().computeCurrentWindowMetrics(requireActivity())
            }
        })
        WindowMetricsCalculator.getOrCreate().computeCurrentWindowMetrics(requireActivity())

        if (viewModel.isNewData) {
            binding?.bottomNav?.selectedItemId = R.id.transactionFragment
            binding?.navRail?.selectedItemId = R.id.transactionFragment
            binding?.navView?.menu?.performIdentifierAction(R.id.transactionFragment, 0)
            viewModel.isNewData = false
        }
    }

    private fun setUsername() {
        binding?.tvUserName?.text = runBlocking { viewModel.getUsername() }
    }

    private fun setWishlistBadge() {
        wishListViewModel.getWishlistData.observe(viewLifecycleOwner) {
            val numberOfItemsInCart = it.size
            val bottomBadge = binding?.bottomNav?.getOrCreateBadge(R.id.wishlistFragment)
            val railBadge = binding?.navRail?.getOrCreateBadge(R.id.wishlistFragment)
            if (numberOfItemsInCart > 0) {
                bottomBadge?.number = numberOfItemsInCart
                railBadge?.number = numberOfItemsInCart
                bottomBadge?.isVisible = true
                railBadge?.isVisible = true
            } else {
                bottomBadge?.isVisible = false
                railBadge?.isVisible = false
                bottomBadge?.clearNumber()
                railBadge?.clearNumber()
            }
        }
    }

    @SuppressLint("UnsafeOptInUsageError")
    private fun setCartBadge() {
        val badgeDrawable = BadgeDrawable.create(requireContext())
        cartViewModel.cartData.observe(viewLifecycleOwner) {
            val numberOfItemsInCart = it.size
            badgeDrawable.number = numberOfItemsInCart
            badgeDrawable.isVisible = numberOfItemsInCart > 0
            attachBadgeDrawable(badgeDrawable, binding!!.topAppBar, R.id.cart)
        }
    }

    @SuppressLint("UnsafeOptInUsageError")
    private fun setNotificationBadge() {
        val badgeDrawable = BadgeDrawable.create(requireContext())
        notificationViewModel.getAllNotification()?.observe(viewLifecycleOwner) {
            val numberofNotifications = it.filter { !it.isRead }.size
            badgeDrawable.number = numberofNotifications
            badgeDrawable.isVisible = numberofNotifications > 0
            attachBadgeDrawable(badgeDrawable, binding!!.topAppBar, R.id.notification)
        }
    }

    private fun appBarActions() {
        binding?.topAppBar?.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.cart -> {
                    findNavController().navigate(R.id.action_mainFragment_to_cartFragment)
                    firebaseAnalytics.logEvent("btn_toCart", null)
                }

                R.id.notification -> {
                    findNavController().navigate(R.id.action_mainFragment_to_notificationFragment)
                    firebaseAnalytics.logEvent("btn_toNotification", null)
                }
            }
            true
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        const val IS_NEW_DATA_KEY = "isNewData"
    }
}
