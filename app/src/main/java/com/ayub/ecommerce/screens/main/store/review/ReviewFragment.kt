package com.ayub.ecommerce.screens.main.store.review

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.ayub.core.datasource.network.request_response.BaseResponse
import com.ayub.core.datasource.network.request_response.ReviewData
import com.ayub.ecommerce.databinding.FragmentReviewBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ReviewFragment : Fragment() {
    private var _binding: FragmentReviewBinding? = null
    private val binding get() = _binding!!
    private lateinit var adapter: ReviewAdapter
    private val viewModel: ReviewViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentReviewBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.topAppBar.setOnClickListener {
            findNavController().navigateUp()
        }

        viewModel.getProductReview.observe(viewLifecycleOwner) {
            viewModel.idProduct = arguments?.getString(BUNDLE_PRODUCT_ID_KEY)
            when (it) {
                is BaseResponse.Loading -> {
                    binding.loadingIndicator.visibility = View.VISIBLE
                }

                is BaseResponse.Success -> {
                    binding.loadingIndicator.visibility = View.INVISIBLE
                    setRecyclerView(it.data)
                }

                is BaseResponse.Error -> {
                    binding.loadingIndicator.visibility = View.INVISIBLE
                }

                else -> binding.loadingIndicator.visibility = View.INVISIBLE
            }
        }
    }

    private fun setRecyclerView(it: List<ReviewData?>?) {
        adapter = ReviewAdapter(it)
        binding.rvReview.adapter = adapter
        binding.rvReview.layoutManager = LinearLayoutManager(requireContext())
    }

    companion object {
        const val BUNDLE_PRODUCT_ID_KEY = "id"
    }
}
