package com.ayub.ecommerce.screens.main.store

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.ayub.core.datasource.network.request_response.ProductRequest
import com.ayub.core.repository.IRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class StoreViewModel @Inject constructor(private val repo: IRepository) : ViewModel() {
    var recyclerViewType = StoreAdapter.ONE_COLUMN_VIEW_TYPE

    private val productRequest = MutableLiveData(ProductRequest())

    private val productData: LiveData<ProductRequest> = productRequest

    val product = productData.switchMap {
        repo.getProducts(it).cachedIn(viewModelScope)
    }

    val filteredProduct: LiveData<ProductRequest> = productRequest

    fun updateFilter(sort: String?, brand: String?, lowest: String?, highest: String?) {
        productRequest.value = productRequest.value?.copy(
            sort = sort,
            brand = brand,
            lowest = lowest,
            highest = highest
        )
    }

    fun updateSearch(search: String?) {
        productRequest.value = productRequest.value?.copy(search)
    }

    fun resetParam() {
        productRequest.value = productRequest.value?.copy(
            search = null,
            brand = null,
            lowest = null,
            highest = null,
            sort = null
        )
    }

    fun logout() = repo.logout()
}
