package com.ayub.ecommerce.screens.main.transaction

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.ayub.core.datasource.network.request_response.TransactionData
import com.ayub.ecommerce.R
import com.ayub.ecommerce.databinding.ItemTransactionBinding
import com.ayub.ecommerce.screens.main.store.StoreAdapter
import com.ayub.ecommerce.utils.toCurrencyFormat
import com.bumptech.glide.Glide

class TransactionAdapter(private val dataList: List<TransactionData?>?) :
    RecyclerView.Adapter<TransactionAdapter.ViewHolder>() {
    private var onItemClickCallback: OnItemClickCallback? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemTransactionBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, onItemClickCallback!!)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = dataList!![position]
        holder.bind(item!!)
        holder.itemView.startAnimation(
            AnimationUtils.loadAnimation(
                holder.itemView.context,
                R.anim.list_item_animation
            )
        )
    }

    override fun getItemCount(): Int {
        return dataList!!.size
    }

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    interface OnItemClickCallback {
        fun toReview(data: TransactionData)
    }

    inner class ViewHolder(
        private val binding: ItemTransactionBinding,
        private val onItemClickCallback: OnItemClickCallback
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: TransactionData) {
            binding.apply {
                tvTransactionDate.text = item.date
                Glide.with(itemView.context)
                    .load(item.image)
                    .fitCenter()
                    .into(ivProduct)
                tvProductName.text = item.name
                tvItemQuantity.text = item.items?.size.toString()
                tvTransactionTotalPayment.text = item.total?.toCurrencyFormat()

                if (item.rating == null || item.review.isNullOrEmpty()) {
                    reviewBtn.visibility = View.VISIBLE
                } else {
                    reviewBtn.visibility = View.GONE
                }

                reviewBtn.setOnClickListener {
                    onItemClickCallback.toReview(data = item)
                }
            }
        }
    }
}
