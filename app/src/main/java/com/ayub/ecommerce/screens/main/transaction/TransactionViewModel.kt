package com.ayub.ecommerce.screens.main.transaction

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ayub.core.datasource.network.request_response.BaseResponse
import com.ayub.core.datasource.network.request_response.TransactionData
import com.ayub.core.repository.IRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TransactionViewModel @Inject constructor(private val repo: IRepository) : ViewModel() {
    private val _transactionData = MutableLiveData<BaseResponse<List<TransactionData?>>>(null)
    val transactionData: LiveData<BaseResponse<List<TransactionData?>>> = _transactionData

    val listTransactionData = repo.transaction()

    init {
        getTransactionData()
    }

    fun getTransactionData() {
        viewModelScope.launch {
            repo.transaction().observeForever {
                _transactionData.value = it
            }
        }
    }

    fun logout() = repo.logout()
}
