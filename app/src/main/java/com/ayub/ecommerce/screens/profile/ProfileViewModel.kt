package com.ayub.ecommerce.screens.profile

import androidx.lifecycle.ViewModel
import com.ayub.core.repository.IRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import okhttp3.MultipartBody
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(private val repo: IRepository) : ViewModel() {
    fun saveProfile(name: MultipartBody.Part, image: MultipartBody.Part?) =
        repo.saveProfile(name, image)
}
