package com.ayub.ecommerce.screens.main.store.detail

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.ayub.core.datasource.network.request_response.BaseResponse
import com.ayub.core.datasource.network.request_response.DetailProductData
import com.ayub.core.datasource.network.request_response.ProductVariantItem
import com.ayub.core.helper.toCart
import com.ayub.ecommerce.R
import com.ayub.ecommerce.databinding.FragmentDetailBinding
import com.ayub.ecommerce.screens.main.checkout.CheckoutFragment
import com.ayub.ecommerce.screens.main.store.review.ReviewFragment
import com.ayub.ecommerce.utils.toCurrencyFormat
import com.google.android.material.chip.Chip
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.runBlocking
import retrofit2.HttpException
import java.io.IOException

@AndroidEntryPoint
class DetailFragment : Fragment() {
    private val firebaseAnalytics: FirebaseAnalytics = Firebase.analytics
    private var _binding: FragmentDetailBinding? = null
    private val binding get() = _binding!!
    private val viewModel: DetailViewModel by viewModels()
    var productName: String? = null
    var productPrice: String? = null
    private var productId: String? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    findNavController().navigateUp()
                }
            }
        )
        // Inflate the layout for this fragment
        _binding = FragmentDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeData()
        itemAction()
        setupWishlist()
        setupCart()
    }

    private fun itemAction() {
        binding.topAppBar.setOnClickListener {
            findNavController().navigateUp()
        }

        binding.wishlistBtn.setOnClickListener {
            actionWishlist()
        }

        binding.allReviewBtn.setOnClickListener {
//            findNavController().navigate(
//                R.id.action_detailFragment_to_reviewFragment,
//                bundleOf(
//                    ReviewFragment.BUNDLE_PRODUCT_ID_KEY to viewModel.idProduct
//                )
//            )
            findNavController().navigate(
                R.id.action_detailFragment_to_reviewFragmentCompose,
                bundleOf(
                    ReviewFragment.BUNDLE_PRODUCT_ID_KEY to viewModel.idProduct
                )
            )
        }

        binding.shareBtn.setOnClickListener {
            val sendIntent: Intent = Intent().apply {
                val text = getString(
                    R.string.share_product_txt,
                    productName,
                    productPrice,
                    viewModel.idProduct
                ).trimIndent()
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, text)
                type = "text/plain"
            }

            val shareIntent = Intent.createChooser(sendIntent, null)
            startActivity(shareIntent)
        }

        binding.addToCartBtn.setOnClickListener {
            runBlocking {
                actionCart()
            }
        }

        binding.addToCheckout.setOnClickListener {
            actionCheckout()
        }
    }

    private fun observeData() {
        viewModel.detailProductData.observe(requireActivity()) {
            when (it) {
                is BaseResponse.Loading -> {
                    binding.loadingIndicator.visibility = View.VISIBLE
                    binding.linearErrorLayout.visibility = View.GONE
                }

                is BaseResponse.Success -> {
                    binding.loadingIndicator.visibility = View.INVISIBLE
                    binding.linearErrorLayout.visibility = View.GONE
                    setDetailProductData(it.data)
                    viewModel.detailProduct = it.data
                    productId = it.data?.productId.toString()
                    productName = it.data?.productName
                    productPrice = it.data?.productPrice?.toCurrencyFormat().toString()
                    firebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM) {
                        param(FirebaseAnalytics.Param.ITEM_ID, it.data?.productId.toString())
                        param(FirebaseAnalytics.Param.ITEM_NAME, it.data?.productName.toString())
                    }
                }

                is BaseResponse.Error -> {
                    binding.loadingIndicator.visibility = View.INVISIBLE
                    binding.linearErrorLayout.visibility = View.VISIBLE
                    processError(it.error!!)
                }

                else -> binding.loadingIndicator.visibility = View.INVISIBLE
            }
        }
    }

    private fun setDetailProductData(data: DetailProductData?) {
        val itemList = data?.image
        val adapter = ImageAdapter(itemList)

        val viewPager = binding.viewPager
        viewPager.adapter = adapter

        val indicator = binding.vpIndicator
        indicator.attachTo(viewPager)

        binding.apply {
            scrollView.visibility = View.VISIBLE
            linearLayout.visibility = View.VISIBLE
            priceTV.text = data?.productPrice?.toCurrencyFormat().toString()
            titleTV.text = data?.productName.toString()
            sellTV.text = " ${data?.sale}"
            ratingTV.text = data?.productRating.toString()
            reviewersTV.text = data?.totalReview.toString()
            setChipVariants(data)
            descriptionTV.text = data?.description.toString()
            productRatingTV.text = data?.productRating.toString()
            satisfactionTV.text = data?.totalSatisfaction.toString()
            totalRatingTV.text = data?.totalRating.toString()
            totalReviewTV.text = "${data?.totalReview} "
        }
    }

    private fun processError(error: Throwable) {
        when (error) {
            is HttpException -> {
                if (error.response()?.code() == 404) {
                    binding.errorTypeText.text = getString(R.string.empty_list_state)
                    binding.errorTypeInfo.text = getString(R.string.empty_list_description)
                    binding.errorButton.text = getString(R.string.reset_txt_btn)
                } else {
                    binding.errorTypeText.text = error.response()?.code().toString()
                    binding.errorTypeInfo.text = error.response()?.message()
                    binding.errorButton.text = getString(R.string.refresh_txt_btn)
                }
            }

            is IOException -> {
                binding.errorTypeText.text = getString(R.string.connection_error_state)
                binding.errorTypeInfo.text = getString(R.string.connection_state_description)
                binding.errorButton.text = getString(R.string.refresh_txt_btn)
            }

            else -> {
                binding.errorTypeText.text = "Error"
                binding.errorTypeInfo.text = error.message
                binding.errorButton.text = getString(R.string.refresh_txt_btn)
            }
        }
    }

    private fun actionWishlist() {
        if (viewModel.isWishlist) {
            viewModel.deleteFromWishlist()
            Snackbar.make(
                requireView(),
                getString(R.string.delete_from_wishlist_success),
                Snackbar.LENGTH_SHORT
            ).show()
        } else {
            viewModel.insertWishlist()
            Snackbar.make(
                requireView(),
                getString(R.string.add_to_wishlist_success),
                Snackbar.LENGTH_SHORT
            ).show()
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_TO_WISHLIST) {
                param(FirebaseAnalytics.Param.ITEM_ID, productId!!)
                param(FirebaseAnalytics.Param.ITEM_NAME, productName!!)
            }
        }
        setWishlistIcon()
    }

    private suspend fun actionCart() {
        if (viewModel.isInCart) {
            var quantity = viewModel.getProductFromCart(productId!!)?.quantity ?: 0
            val stock = viewModel.getProductFromCart(productId!!)?.stock
            if (quantity < stock!!) {
                quantity++
                viewModel.updateProductQuantity(productId!!, quantity)
                Snackbar.make(
                    requireView(),
                    getString(R.string.add_quantity_success_txt),
                    Snackbar.LENGTH_SHORT
                ).show()
            } else {
                Snackbar.make(
                    requireView(),
                    getString(R.string.item_quantity_maxed_txt),
                    Snackbar.LENGTH_SHORT
                ).show()
            }
        } else {
            viewModel.addToCart(viewModel.productVariant!!)
            Snackbar.make(
                requireView(),
                getString(R.string.add_cart_success),
                Snackbar.LENGTH_SHORT
            ).show()
        }
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_TO_CART) {
            val bundle = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_ID, productId)
                putString(FirebaseAnalytics.Param.ITEM_NAME, productName)
                param(FirebaseAnalytics.Param.CURRENCY, productPrice!!)
                putString(
                    FirebaseAnalytics.Param.ITEM_VARIANT,
                    viewModel.productVariant?.variantName
                )
            }
            param(FirebaseAnalytics.Param.ITEMS, bundle)
        }
    }

    private fun actionCheckout() {
        if (viewModel.detailProduct != null && viewModel.productVariant != null) {
            val data = viewModel.detailProduct!!.toCart(viewModel.productVariant!!)
            val bundle = Bundle().apply {
                putParcelableArrayList(CheckoutFragment.ARG_DATA, arrayListOf(data))
            }
            findNavController().navigate(R.id.action_detailFragment_to_checkoutFragment, bundle)
        }
    }

    private fun setupWishlist() {
        viewModel.isWishlist = runBlocking { viewModel.checkProductInWishlist() }
        setWishlistIcon()
    }

    private fun setupCart() {
        viewModel.isInCart = runBlocking { viewModel.checkProductInCart() }
    }

    private fun setWishlistIcon() {
        binding.wishlistBtn.setImageResource(
            if (viewModel.isWishlist) {
                R.drawable.ic_favorite_product
            } else {
                R.drawable.ic_favorite_border
            }
        )
    }

    private fun setChipVariants(data: DetailProductData?) {
        setChip(data?.productVariant, data?.productPrice!!)
    }

    private fun setChip(variants: List<ProductVariantItem?>?, price: Int) {
        viewModel.productVariant = variants?.first()
        binding.apply {
            chipsVariant.removeAllViewsInLayout()
            variants?.forEach { variant ->
                val chip = Chip(requireContext())
                chip.apply {
                    text = variant?.variantName
                    isChipIconVisible = false
                    isCloseIconVisible = false
                    isCheckable = true
                    isChecked = variant?.variantName == variants[0]?.variantName
                    setOnClickListener {
                        binding.priceTV.text =
                            price.plus(variant?.variantPrice!!).toCurrencyFormat()
                        viewModel.productVariant = variant
                    }
                    chipsVariant.addView(chip as View)
                }
            }
        }
    }

    companion object {
        const val BUNDLE_PRODUCT_ID_KEY = "id"
    }
}
