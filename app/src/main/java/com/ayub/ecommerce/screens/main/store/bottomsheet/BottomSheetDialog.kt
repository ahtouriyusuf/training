package com.ayub.ecommerce.screens.main.store.bottomsheet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.children
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.setFragmentResult
import com.ayub.ecommerce.databinding.BottomSheetDialogBinding
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.chip.Chip
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.ktx.Firebase

class BottomSheetDialog : BottomSheetDialogFragment() {

    private val firebaseAnalytics: FirebaseAnalytics = Firebase.analytics
    private var _binding: BottomSheetDialogBinding? = null
    private val binding get() = _binding!!
    private var sort: String? = null
    private var brand: String? = null
    private var lowest: String? = null
    private var highest: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sort = arguments?.getString(CHIP_SORT)
        brand = arguments?.getString(CHIP_BRAND)
        lowest = arguments?.getString(LOWEST)
        highest = arguments?.getString(HIGHEST)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = BottomSheetDialogBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val modalBottomsheetBehavior =
            (dialog as com.google.android.material.bottomsheet.BottomSheetDialog).behavior
        modalBottomsheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED

        setSelectedFilter()
        binding.apply {
            filterProductBtn.setOnClickListener {
                sendFilterData()
                dismiss()
            }

            chipsSort.setOnCheckedStateChangeListener { _, _ ->
                resetBtn.visibility = View.VISIBLE
            }

            chipsCategory.setOnCheckedStateChangeListener { _, _ ->
                resetBtn.visibility = View.VISIBLE
            }

            inputLowest.doOnTextChanged { text, _, _, _ ->
                resetBtn.visibility = View.VISIBLE
            }

            inputHighest.doOnTextChanged { text, _, _, _ ->
                resetBtn.visibility = View.VISIBLE
            }

            resetBtn.setOnClickListener {
                resetFilterData()
            }
        }
    }

    private fun resetFilterData() {
        binding.apply {
            chipsSort.clearCheck()
            chipsCategory.clearCheck()
            inputLowest.text?.clear()
            inputHighest.text?.clear()
            resetBtn.visibility = View.GONE
        }
    }

    private fun sendFilterData() {
        binding.apply {
            val sort: Chip? = chipsSort.findViewById(chipsSort.checkedChipId)
            val category: Chip? = chipsCategory.findViewById(chipsCategory.checkedChipId)
            val lowest = inputLowest.text.toString()
            val highest = inputHighest.text.toString()
            val bundle = Bundle().apply {
                putString(CHIP_SORT, sort?.text?.toString())
                putString(CHIP_BRAND, category?.text?.toString())
                putString(LOWEST, lowest)
                putString(HIGHEST, highest)
            }
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
                param(
                    FirebaseAnalytics.Param.ITEMS,
                    Bundle().apply {
                        putString(FirebaseAnalytics.Param.ITEM_CATEGORY, sort?.text.toString())
                        putString(FirebaseAnalytics.Param.ITEM_CATEGORY2, category?.text.toString())
                        putString(FirebaseAnalytics.Param.ITEM_CATEGORY3, lowest)
                        putString(FirebaseAnalytics.Param.ITEM_CATEGORY4, highest)
                    }
                )
            }
            setFragmentResult(FILTER, bundle)
        }
    }

    private fun setSelectedFilter() {
        binding.apply {
//            sort = arguments?.getString(CHIP_SORT)
//            brand = arguments?.getString(CHIP_BRAND)
//            lowest = arguments?.getString(LOWEST)
//            highest = arguments?.getString(HIGHEST)

            chipsSort.children.forEach { chipProduct ->
                if ((chipProduct as Chip).text == sort) {
                    chipProduct.isChecked = true
                    resetBtn.visibility = View.VISIBLE
                }
            }

//            setSortChipView()

            chipsCategory.children.forEach { chipProduct ->
                if ((chipProduct as Chip).text == brand) {
                    chipProduct.isChecked = true
                    resetBtn.visibility = View.VISIBLE
                }
            }

            val highestValue = highest ?: ""
            val lowestValue = lowest ?: ""

            if (highestValue.isEmpty() || highestValue == "null") {
                inputHighest.setText("")
            } else {
                inputHighest.setText(highestValue.replace(Regex("[Rp<> ,.]"), ""))
                resetBtn.visibility = View.VISIBLE
            }

            if (lowestValue.isEmpty() || lowestValue == "null") {
                inputLowest.setText("")
            } else {
                inputLowest.setText(lowestValue.replace(Regex("[Rp<> ,.]"), ""))
                resetBtn.visibility = View.VISIBLE
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        const val TAG = "BottomSheet"
        const val CHIP_SORT = "chip_sort"
        const val CHIP_SORT_ID = "chip_sort_id"
        const val CHIP_BRAND = "chip_brand"
        const val LOWEST = "lowest"
        const val HIGHEST = "higest"
        const val FILTER = "filter"

        @JvmStatic
        fun instance(
            sort: String?,
            brand: String?,
            lowest: String?,
            highest: String?,
        ): BottomSheetDialog {
            val myFragment = BottomSheetDialog()
            val args = Bundle().apply {
                putString(CHIP_SORT, sort)
                putString(CHIP_BRAND, brand)
                putString(LOWEST, lowest)
                putString(HIGHEST, highest)
            }
            myFragment.arguments = args

            return myFragment
        }
    }
}
