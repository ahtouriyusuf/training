package com.ayub.ecommerce.screens.main.cart

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ayub.core.datasource.local.room.entity.Cart
import com.ayub.core.repository.IRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CartViewModel @Inject constructor(private val repo: IRepository) : ViewModel() {
    var totalPrice = 0

    val cartData: LiveData<List<Cart>> = repo.getCartProducts()

    fun deleteProduct(cartProduct: Cart) {
        viewModelScope.launch {
            repo.deleteProductInCart(cartProduct)
        }
    }

    fun deleteAllProductInCart(id: List<String>) {
        viewModelScope.launch {
            repo.deleteAllProductInCart(id)
        }
    }

    fun updateProductQuantity(id: String, newQuantity: Int) {
        viewModelScope.launch {
            repo.updateCartItemQuantity(id, newQuantity)
        }
    }

    fun updateProductIsSelected(id: List<String>, isSelected: Boolean) {
        viewModelScope.launch {
            repo.updateCartItemSelected(id, isSelected)
        }
    }
}
