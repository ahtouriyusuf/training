package com.ayub.ecommerce.screens.main.cart

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ayub.core.datasource.local.room.entity.Cart
import com.ayub.ecommerce.R
import com.ayub.ecommerce.databinding.ItemCartBinding
import com.ayub.ecommerce.utils.CartComparator
import com.ayub.ecommerce.utils.toCurrencyFormat
import com.bumptech.glide.Glide

class CartAdapter : ListAdapter<Cart, CartAdapter.ViewHolder>(CartComparator) {
    private var onItemClickCallback: OnItemClickCallback? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val binding = ItemCartBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, onItemClickCallback)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    interface OnItemClickCallback {
        fun onItemClicked(data: Cart)
        fun deleteCallback(data: Cart)
        fun increaseQuantity(data: Cart)
        fun decreaseQuantity(data: Cart)
        fun selectProduct(data: Cart)
    }

    class ViewHolder(
        private val binding: ItemCartBinding,
        private val onItemClickCallback: OnItemClickCallback?
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Cart) {
            itemView.setOnClickListener {
                onItemClickCallback?.onItemClicked(data)
            }
            binding.apply {
                Glide.with(binding.root.context)
                    .load(data.image)
                    .fitCenter()
                    .into(ivProduct)
                tvProductName.text = data.productName
                tvVariant.text = data.variantName
                tvStock.text = data.stock.toString()
                getStockText(data.stock!!)
                tvPrice.text = data.productPrice.plus(data.variantPrice!!).toCurrencyFormat()
                quantity.tvCounter.text = data.quantity.toString()
                isSelected.isChecked = data.selected
                quantity.btnIncrease.setOnClickListener { onItemClickCallback?.increaseQuantity(data) }
                quantity.btnDecrease.setOnClickListener { onItemClickCallback?.decreaseQuantity(data) }
                btnDeleteItem.setOnClickListener { onItemClickCallback?.deleteCallback(data) }
                isSelected.setOnClickListener { onItemClickCallback?.selectProduct(data) }
            }
        }

        private fun getStockText(stock: Int) {
            if (stock >= 10) {
                binding.tvStock.setTextColor(Color.BLACK)
                binding.stockTitle.setTextColor(Color.BLACK)
                binding.stockTitle.setText(R.string.stock_text)
            } else {
                binding.tvStock.setTextColor(Color.RED)
                binding.stockTitle.setTextColor(Color.RED)
                binding.stockTitle.setText(R.string.remaining_text)
            }
        }
    }
}
