package com.ayub.ecommerce.screens.login

import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.util.Patterns
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.ColorInt
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.ayub.core.datasource.network.request_response.BaseResponse
import com.ayub.core.datasource.network.request_response.LoginData
import com.ayub.ecommerce.R
import com.ayub.ecommerce.databinding.FragmentLoginBinding
import com.ayub.ecommerce.utils.getApiErrorMessage
import com.ayub.ecommerce.utils.hideKeyboard
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.runBlocking

@AndroidEntryPoint
class LoginFragment : Fragment() {
    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!
    private val viewModel: LoginViewModel by viewModels()
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private var tokenFcm: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isFirstLaunch()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        changeSpannedTextColor()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        firebaseAnalytics = Firebase.analytics

        binding.registerBtn.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
            firebaseAnalytics.logEvent("btn_login_toRegister", null)
        }

        binding.loginBtn.setOnClickListener {
            activity?.hideKeyboard(it)
            doLogin()
            firebaseAnalytics.logEvent("btn_login_action", null)
        }

        viewModel.loginResult.observe(requireActivity()) {
            when (it) {
                is BaseResponse.Loading -> {
                    binding.progressIndicator.visibility = View.VISIBLE
                    binding.loginBtn.isEnabled = false
                }

                is BaseResponse.Success -> {
                    binding.progressIndicator.visibility = View.INVISIBLE
                    binding.loginBtn.isEnabled = true
                    processLogin(it.data)
                }

                is BaseResponse.Error -> {
                    binding.progressIndicator.visibility = View.INVISIBLE
                    binding.loginBtn.isEnabled = true
                    processError(getApiErrorMessage(it.error!!) ?: "Unknown Error")
                }

                else -> {
                    binding.progressIndicator.visibility = View.INVISIBLE
                    binding.loginBtn.isEnabled = true
                }
            }
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN) {
                param(FirebaseAnalytics.Param.METHOD, "email")
            }
        }

        binding.emailValueTI.doOnTextChanged { text, _, _, _ ->
            val email = text.toString().trim()
            if (!isValidEmail(email) && email.isNotEmpty()) {
                binding.emailTI.error = getString(R.string.email_error_ti)
            } else {
                binding.emailTI.error = null
            }

            enableLoginButton()
        }

        binding.passwordValueTI.doOnTextChanged { text, _, _, _ ->
            val password = text.toString().trim()
            if (password.isNotEmpty() && password.length < 8) {
                binding.passwordTI.error = getString(R.string.password_error_ti)
            } else {
                binding.passwordTI.error = null
            }

            enableLoginButton()
        }
    }

    private fun isValidEmail(email: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun enableLoginButton() {
        val email = binding.emailValueTI.text.toString()
        val password = binding.passwordValueTI.text.toString()
        binding.loginBtn.isEnabled =
            email.isNotEmpty() && isValidEmail(email) && password.length >= 8
    }

    private fun changeSpannedTextColor() {
        val typedValue = TypedValue()
        val theme = requireContext().theme
        theme.resolveAttribute(androidx.appcompat.R.attr.colorPrimary, typedValue, true)
        @ColorInt val color = typedValue.data

        val spannable = SpannableString(binding.termsTv.text)

        val isLanguageId = viewModel.getLang()

        if (isLanguageId) {
            spannable.setSpan(
                ForegroundColorSpan(color),
                38,
                55,
                Spannable.SPAN_EXCLUSIVE_INCLUSIVE
            )

            spannable.setSpan(
                ForegroundColorSpan(color),
                63,
                79,
                Spannable.SPAN_EXCLUSIVE_INCLUSIVE
            )
        } else {
            spannable.setSpan(
                ForegroundColorSpan(color),
                33,
                51,
                Spannable.SPAN_EXCLUSIVE_INCLUSIVE
            )

            spannable.setSpan(
                ForegroundColorSpan(color),
                68,
                83,
                Spannable.SPAN_EXCLUSIVE_INCLUSIVE
            )
        }
        binding.termsTv.text = spannable
    }

    private fun isFirstLaunch() {
        val isFirstTime = runBlocking { viewModel.isFirstLaunch() }
        if (isFirstTime) {
            findNavController().navigate(R.id.action_loginFragment_to_onboardingFragment)
        }
    }

    private fun processLogin(data: LoginData?) {
        if (!data?.accessToken.isNullOrEmpty()) {
            data?.let {
                getTokenNotification()
                findNavController().navigate(R.id.action_global_main_navigation)
            }
        }
    }

    private fun processError(msg: CharSequence) {
        Snackbar.make(requireView(), msg, Snackbar.LENGTH_SHORT).show()
    }

    private fun doLogin() {
        val email = binding.emailValueTI.text.toString()
        val pwd = binding.passwordValueTI.text.toString()

        Firebase.messaging.token.addOnCompleteListener {
            if (it.isSuccessful) {
                val token = it.result
                viewModel.login(email, pwd, token)
                tokenFcm = token
            } else {
                Log.w("MainActivity", "Fetching FCM registration token failed", it.exception)
                return@addOnCompleteListener
            }
        }
    }

    private fun getTokenNotification() {
        Firebase.messaging.token.addOnCompleteListener(
            OnCompleteListener { task ->
                if (task.isSuccessful) {
                    val token = task.result
                    tokenFcm = token
                    val msg = "Generate Token succes, $token"
                    Log.d("MainActivity", msg)

                    Firebase.messaging.subscribeToTopic("promo")
                        .addOnCompleteListener { task1 ->
                            var msg1 = "Subscribed"
                            if (!task1.isSuccessful) {
                                msg1 = "Subscribe failed"
                            }
                            Log.d("MainActivity Subs", msg1)
                        }
                } else {
//                    Log.w("MainActivity", "Fetching FCM registration token failed", task.exception)
                    return@OnCompleteListener
                }
            },
        )
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
