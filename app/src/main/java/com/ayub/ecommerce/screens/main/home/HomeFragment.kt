package com.ayub.ecommerce.screens.main.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.os.LocaleListCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.ayub.ecommerce.databinding.FragmentHomeBinding
import com.ayub.ecommerce.screens.MainActivity
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private val viewModel: HomeViewModel by viewModels()
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        firebaseAnalytics = Firebase.analytics

        darkMode()
        setLang()
        binding.logoutBtn.setOnClickListener {
            viewModel.logout()
            (requireActivity() as MainActivity).logout()
            firebaseAnalytics.logEvent("btn_logout", null)
            Firebase.messaging.unsubscribeFromTopic("promo")
        }
    }

    private fun darkMode() {
        binding.darkmodeToggleBtn.isChecked = viewModel.getDarkMode()
        binding.darkmodeToggleBtn.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            }
            viewModel.setDarkMode(isChecked)
        }
    }

    private fun setLang() {
        binding.langToggleBtn.isChecked = viewModel.getLang()

        binding.langToggleBtn.setOnCheckedChangeListener { _, isChecked ->
            viewModel.setLang(isChecked)

            if (isChecked) {
                val languageIn: LocaleListCompat = LocaleListCompat.forLanguageTags("in")
                AppCompatDelegate.setApplicationLocales(languageIn)
            } else {
                val languageEn: LocaleListCompat = LocaleListCompat.forLanguageTags("en")
                AppCompatDelegate.setApplicationLocales(languageEn)
            }
        }
    }
}
