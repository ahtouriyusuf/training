package com.ayub.ecommerce.screens.main.payment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ayub.core.datasource.network.request_response.PaymentTypeItem
import com.ayub.ecommerce.databinding.ItemSubPaymentBinding
import com.ayub.ecommerce.utils.PaymentItemComparator
import com.bumptech.glide.Glide

class PaymentItemAdapter(
    private val dataList: List<PaymentTypeItem.PaymentItem>, // show data
    private val itemClickListener: PaymentItemClickListener
) :
    ListAdapter<PaymentTypeItem.PaymentItem, PaymentItemAdapter.ViewHolder>(PaymentItemComparator) {

    inner class ViewHolder(
        var binding: ItemSubPaymentBinding,
        private val itemClickListener: PaymentItemClickListener
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: PaymentTypeItem.PaymentItem?) {
            binding.apply {
                Glide.with(itemView.context)
                    .load(item?.image)
                    .fitCenter()
                    .into(binding.ivPaymentLogo)
                tvPaymentLabel.text = item?.label
                if (!item?.status!!) {
                    root.alpha = 0.5f
                    itemView.isClickable = false
                    itemView.isFocusable = false
//                    itemView.setBackgroundColor(
//                        getColorTheme(
//                            itemView.context,
//                            com.google.android.material.R.attr.colorTertiaryContainer
//                        )
//                    )
                } else {
                    root.alpha = 1f
                    itemView.setOnClickListener {
                        itemClickListener.onItemClick(item)
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemSubPaymentBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, itemClickListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = dataList[position]
        holder.bind(item)
    }

    override fun getItemCount() = dataList.size
    interface PaymentItemClickListener {
        fun onItemClick(item: PaymentTypeItem.PaymentItem)
    }
}
