package com.ayub.ecommerce.screens.main.store.review

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInHorizontally
import androidx.compose.animation.slideOutHorizontally
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Star
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.PlatformTextStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.ayub.core.datasource.network.request_response.BaseResponse
import com.ayub.core.datasource.network.request_response.ReviewData
import com.ayub.ecommerce.R
import com.ayub.ecommerce.utils.compose.EcommerceTheme
import com.ayub.ecommerce.utils.compose.poppinsFamily
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.AndroidEntryPoint
import retrofit2.HttpException
import java.io.IOException

@AndroidEntryPoint
class ReviewFragmentCompose : Fragment() {

    private lateinit var composeView: ComposeView
    private val viewModel: ReviewViewModel by viewModels()
    private val firebaseAnalytics: FirebaseAnalytics = Firebase.analytics

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        return ComposeView(requireContext()).also {
            composeView = it
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        composeView.setContent {
            EcommerceTheme {
                viewModel.idProduct = arguments?.getString(BUNDLE_PRODUCT_ID_KEY)
                val reviewState by viewModel.reviewProductData.observeAsState(initial = BaseResponse.Loading)

                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    ReviewPage(
                        reviewState = reviewState,
                        viewModel = viewModel,
                        actionBack = {
                            findNavController().navigateUp()
                            firebaseAnalytics.logEvent("btn_back_to_detail", null)
                        },
                    )
                }
            }
        }
    }

    companion object {
        const val BUNDLE_PRODUCT_ID_KEY = "id"
    }
}

// @Preview(showBackground = true)
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ReviewPage(
    reviewState: BaseResponse<List<ReviewData?>>,
    viewModel: ReviewViewModel,
    actionBack: () -> Unit,
) {
    Scaffold(
        topBar = {
            Column {
                TopAppBar(
                    title = {
                        Text(
                            text = stringResource(R.string.customers_review_txt),
                            style = TextStyle(
                                fontFamily = poppinsFamily,
                                fontWeight = FontWeight.Normal,
                                fontSize = 22.sp,
                                platformStyle = PlatformTextStyle(
                                    includeFontPadding = false
                                )
                            ),
                        )
                    },
                    navigationIcon = {
                        IconButton(
                            onClick = actionBack
                        ) {
                            Icon(
                                painter = painterResource(id = R.drawable.ic_back),
                                contentDescription = "Back to Detail Fragment"
                            )
                        }
                    }
                )
                Divider(Modifier.height(1.dp))
            }
        },
        content = {
            Column(
                Modifier
                    .padding(it)
                    .fillMaxSize()

            ) {
                when (reviewState) {
                    is BaseResponse.Loading -> LoadingScreen()
                    is BaseResponse.Success -> ListReview(reviewState.data ?: emptyList())
                    is BaseResponse.Error -> ErrorScreen(reviewState.error, viewModel)
                }
            }
        }
    )
}
@Composable
fun ListReview(reviewData: List<ReviewData?>) {
    var isVisible by remember { mutableStateOf(false) }
    LazyColumn {
        items(reviewData) { data ->
            AnimatedVisibility(
                visible = isVisible,
                enter = slideInHorizontally() + fadeIn(),
                exit = slideOutHorizontally() + fadeOut()
            ) {
                ItemReview(data = data)
            }
            LaunchedEffect(key1 = data){
                isVisible = true
            }
        }
    }
}

@Composable
fun LoadingScreen() {
    Column(
        Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        CircularProgressIndicator()
    }
}

@Composable
fun ErrorScreen(t: Throwable?, viewModel: ReviewViewModel) {
    Column(
        Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Column(
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                painter = painterResource(id = R.drawable.error_img),
                contentDescription = "error state",
                modifier = Modifier.size(128.dp)
            )
            when (t) {
                is HttpException -> {
                    if (t.response()?.code() == 404) {
                        ErrorContent(
                            code = stringResource(id = R.string.empty_list_state),
                            message = stringResource(
                                id = R.string.empty_list_description
                            ),
                            buttonText = stringResource(id = R.string.reset_txt_btn),
                            viewModel
                        )
                    } else {
                        ErrorContent(
                            code = t.response()?.code().toString(),
                            message = t.response()?.message().toString(),
                            buttonText = stringResource(id = R.string.refresh_txt_btn),
                            viewModel
                        )
                    }
                }

                is IOException -> {
                    ErrorContent(
                        code = stringResource(id = R.string.connection_error_state),
                        message = stringResource(
                            id = R.string.connection_state_description
                        ),
                        buttonText = stringResource(id = R.string.refresh_txt_btn),
                        viewModel
                    )
                }

                else -> {
                    ErrorContent(
                        code = "Error",
                        message = t?.message.toString(),
                        buttonText = stringResource(
                            id = R.string.refresh_txt_btn
                        ),
                        viewModel
                    )
                }
            }
        }
    }
}

@Composable
fun ErrorContent(code: String, message: String, buttonText: String, viewModel: ReviewViewModel) {
    Text(
        text = code,
        modifier = Modifier
            .padding(top = 8.dp),
        style = TextStyle(
            fontFamily = poppinsFamily,
            fontWeight = FontWeight.Medium,
            fontSize = 32.sp,
            platformStyle = PlatformTextStyle(
                includeFontPadding = false
            )
        )
    )
    Text(
        text = message,
        modifier = Modifier
            .padding(top = 4.dp),
        style = TextStyle(
            fontFamily = poppinsFamily,
            fontWeight = FontWeight.Normal,
            fontSize = 16.sp,
            platformStyle = PlatformTextStyle(
                includeFontPadding = false
            )
        )
    )
    Button(
        onClick = {
            viewModel.getReviewProduct()
                  },
        modifier = Modifier.padding(top = 8.dp)
    ) {
        Text(
            text = buttonText
        )
    }
}

@OptIn(ExperimentalGlideComposeApi::class)
// @Preview(showBackground = true)
@Composable
fun ItemReview(data: ReviewData?) {
    Column {
        Column(
            Modifier.padding(16.dp)
        ) {
            Row {
                GlideImage(
                    model = data?.userImage,
                    contentScale = ContentScale.Fit,
                    contentDescription = "Reviewer Image",
                    modifier = Modifier.size(36.dp)
                ) {
                    it.circleCrop()
                }
                Column(
                    Modifier.padding(start = 8.dp)
                ) {
                    Text(
                        text = data?.userName.toString(),
                        style = TextStyle(
                            fontFamily = poppinsFamily,
                            fontWeight = FontWeight.SemiBold,
                            fontSize = 12.sp
                        )
                    )
                    Row {
                        for (i in 1..5) {
                            val color =
                                if (i <= data?.userRating!!) {
                                    MaterialTheme.colorScheme.onSurfaceVariant
                                } else {
                                    MaterialTheme.colorScheme.outlineVariant
                                }
                            Icon(
                                imageVector = Icons.Default.Star,
                                contentDescription = null,
                                tint = color,
                                modifier = Modifier.size(12.dp)
                            )
                        }
                    }
                }
            }
            Text(
                text = data?.userReview.toString(),
                style = TextStyle(
                    fontFamily = poppinsFamily,
                    fontWeight = FontWeight.Normal,
                    fontSize = 12.sp
                ),
                modifier = Modifier.padding(top = 8.dp)
            )
        }
        Divider()
    }
}
