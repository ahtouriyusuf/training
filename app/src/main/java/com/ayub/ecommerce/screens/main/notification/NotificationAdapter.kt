package com.ayub.ecommerce.screens.main.notification

import android.content.res.Configuration
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ayub.core.datasource.local.room.entity.Notification
import com.ayub.ecommerce.R
import com.ayub.ecommerce.databinding.ItemNotificationBinding
import com.ayub.ecommerce.utils.NotificationComparator
import com.bumptech.glide.Glide

class NotificationAdapter : ListAdapter<Notification, NotificationAdapter.ViewHolder>(
    NotificationComparator
) {
    private var onItemClickCallback: OnItemClickCallback? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val binding =
            ItemNotificationBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, onItemClickCallback)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    interface OnItemClickCallback {
        fun onItemClick(item: Notification)
    }

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    class ViewHolder(
        private val binding: ItemNotificationBinding,
        private val onItemClickCallback: OnItemClickCallback?
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Notification) {
            itemView.setOnClickListener {
                onItemClickCallback?.onItemClick(data)
            }
            binding.apply {
                Glide.with(binding.root.context)
                    .load(data.image)
                    .fitCenter()
                    .into(ivNotification)
                tvType.text = data.type
                tvDateNTime.text = "${data.date}, ${data.time}"
                tvTitle.text = data.title
                tvBody.text = data.body
            }
            if (data.isRead) {
                when (itemView.resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
                    Configuration.UI_MODE_NIGHT_YES ->
                        binding.notificationListItem.setBackgroundColor(
                            itemView.resources.getColor(
                                R.color.black,
                                null
                            )
                        )

                    else -> {
                        binding.notificationListItem.setBackgroundColor(
                            ContextCompat.getColor(
                                itemView.getContext(),
                                android.R.color.white
                            )
                        )
                    }
                }
            } else {
                when (itemView.resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
                    Configuration.UI_MODE_NIGHT_YES ->
                        binding.notificationListItem.setBackgroundColor(
                            itemView.resources.getColor(
                                com.google.android.material.R.color.m3_sys_color_dark_outline_variant,
                                null
                            )
                        )

                    else -> {
                        binding.notificationListItem.setBackgroundColor(
                            itemView.resources.getColor(
                                com.google.android.material.R.color.m3_sys_color_light_outline_variant,
                                null
                            )
                        )
                    }
                }
            }
        }
    }
}
