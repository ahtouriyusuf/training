package com.ayub.ecommerce.utils

import android.content.Context
import android.util.TypedValue

fun getColorTheme(context: Context, colorName: Int): Int {
    val typedValue = TypedValue()
    val theme = context.theme
    theme.resolveAttribute(colorName, typedValue, true)
    return typedValue.data
}
