package com.ayub.ecommerce.utils

import androidx.recyclerview.widget.DiffUtil
import com.ayub.core.datasource.local.room.entity.WishlistProduct

object WishlistComparator : DiffUtil.ItemCallback<WishlistProduct>() {

    override fun areItemsTheSame(oldItem: WishlistProduct, newItem: WishlistProduct): Boolean {
        return oldItem.productId == newItem.productId
    }

    override fun areContentsTheSame(oldItem: WishlistProduct, newItem: WishlistProduct): Boolean {
        return oldItem == newItem
    }
}
