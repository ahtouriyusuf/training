package com.ayub.ecommerce.utils

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.navigation.NavDeepLinkBuilder
import com.ayub.core.datasource.local.room.entity.Notification
import com.ayub.core.repository.IRepository
import com.ayub.ecommerce.R
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.runBlocking
import java.util.UUID
import javax.inject.Inject

@AndroidEntryPoint
class AppFirebaseMessagingService :
    FirebaseMessagingService() {

    @Inject
    lateinit var repo: IRepository

    override fun onCreate() {
        super.onCreate()
    }

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)
        sendNotification(message)
    }

    private fun sendNotification(message: RemoteMessage) {
        val title = message.data["title"] ?: ""
        val body = message.data["body"] ?: ""

        Log.d("promo", title)
        Log.d("promo", body)

        val pendingIntent = NavDeepLinkBuilder(this)
            .setGraph(R.navigation.app_nav)
            .setDestination(R.id.notificationFragment)
            .createPendingIntent()

        val channelId = getString(R.string.app_name)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.drawable.splash_screen_icon)
            .setContentTitle(title)
            .setContentText(body)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(pendingIntent)

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "Channel human readable title",
                NotificationManager.IMPORTANCE_DEFAULT,
            )
            notificationManager.createNotificationChannel(channel)
        }

        val notificationId = System.currentTimeMillis().toInt()
        notificationManager.notify(notificationId, notificationBuilder.build())

        val notifEntity = Notification(
            title = title,
            body = message.data["body"] ?: "",
            date = message.data["date"] ?: "",
            id = UUID.randomUUID().toString(),
            image = message.data["image"] ?: "",
            type = message.data["type"] ?: "",
            isRead = false,
            time = message.data["time"] ?: "",
        )
        runBlocking {
            repo.addToNotification(notifEntity)
        }
    }
}
