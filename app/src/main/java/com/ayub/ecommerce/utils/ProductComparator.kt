package com.ayub.ecommerce.utils

import androidx.recyclerview.widget.DiffUtil
import com.ayub.core.datasource.network.request_response.Items

object ProductComparator : DiffUtil.ItemCallback<Items>() {
    override fun areItemsTheSame(oldItem: Items, newItem: Items): Boolean {
        return oldItem.productId == newItem.productId
    }

    override fun areContentsTheSame(oldItem: Items, newItem: Items): Boolean {
        return oldItem == newItem
    }
}
