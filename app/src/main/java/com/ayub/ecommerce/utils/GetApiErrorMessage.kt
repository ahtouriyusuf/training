package com.ayub.ecommerce.utils

import com.ayub.core.datasource.network.request_response.ErrorResponse
import com.google.gson.Gson
import retrofit2.HttpException

fun getApiErrorMessage(t: Throwable): String? {
    var message = t.message
    if (t is HttpException) {
        val errorResponse = Gson().fromJson(
            t.response()?.errorBody()?.string(),
            ErrorResponse::class.java
        ) ?: ErrorResponse()
        errorResponse.message?.let { message = it }
    }
    return message
}
