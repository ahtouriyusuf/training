package com.ayub.ecommerce.utils

import androidx.recyclerview.widget.DiffUtil
import com.ayub.core.datasource.local.room.entity.Cart

object CartComparator : DiffUtil.ItemCallback<Cart>() {
    override fun areItemsTheSame(oldItem: Cart, newItem: Cart): Boolean {
        return oldItem.productId == newItem.productId
    }

    override fun areContentsTheSame(oldItem: Cart, newItem: Cart): Boolean {
        return oldItem == newItem
    }
}
