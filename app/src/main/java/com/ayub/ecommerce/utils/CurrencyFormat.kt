package com.ayub.ecommerce.utils

import java.text.NumberFormat
import java.util.Locale

fun Number.toCurrencyFormat(lang: String = "in", country: String = "ID"): String {
    val localId = Locale(lang, country)
    val formatter = NumberFormat.getCurrencyInstance(localId)
    formatter.maximumFractionDigits = 0
    return formatter.format(this)
}
