package com.ayub.ecommerce.utils

import androidx.recyclerview.widget.DiffUtil
import com.ayub.core.datasource.network.request_response.PaymentTypeItem

object PaymentComparator : DiffUtil.ItemCallback<PaymentTypeItem>() {
    override fun areItemsTheSame(
        oldItem: PaymentTypeItem,
        newItem: PaymentTypeItem
    ): Boolean = oldItem.title == newItem.title

    override fun areContentsTheSame(
        oldItem: PaymentTypeItem,
        newItem: PaymentTypeItem
    ): Boolean = oldItem == newItem
}
