package com.ayub.ecommerce.utils

import androidx.recyclerview.widget.DiffUtil
import com.ayub.core.datasource.network.request_response.PaymentTypeItem

object PaymentItemComparator : DiffUtil.ItemCallback<PaymentTypeItem.PaymentItem>() {
    override fun areItemsTheSame(
        oldItem: PaymentTypeItem.PaymentItem,
        newItem: PaymentTypeItem.PaymentItem
    ): Boolean = oldItem.label == newItem.label

    override fun areContentsTheSame(
        oldItem: PaymentTypeItem.PaymentItem,
        newItem: PaymentTypeItem.PaymentItem
    ): Boolean = oldItem == newItem
}
