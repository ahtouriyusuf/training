package com.ayub.ecommerce.utils

import androidx.recyclerview.widget.DiffUtil
import com.ayub.core.datasource.local.room.entity.Notification

object NotificationComparator : DiffUtil.ItemCallback<Notification>() {
    override fun areItemsTheSame(oldItem: Notification, newItem: Notification): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Notification, newItem: Notification): Boolean {
        return oldItem == newItem
    }
}
