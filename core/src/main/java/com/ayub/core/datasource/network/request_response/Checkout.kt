package com.ayub.core.datasource.network.request_response

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize

@Keep
@Parcelize
data class Checkout(
    val productId: String,
    val variantName: String,
    val quantity: Int
) : Parcelable
