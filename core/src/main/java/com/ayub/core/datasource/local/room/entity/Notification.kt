package com.ayub.core.datasource.local.room.entity

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Entity(tableName = "notification")
@Parcelize
data class Notification(
    @PrimaryKey
    val id: String,
    val type: String,
    val date: String,
    val time: String,
    val title: String,
    val body: String,
    val image: String,
    var isRead: Boolean,
) : Parcelable
