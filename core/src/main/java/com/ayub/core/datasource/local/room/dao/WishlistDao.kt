package com.ayub.core.datasource.local.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.ayub.core.datasource.local.room.entity.WishlistProduct

@Dao
interface WishlistDao {
    @Query("SELECT * FROM WISHLIST")
    fun getAllProducts(): LiveData<List<WishlistProduct>>

    @Query("SELECT EXISTS(SELECT * FROM wishlist WHERE product_id = :id)")
    suspend fun checkProductIsExist(id: String): Boolean

    @Insert
    suspend fun insertProduct(product: WishlistProduct)

    @Delete
    suspend fun deleteProduct(product: WishlistProduct)
}
