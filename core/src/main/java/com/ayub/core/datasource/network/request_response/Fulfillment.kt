package com.ayub.core.datasource.network.request_response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class FulfillmentRequest(
    val payment: String?,
    val items: List<Checkout>
)

data class FulfillmentResponse(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val data: FulfillmentData? = null,

    @field:SerializedName("message")
    val message: String? = null
)

@Parcelize
data class FulfillmentData(

    @field:SerializedName("date")
    val date: String? = null,

    @field:SerializedName("total")
    val total: Int? = null,

    @field:SerializedName("invoiceId")
    val invoiceId: String? = null,

    @field:SerializedName("payment")
    val payment: String? = null,

    @field:SerializedName("time")
    val time: String? = null,

    @field:SerializedName("status")
    val status: Boolean? = null
) : Parcelable
