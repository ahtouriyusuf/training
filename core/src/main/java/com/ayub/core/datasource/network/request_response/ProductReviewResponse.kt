package com.ayub.core.datasource.network.request_response

import com.google.gson.annotations.SerializedName

data class ProductReviewResponse(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val data: List<ReviewData?>? = null,

    @field:SerializedName("message")
    val message: String? = null
)

data class ReviewData(

    @field:SerializedName("userImage")
    val userImage: String? = null,

    @field:SerializedName("userName")
    val userName: String? = null,

    @field:SerializedName("userReview")
    val userReview: String? = null,

    @field:SerializedName("userRating")
    val userRating: Int? = null
)
