package com.ayub.core.datasource.network

import com.ayub.core.datasource.network.request_response.DetailProductResponse
import com.ayub.core.datasource.network.request_response.FulfillmentRequest
import com.ayub.core.datasource.network.request_response.FulfillmentResponse
import com.ayub.core.datasource.network.request_response.LoginRequest
import com.ayub.core.datasource.network.request_response.LoginResponse
import com.ayub.core.datasource.network.request_response.PaymentResponse
import com.ayub.core.datasource.network.request_response.ProductResponse
import com.ayub.core.datasource.network.request_response.ProductReviewResponse
import com.ayub.core.datasource.network.request_response.ProfileResponse
import com.ayub.core.datasource.network.request_response.RatingRequest
import com.ayub.core.datasource.network.request_response.RatingResponse
import com.ayub.core.datasource.network.request_response.RefreshTokenRequest
import com.ayub.core.datasource.network.request_response.RefreshTokenResponse
import com.ayub.core.datasource.network.request_response.RegisterRequest
import com.ayub.core.datasource.network.request_response.RegisterResponse
import com.ayub.core.datasource.network.request_response.SearchProductResponse
import com.ayub.core.datasource.network.request_response.TransactionResponse
import okhttp3.MultipartBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.QueryMap

interface ApiServices {
    @POST("login")
    suspend fun login(
        @Body loginRequest: LoginRequest,
    ): LoginResponse

    @POST("register")
    suspend fun register(
        @Body registerRequest: RegisterRequest,
    ): RegisterResponse

    @POST("refresh")
    suspend fun refresh(
        @Body refreshTokenRequest: RefreshTokenRequest
    ): RefreshTokenResponse

    @Multipart
    @POST("profile")
    suspend fun profile(
        @Part userName: MultipartBody.Part,
        @Part userImage: MultipartBody.Part?
    ): ProfileResponse

    @POST("products")
    suspend fun products(
        @QueryMap query: Map<String, String>
    ): ProductResponse

    @POST("search")
    suspend fun searchProducts(
        @Query("query") query: String
    ): SearchProductResponse

    @GET("products/{id}")
    suspend fun detailProduct(
        @Path("id") id: String?
    ): DetailProductResponse

    @GET("review/{id}")
    suspend fun reviewProduct(
        @Path("id") id: String?
    ): ProductReviewResponse

    @GET("payment")
    suspend fun payment(): PaymentResponse

    @POST("fulfillment")
    suspend fun fulfillment(
        @Body fulfillmentRequest: FulfillmentRequest
    ): FulfillmentResponse

    @POST("rating")
    suspend fun rating(
        @Body ratingRequest: RatingRequest
    ): RatingResponse

    @GET("transaction")
    suspend fun transaction(): TransactionResponse
}
