package com.ayub.core.datasource.local.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.ayub.core.datasource.local.room.dao.CartDao
import com.ayub.core.datasource.local.room.dao.NotificationDao
import com.ayub.core.datasource.local.room.dao.WishlistDao
import com.ayub.core.datasource.local.room.entity.Cart
import com.ayub.core.datasource.local.room.entity.Notification
import com.ayub.core.datasource.local.room.entity.WishlistProduct

@Database(entities = [WishlistProduct::class, Cart::class, Notification::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun wishlistDao(): WishlistDao
    abstract fun cartDao(): CartDao
    abstract fun notificationDao(): NotificationDao
}
