package com.ayub.core.datasource.network.request_response

import com.google.gson.annotations.SerializedName

data class RefreshTokenRequest(
    @field:SerializedName("token")
    val token: String? = null,
)

data class RefreshTokenResponse(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val data: RefreshTokenData? = null,

    @field:SerializedName("message")
    val message: String? = null
)

data class RefreshTokenData(

    @field:SerializedName("accessToken")
    val accessToken: String? = null,

    @field:SerializedName("expiresAt")
    val expiresAt: Int? = null,

    @field:SerializedName("refreshToken")
    val refreshToken: String? = null
)
