package com.ayub.core.datasource

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.ayub.core.datasource.network.ApiServices
import com.ayub.core.datasource.network.request_response.Items
import com.ayub.core.datasource.network.request_response.ProductRequest
import com.ayub.core.helper.toQueryMap

class ProductPagingSource(private val apiServices: ApiServices, private val query: ProductRequest) :
    PagingSource<Int, Items>() {
    override fun getRefreshKey(state: PagingState<Int, Items>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Items> {
        return try {
            val position = params.key ?: 1
            val response = apiServices.products(
                query.copy(
                    page = position,
                    limit = params.loadSize
                ).toQueryMap()
            )
            LoadResult.Page(
                data = response.data?.items ?: emptyList(),
                prevKey = if (position == 1) null else position - 1,
                nextKey = if (position == response.data?.totalPages) null else position + 1
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }
}
