package com.ayub.core.datasource.network.request_response

import com.google.gson.annotations.SerializedName

data class ProductRequest(
    val search: String? = null,
    val brand: String? = null,
    val lowest: String? = null,
    val highest: String? = null,
    val sort: String? = null,
    val limit: Int? = null,
    val page: Int? = null
)

data class ProductResponse(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val data: ProductData? = null,

    @field:SerializedName("message")
    val message: String? = null
)

data class ProductData(

    @field:SerializedName("pageIndex")
    val pageIndex: Int? = null,

    @field:SerializedName("itemsPerPage")
    val itemsPerPage: Int? = null,

    @field:SerializedName("currentItemCount")
    val currentItemCount: Int? = null,

    @field:SerializedName("totalPages")
    val totalPages: Int? = null,

    @field:SerializedName("items")
    val items: List<Items>? = null
)

data class Items(

    @field:SerializedName("image")
    val image: String? = null,

    @field:SerializedName("sale")
    val sale: Int? = null,

    @field:SerializedName("productId")
    val productId: String? = null,

    @field:SerializedName("store")
    val store: String? = null,

    @field:SerializedName("productRating")
    val productRating: Any? = null,

    @field:SerializedName("brand")
    val brand: String? = null,

    @field:SerializedName("productName")
    val productName: String? = null,

    @field:SerializedName("productPrice")
    val productPrice: Int? = null
)
