package com.ayub.core.datasource.local.room.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Entity(tableName = "cart")
@Parcelize
data class Cart(
    @PrimaryKey
    @ColumnInfo(name = "product_id") val productId: String,
    @ColumnInfo(name = "product_name") val productName: String,
    @ColumnInfo(name = "product_price") val productPrice: Int,
    @ColumnInfo(name = "image") val image: String,
    @ColumnInfo(name = "brand") val brand: String,
    @ColumnInfo(name = "description") val description: String,
    @ColumnInfo(name = "store") val store: String,
    @ColumnInfo(name = "sale") val sale: Int,
    @ColumnInfo(name = "stock") val stock: Int?,
    @ColumnInfo(name = "total_rating") val totalRating: Int,
    @ColumnInfo(name = "total_review") val totalReview: Int,
    @ColumnInfo(name = "total_satisfaction") val totalSatisfaction: Int,
    @ColumnInfo(name = "product_rating") val productRating: Float,
    @ColumnInfo(name = "variant_name") var variantName: String,
    @ColumnInfo(name = "variant_price") var variantPrice: Int?,
    @ColumnInfo(name = "quantity") var quantity: Int = 1,
    @ColumnInfo(name = "selected") var selected: Boolean = false,
) : Parcelable
