package com.ayub.core.datasource.local.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.ayub.core.datasource.local.room.entity.Cart

@Dao
interface CartDao {
    @Query("SELECT * FROM cart")
    fun getAllProducts(): LiveData<List<Cart>>

    @Query("SELECT EXISTS(SELECT * FROM cart WHERE product_id = :cartId)")
    suspend fun checkExistData(cartId: String): Boolean

    @Query("SELECT * FROM cart WHERE product_id = :cartId")
    suspend fun getProductById(cartId: String): Cart?

    @Query("UPDATE cart SET quantity = :newQuantity WHERE product_id = :productId")
    suspend fun updateCartItemQuantity(productId: String, newQuantity: Int)

    @Query("UPDATE cart SET selected = :isSelected WHERE product_id IN (:productId)")
    suspend fun updateCartItemCheckbox(productId: List<String>, isSelected: Boolean)

    @Insert
    suspend fun insertProductToCart(cart: Cart)

    @Delete
    suspend fun deleteProductFromCart(cart: Cart)

    @Query("DELETE FROM cart WHERE product_id IN (:productId)")
    fun deleteAllProduct(productId: List<String>)

    @Query("DELETE FROM cart")
    fun deleteAllCart()
}
