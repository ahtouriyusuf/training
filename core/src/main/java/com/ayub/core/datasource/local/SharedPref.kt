package com.ayub.core.datasource.local

import android.content.Context
import android.content.SharedPreferences
import javax.inject.Inject

class SharedPref @Inject constructor(context: Context) {
    private var sharedPreferences: SharedPreferences =
        context.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
    private var editor: SharedPreferences.Editor = sharedPreferences.edit()

    fun setIsFirstLaunchToFalse() {
        editor.putBoolean(IS_FIRST_LAUNCH, false)
        editor.commit()
    }

    fun isFirstLaunch(): Boolean {
        return sharedPreferences.getBoolean(IS_FIRST_LAUNCH, true)
    }

    fun setLanguageToId(lang: Boolean) {
        editor.putBoolean(IS_LANG_ID, lang)
        editor.commit()
    }

    fun isLangID(): Boolean {
        return sharedPreferences.getBoolean(IS_LANG_ID, false)
    }

    fun setDarkMode(darkMode: Boolean) {
        editor.putBoolean(IS_DARK_MODE, darkMode)
        editor.commit()
    }

    fun isDarkModeEnabled(): Boolean {
        return sharedPreferences.getBoolean(IS_DARK_MODE, false)
    }

    fun saveAccessToken(token: String) {
        saveString(ACCESS_TOKEN, token)
    }

    fun saveRefreshToken(token: String) {
        saveString(REFRESH_TOKEN, token)
    }

    fun saveUsername(name: String) {
        saveString(NAME, name)
    }

    fun getAccessToken(): String? {
        return sharedPreferences.getString(ACCESS_TOKEN, null)
    }

    fun getRefreshToken(): String? {
        return sharedPreferences.getString(REFRESH_TOKEN, null)
    }

    fun getUsername(): String? {
        return sharedPreferences.getString(NAME, null)
    }

    fun saveString(key: String, value: String) {
        editor.putString(key, value)
        editor.apply()
    }

    fun clearData() {
        editor.remove(ACCESS_TOKEN)
        editor.remove(REFRESH_TOKEN)
        editor.remove(NAME)
        editor.apply()
    }

    companion object {
        private const val IS_FIRST_LAUNCH = "is_first_launch"
        private const val IS_LANG_ID = "is_lang_id"
        private const val IS_DARK_MODE = "is_dark_mode"
        private const val REFRESH_TOKEN = "refresh_token"
        private const val ACCESS_TOKEN = "access_token"
        private const val NAME = "name"
    }
}
