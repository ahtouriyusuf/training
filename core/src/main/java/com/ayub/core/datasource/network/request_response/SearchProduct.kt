package com.ayub.core.datasource.network.request_response

import com.google.gson.annotations.SerializedName

data class SearchProductResponse(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val data: List<String>,

    @field:SerializedName("message")
    val message: String? = null
)
