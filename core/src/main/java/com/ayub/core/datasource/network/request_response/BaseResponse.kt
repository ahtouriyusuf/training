package com.ayub.core.datasource.network.request_response

sealed class BaseResponse<out T> {
    data object Loading : BaseResponse<Nothing>()
    data class Success<T>(val data: T? = null) : BaseResponse<T>()
    data class Error(val error: Throwable?) : BaseResponse<Nothing>()
}
