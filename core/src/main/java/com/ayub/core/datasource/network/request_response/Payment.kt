package com.ayub.core.datasource.network.request_response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class PaymentResponse(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val data: List<PaymentTypeItem?>? = null,

    @field:SerializedName("message")
    val message: String? = null
)

data class PaymentTypeItem(

    @field:SerializedName("item")
    val item: List<PaymentItem>?,

    @field:SerializedName("title")
    val title: String? = null
) {
    @Parcelize
    data class PaymentItem(

        @field:SerializedName("image")
        val image: String? = null,

        @field:SerializedName("label")
        val label: String? = null,

        @field:SerializedName("status")
        val status: Boolean? = null
    ) : Parcelable
}
