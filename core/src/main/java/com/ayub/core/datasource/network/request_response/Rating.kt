package com.ayub.core.datasource.network.request_response

import com.google.gson.annotations.SerializedName

data class RatingRequest(

    @field:SerializedName("invoiceId")
    val invoiceId: String,

    @field:SerializedName("rating")
    val rating: Int? = null,

    @field:SerializedName("review")
    val review: String? = null

)

data class RatingResponse(
    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("message")
    val message: String? = null
)
