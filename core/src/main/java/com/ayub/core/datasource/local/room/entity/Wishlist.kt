package com.ayub.core.datasource.local.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "wishlist")
data class WishlistProduct(
    @PrimaryKey
    @ColumnInfo(name = "product_id") val productId: String,
    @ColumnInfo(name = "product_name") val productName: String? = null,
    @ColumnInfo(name = "product_price") val productPrice: Int? = null,
    @ColumnInfo(name = "image") val image: String? = null,
    @ColumnInfo(name = "brand") val brand: String? = null,
    @ColumnInfo(name = "description") val description: String? = null,
    @ColumnInfo(name = "store") val store: String? = null,
    @ColumnInfo(name = "sale") val sale: Int? = null,
    @ColumnInfo(name = "stock") val stock: Int? = null,
    @ColumnInfo(name = "total_rating") val totalRating: Int? = null,
    @ColumnInfo(name = "total_review") val totalReview: Int? = null,
    @ColumnInfo(name = "total_satisfaction") val totalSatisfaction: Int? = null,
    @ColumnInfo(name = "product_rating") val productRating: Float? = null,
    @ColumnInfo(name = "variant_name") var variantName: String,
    @ColumnInfo(name = "variant_price") var variantPrice: Int?,
    @ColumnInfo(name = "quantity") var quantity: Int = 1,
    @ColumnInfo(name = "selected") var selected: Boolean = false
)
