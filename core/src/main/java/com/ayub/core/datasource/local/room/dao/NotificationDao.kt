package com.ayub.core.datasource.local.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.ayub.core.datasource.local.room.entity.Notification

@Dao
interface NotificationDao {
    @Query("SELECT * FROM notification")
    fun getNotification(): LiveData<List<Notification>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addToNotification(notification: Notification)

    @Update
    suspend fun updateNotification(notification: Notification)
}
