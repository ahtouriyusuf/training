package com.ayub.core.di

import android.content.Context
import androidx.room.Room
import com.ayub.core.datasource.local.SharedPref
import com.ayub.core.datasource.local.room.AppDatabase
import com.ayub.core.datasource.network.ApiServices
import com.ayub.core.helper.Authenticator
import com.ayub.core.helper.Constants.BASE_URL
import com.ayub.core.repository.IRepository
import com.ayub.core.repository.Repository
import com.chuckerteam.chucker.api.ChuckerCollector
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.chuckerteam.chucker.api.RetentionManager
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class AppModule {

    companion object {
        @Singleton
        @Provides
        fun provideSharedPreferences(@ApplicationContext context: Context): SharedPref =
            SharedPref(context = context)

        @Singleton
        @Provides
        fun provideApiServices(
            @ApplicationContext context: Context,
            interceptor: com.ayub.core.helper.Interceptor,
            authenticator: Authenticator
        ): ApiServices {
            val client = OkHttpClient.Builder()
                .apply {
                    addInterceptor(
                        ChuckerInterceptor.Builder(context)
                            .collector(
                                ChuckerCollector(
                                    context = context,
                                    showNotification = true,
                                    retentionPeriod = RetentionManager.Period.ONE_HOUR
                                )
                            )
                            .build()
                    )
                    addInterceptor(interceptor)
                }
                .authenticator(authenticator)
                .build()

            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()

            return retrofit.create(ApiServices::class.java)
        }

        @Provides
        @Singleton
        fun provideAppDatabase(@ApplicationContext context: Context): AppDatabase =
            Room.databaseBuilder(
                context,
                AppDatabase::class.java,
                "app_database"
            ).allowMainThreadQueries().build()
    }

    @Singleton
    @Binds
    abstract fun bindRepository(repository: Repository): IRepository
}
