package com.ayub.core.repository

import androidx.lifecycle.LiveData
import androidx.paging.PagingData
import com.ayub.core.datasource.local.room.entity.Cart
import com.ayub.core.datasource.local.room.entity.Notification
import com.ayub.core.datasource.local.room.entity.WishlistProduct
import com.ayub.core.datasource.network.request_response.BaseResponse
import com.ayub.core.datasource.network.request_response.DetailProductData
import com.ayub.core.datasource.network.request_response.FulfillmentData
import com.ayub.core.datasource.network.request_response.Items
import com.ayub.core.datasource.network.request_response.LoginData
import com.ayub.core.datasource.network.request_response.PaymentTypeItem
import com.ayub.core.datasource.network.request_response.ProductRequest
import com.ayub.core.datasource.network.request_response.ProfileData
import com.ayub.core.datasource.network.request_response.RegisterData
import com.ayub.core.datasource.network.request_response.ReviewData
import com.ayub.core.datasource.network.request_response.SearchProductResponse
import com.ayub.core.datasource.network.request_response.TransactionData
import okhttp3.MultipartBody

interface IRepository {
    fun login(email: String, pwd: String, firebaseToken: String): LiveData<BaseResponse<LoginData>>
    fun register(
        email: String,
        pwd: String,
        firebaseToken: String
    ): LiveData<BaseResponse<RegisterData>>

    fun saveProfile(
        userName: MultipartBody.Part,
        userImage: MultipartBody.Part?
    ): LiveData<BaseResponse<ProfileData>>

    fun logout()

    fun getProducts(query: ProductRequest): LiveData<PagingData<Items>>
    fun searchProducts(query: String): LiveData<BaseResponse<SearchProductResponse>>
    fun getDetailProduct(id: String?): LiveData<BaseResponse<DetailProductData>>
    fun getProductReview(id: String?): LiveData<BaseResponse<List<ReviewData?>>>

    suspend fun saveToWishlist(wishlist: WishlistProduct)
    fun getWishlistProducts(): LiveData<List<WishlistProduct>>
    suspend fun checkProductInWishlist(id: String): Boolean
    suspend fun deleteFromWishlist(wishlist: WishlistProduct)

    suspend fun addToCart(cartProduct: Cart)
    fun getCartProducts(): LiveData<List<Cart>>
    suspend fun getCartProductById(id: String): Cart?
    suspend fun checkProductInCart(id: String): Boolean
    suspend fun deleteProductInCart(cartProduct: Cart)
    fun deleteAllProductInCart(id: List<String>)
    suspend fun updateCartItemQuantity(id: String, newQuantity: Int)
    suspend fun updateCartItemSelected(id: List<String>, isSelected: Boolean)
    fun getPayment(): LiveData<BaseResponse<List<PaymentTypeItem?>>>
    fun fulfillment(payment: String, items: List<Cart>): LiveData<BaseResponse<FulfillmentData>>
    fun rating(invoiceId: String, rating: Int?, review: String?): LiveData<BaseResponse<Boolean>>
    fun transaction(): LiveData<BaseResponse<List<TransactionData?>>>

    fun getAllNotification(): LiveData<List<Notification>>?
    suspend fun addToNotification(notification: Notification)
    suspend fun updateNotification(notification: Notification)

    // token management
    fun isFirstLaunch(): Boolean
    fun setIsFirstLaunchToFalse()

    fun isLangId(): Boolean
    fun setLangId(lang: Boolean)
    fun isDarkModeEnabled(): Boolean
    fun setDarkMode(darkMode: Boolean)
    fun saveAccessToken(token: String)
    fun saveRefreshToken(token: String)
    fun saveUsername(name: String)
    fun getAccessToken(): String?
    fun getRefreshToken(): String?
    fun getUsername(): String?
    fun clearData()
}
