package com.ayub.core.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.map
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.liveData
import androidx.paging.map
import com.ayub.core.datasource.ProductPagingSource
import com.ayub.core.datasource.local.SharedPref
import com.ayub.core.datasource.local.room.AppDatabase
import com.ayub.core.datasource.local.room.entity.Cart
import com.ayub.core.datasource.local.room.entity.Notification
import com.ayub.core.datasource.local.room.entity.WishlistProduct
import com.ayub.core.datasource.network.ApiServices
import com.ayub.core.datasource.network.request_response.BaseResponse
import com.ayub.core.datasource.network.request_response.DetailProductData
import com.ayub.core.datasource.network.request_response.FulfillmentData
import com.ayub.core.datasource.network.request_response.FulfillmentRequest
import com.ayub.core.datasource.network.request_response.Items
import com.ayub.core.datasource.network.request_response.LoginData
import com.ayub.core.datasource.network.request_response.LoginRequest
import com.ayub.core.datasource.network.request_response.PaymentTypeItem
import com.ayub.core.datasource.network.request_response.ProductRequest
import com.ayub.core.datasource.network.request_response.ProfileData
import com.ayub.core.datasource.network.request_response.RatingRequest
import com.ayub.core.datasource.network.request_response.RegisterData
import com.ayub.core.datasource.network.request_response.RegisterRequest
import com.ayub.core.datasource.network.request_response.ReviewData
import com.ayub.core.datasource.network.request_response.SearchProductResponse
import com.ayub.core.datasource.network.request_response.TransactionData
import com.ayub.core.helper.toCheckout
import com.ayub.core.helper.toProduct
import okhttp3.MultipartBody
import javax.inject.Inject

class Repository @Inject constructor(
    private val sharedPref: SharedPref,
    private val apiServices: ApiServices,
    private val appDatabase: AppDatabase
) : IRepository {
    override fun login(
        email: String,
        pwd: String,
        firebaseToken: String
    ): LiveData<BaseResponse<LoginData>> = liveData {
        emit(BaseResponse.Loading)
        try {
            val loginRequest =
                LoginRequest(email = email, password = pwd, firebaseToken = firebaseToken)
            val response = apiServices.login(loginRequest)
            emit(BaseResponse.Success(response.data))
            saveAccessToken(response.data?.accessToken.toString())
            saveRefreshToken(response.data?.refreshToken.toString())
            saveUsername(response.data?.userName.toString())
        } catch (e: Exception) {
            emit(BaseResponse.Error(e))
        }
    }

    override fun register(
        email: String,
        pwd: String,
        firebaseToken: String
    ): LiveData<BaseResponse<RegisterData>> =
        liveData {
            emit(BaseResponse.Loading)
            try {
                val registerRequest = RegisterRequest(email, pwd, firebaseToken)
                val response = apiServices.register(registerRequest)
                emit(BaseResponse.Success(response.data))
                saveAccessToken(response.data?.accessToken.toString())
                saveRefreshToken(response.data?.refreshToken.toString())
            } catch (e: Exception) {
                emit(BaseResponse.Error(e))
            }
        }

    override fun saveProfile(
        userName: MultipartBody.Part,
        userImage: MultipartBody.Part?
    ): LiveData<BaseResponse<ProfileData>> = liveData {
        emit(BaseResponse.Loading)
        try {
            val response = apiServices.profile(userName, userImage)
            val resultResponse = response.data
            sharedPref.saveUsername(resultResponse?.userName.toString())
            emit(BaseResponse.Success(resultResponse))
        } catch (e: Exception) {
            emit(BaseResponse.Error(e))
        }
    }

    override fun logout() {
        clearData()
        appDatabase.clearAllTables()
    }

    override fun getProducts(query: ProductRequest): LiveData<PagingData<Items>> {
        return Pager(
            config = PagingConfig(pageSize = 10, initialLoadSize = 10, prefetchDistance = 1),
            pagingSourceFactory = {
                ProductPagingSource(apiServices, query)
            },
        ).liveData.map {
            it.map { items ->
                items.toProduct()
            }
        }
    }

    override fun searchProducts(query: String): LiveData<BaseResponse<SearchProductResponse>> =
        liveData {
            emit(BaseResponse.Loading)
            try {
                val response = apiServices.searchProducts(query)
                emit(BaseResponse.Success(response))
            } catch (t: Throwable) {
                emit(BaseResponse.Error(t))
            }
        }

    override fun getDetailProduct(id: String?): LiveData<BaseResponse<DetailProductData>> =
        liveData {
            emit(BaseResponse.Loading)
            try {
                val response = apiServices.detailProduct(id)
                emit(BaseResponse.Success(response.data))
            } catch (t: Throwable) {
                emit(BaseResponse.Error(t))
            }
        }

    override fun getProductReview(id: String?): LiveData<BaseResponse<List<ReviewData?>>> =
        liveData {
            emit(BaseResponse.Loading)
            try {
                val response = apiServices.reviewProduct(id)
                val data = response.data
                emit(BaseResponse.Success(data))
            } catch (t: Throwable) {
                emit(BaseResponse.Error(t))
            }
        }

    override suspend fun saveToWishlist(wishlist: WishlistProduct) {
        appDatabase.wishlistDao().insertProduct(wishlist)
    }

    override fun getWishlistProducts(): LiveData<List<WishlistProduct>> =
        appDatabase.wishlistDao().getAllProducts()

    override suspend fun checkProductInWishlist(id: String): Boolean {
        return appDatabase.wishlistDao().checkProductIsExist(id)
    }

    override suspend fun deleteFromWishlist(wishlist: WishlistProduct) {
        appDatabase.wishlistDao().deleteProduct(wishlist)
    }

    override suspend fun addToCart(cartProduct: Cart) {
        appDatabase.cartDao().insertProductToCart(cartProduct)
    }

    override fun getCartProducts(): LiveData<List<Cart>> {
        return appDatabase.cartDao().getAllProducts()
    }

    override suspend fun getCartProductById(id: String): Cart? {
        return appDatabase.cartDao().getProductById(id)
    }

    override suspend fun checkProductInCart(id: String): Boolean {
        return appDatabase.cartDao().checkExistData(id)
    }

    override suspend fun deleteProductInCart(cartProduct: Cart) {
        appDatabase.cartDao().deleteProductFromCart(cartProduct)
    }

    override fun deleteAllProductInCart(id: List<String>) {
        appDatabase.cartDao().deleteAllProduct(id)
    }

    override suspend fun updateCartItemQuantity(id: String, newQuantity: Int) {
        appDatabase.cartDao().updateCartItemQuantity(id, newQuantity)
    }

    override suspend fun updateCartItemSelected(id: List<String>, isSelected: Boolean) {
        appDatabase.cartDao().updateCartItemCheckbox(id, isSelected)
    }

    override fun getPayment(): LiveData<BaseResponse<List<PaymentTypeItem?>>> = liveData {
        emit(BaseResponse.Loading)
        try {
            val response = apiServices.payment()
            val data = response.data
            if (data != null) {
                emit(BaseResponse.Success(data))
            } else {
                throw Exception("No data")
            }
        } catch (t: Throwable) {
            emit(BaseResponse.Error(t))
        }
    }

    override fun fulfillment(
        payment: String,
        items: List<Cart>
    ): LiveData<BaseResponse<FulfillmentData>> = liveData {
        emit(BaseResponse.Loading)
        try {
            val checkoutData = items.map { it.toCheckout() }
            val request = FulfillmentRequest(payment, checkoutData)
            val response = apiServices.fulfillment(request)
            val data = response.data
            if (data != null) {
                emit(BaseResponse.Success(data))
                appDatabase.cartDao().deleteAllCart()
            } else {
                throw Exception("No Data")
            }
        } catch (e: Exception) {
            emit(BaseResponse.Error(e))
        }
    }

    override fun rating(
        invoiceId: String,
        rating: Int?,
        review: String?
    ): LiveData<BaseResponse<Boolean>> = liveData {
        emit(BaseResponse.Loading)
        try {
            val response = apiServices.rating(RatingRequest(invoiceId, rating, review))
            if (response.code == 200) {
                emit(BaseResponse.Success(true))
            } else {
                throw Exception("Failed")
            }
        } catch (e: Exception) {
            emit(BaseResponse.Error(e))
        }
    }

    override fun transaction(): LiveData<BaseResponse<List<TransactionData?>>> = liveData {
        emit(BaseResponse.Loading)
        try {
            val response = apiServices.transaction()
            emit(BaseResponse.Success(response.data))
        } catch (e: Exception) {
            emit(BaseResponse.Error(e))
        }
    }

    override fun getAllNotification(): LiveData<List<Notification>>? {
        return appDatabase.notificationDao().getNotification()
    }

    override suspend fun addToNotification(notification: Notification) {
        return appDatabase.notificationDao().addToNotification(notification)
    }

    override suspend fun updateNotification(notification: Notification) {
        return appDatabase.notificationDao().updateNotification(notification)
    }

    override fun isFirstLaunch(): Boolean = sharedPref.isFirstLaunch()
    override fun setIsFirstLaunchToFalse() {
        sharedPref.setIsFirstLaunchToFalse()
    }

    override fun isLangId(): Boolean = sharedPref.isLangID()

    override fun setLangId(lang: Boolean) {
        sharedPref.setLanguageToId(lang)
    }

    override fun isDarkModeEnabled(): Boolean = sharedPref.isDarkModeEnabled()

    override fun setDarkMode(darkMode: Boolean) {
        sharedPref.setDarkMode(darkMode)
    }

    override fun saveAccessToken(token: String) {
        sharedPref.saveAccessToken(token)
    }

    override fun saveRefreshToken(token: String) {
        sharedPref.saveRefreshToken(token)
    }

    override fun saveUsername(name: String) {
        sharedPref.saveUsername(name)
    }

    override fun getAccessToken(): String? {
        return sharedPref.getAccessToken()
    }

    override fun getRefreshToken(): String? {
        return sharedPref.getRefreshToken()
    }

    override fun getUsername(): String? {
        return sharedPref.getUsername()
    }

    override fun clearData() {
        return sharedPref.clearData()
    }
}
