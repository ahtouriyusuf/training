package com.ayub.core.helper

import com.ayub.core.datasource.local.room.entity.Cart
import com.ayub.core.datasource.local.room.entity.WishlistProduct
import com.ayub.core.datasource.network.request_response.Checkout
import com.ayub.core.datasource.network.request_response.DetailProductData
import com.ayub.core.datasource.network.request_response.FulfillmentData
import com.ayub.core.datasource.network.request_response.Items
import com.ayub.core.datasource.network.request_response.ProductRequest
import com.ayub.core.datasource.network.request_response.ProductVariantItem
import com.ayub.core.datasource.network.request_response.TransactionData

fun ProductRequest.toQueryMap(): Map<String, String> {
    val map = HashMap<String, String>()
    search?.let { map.put("search", it) }
    brand?.let { map.put("brand", it) }
    lowest?.let { map.put("lowest", it) }
    highest?.let { map.put("highest", it) }
    sort?.let { map.put("sort", it) }
    limit?.let { map.put("limit", it.toString()) }
    page?.let { map.put("page", it.toString()) }
    return map
}

fun Items.toProduct(): Items =
    Items(
        image ?: "",
        sale ?: 0,
        productId ?: "",
        store ?: "",
        productRating ?: 0F,
        brand ?: "",
        productName ?: "",
        productPrice ?: 0
    )

fun DetailProductData.toWishlist(variant: ProductVariantItem): WishlistProduct =
    WishlistProduct(
        productId!!,
        productName,
        productPrice,
        image?.get(0),
        brand,
        description,
        store,
        sale,
        stock,
        totalRating,
        totalReview,
        totalSatisfaction,
        productRating,
        variant.variantName!!,
        variant.variantPrice!!
    )

fun DetailProductData.toCart(variant: ProductVariantItem): Cart =
    Cart(
        productId!!,
        productName!!,
        productPrice!!,
        image?.get(0)!!,
        brand!!,
        description!!,
        store!!,
        sale!!,
        stock!!,
        totalRating!!,
        totalReview!!,
        totalSatisfaction!!,
        productRating!!,
        variant.variantName!!,
        variant.variantPrice!!
    )

fun WishlistProduct.toCart(): Cart =
    Cart(
        productId,
        productName!!,
        productPrice!!,
        image!!,
        brand!!,
        description!!,
        store!!,
        sale!!,
        stock!!,
        totalRating!!,
        totalReview!!,
        totalSatisfaction!!,
        productRating!!,
        variantName,
        variantPrice!!
    )

fun Cart.toCheckout(): Checkout =
    Checkout(
        productId,
        variantName,
        quantity
    )

fun TransactionData.toFulfillmentData(): FulfillmentData =
    FulfillmentData(
        date,
        total,
        invoiceId,
        payment,
        time,
        status
    )
