package com.ayub.core.helper

import com.ayub.core.datasource.local.SharedPref
import com.ayub.core.datasource.network.ApiServices
import com.ayub.core.datasource.network.request_response.RefreshTokenRequest
import com.ayub.core.datasource.network.request_response.RefreshTokenResponse
import com.ayub.core.helper.Constants.BASE_URL
import kotlinx.coroutines.runBlocking
import okhttp3.Authenticator
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject

class Authenticator @Inject constructor(
    private val sharedPref: SharedPref,
    private val interceptor: Interceptor
) : Authenticator {

    override fun authenticate(route: Route?, response: Response): Request? {
        val refreshToken = sharedPref.getRefreshToken().toString()
        synchronized(this) {
            return runBlocking {
                try {
                    val newToken = getToken(refreshToken)
                    run {
                        sharedPref.saveAccessToken(newToken.data?.accessToken.toString())
                        sharedPref.saveRefreshToken(newToken.data?.refreshToken.toString())
                        response.request
                            .newBuilder()
                            .header("Authorization", "Bearer ${newToken.data?.accessToken}")
                            .build()
                    }
                } catch (t: Throwable) {
                    null
                }
            }
        }
    }

    private suspend fun getToken(token: String): RefreshTokenResponse {
        val loggingInterceptor =
            HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

        val httpClient = OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .addInterceptor(interceptor)
            .build()

        val client = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .client(httpClient)
            .build()

        val services = client.create(ApiServices::class.java)

        val tokenReq = RefreshTokenRequest(token)

        return services.refresh(refreshTokenRequest = tokenReq)
    }
}
