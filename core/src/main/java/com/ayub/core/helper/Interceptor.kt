package com.ayub.core.helper

import com.ayub.core.datasource.local.SharedPref
import com.ayub.core.helper.Constants.API_KEY
import kotlinx.coroutines.runBlocking
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class Interceptor @Inject constructor(private val sharedPref: SharedPref) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val requestUrl = originalRequest.url.toString()
        val isPreloginRequest = Constants.PRELOGIN_ENDPOINT.contains(requestUrl)
        val request = chain.request().newBuilder().apply {
            if (isPreloginRequest) {
                addHeader("API_KEY", API_KEY)
            } else {
                val accessToken = runBlocking { sharedPref.getAccessToken() }
                addHeader("Authorization", "Bearer $accessToken")
            }
        }.build()
        return chain.proceed(request)
    }
}
