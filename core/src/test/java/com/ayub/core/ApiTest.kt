package com.ayub.core

import com.ayub.core.datasource.network.ApiServices
import com.ayub.core.datasource.network.request_response.Checkout
import com.ayub.core.datasource.network.request_response.DetailProductData
import com.ayub.core.datasource.network.request_response.DetailProductResponse
import com.ayub.core.datasource.network.request_response.FulfillmentData
import com.ayub.core.datasource.network.request_response.FulfillmentRequest
import com.ayub.core.datasource.network.request_response.FulfillmentResponse
import com.ayub.core.datasource.network.request_response.Items
import com.ayub.core.datasource.network.request_response.ItemsItem
import com.ayub.core.datasource.network.request_response.LoginData
import com.ayub.core.datasource.network.request_response.LoginRequest
import com.ayub.core.datasource.network.request_response.LoginResponse
import com.ayub.core.datasource.network.request_response.PaymentResponse
import com.ayub.core.datasource.network.request_response.PaymentTypeItem
import com.ayub.core.datasource.network.request_response.ProductData
import com.ayub.core.datasource.network.request_response.ProductResponse
import com.ayub.core.datasource.network.request_response.ProductReviewResponse
import com.ayub.core.datasource.network.request_response.ProductVariantItem
import com.ayub.core.datasource.network.request_response.ProfileData
import com.ayub.core.datasource.network.request_response.ProfileResponse
import com.ayub.core.datasource.network.request_response.RatingRequest
import com.ayub.core.datasource.network.request_response.RatingResponse
import com.ayub.core.datasource.network.request_response.RefreshTokenData
import com.ayub.core.datasource.network.request_response.RefreshTokenRequest
import com.ayub.core.datasource.network.request_response.RefreshTokenResponse
import com.ayub.core.datasource.network.request_response.RegisterData
import com.ayub.core.datasource.network.request_response.RegisterRequest
import com.ayub.core.datasource.network.request_response.RegisterResponse
import com.ayub.core.datasource.network.request_response.ReviewData
import com.ayub.core.datasource.network.request_response.SearchProductResponse
import com.ayub.core.datasource.network.request_response.TransactionData
import com.ayub.core.datasource.network.request_response.TransactionResponse
import com.ayub.core.utils.ConvertJSON
import junit.framework.TestCase
import kotlinx.coroutines.test.runTest
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@RunWith(RobolectricTestRunner::class)
@Config(manifest = Config.NONE)
class ApiTest {
    private lateinit var mockWebServer: MockWebServer
    private lateinit var apiServices: ApiServices

    @Before
    fun setup() {
        mockWebServer = MockWebServer()
        mockWebServer.start()
        apiServices = Retrofit.Builder().baseUrl(mockWebServer.url("/"))
            .client(OkHttpClient.Builder().build())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiServices::class.java)
    }

    @After
    fun close() {
        mockWebServer.shutdown()
    }

    @Test
    fun login() = runTest {
        val mockResponse = ConvertJSON.createMockResponse("login.json")
        mockWebServer.enqueue(mockResponse)

        val loginRequest = LoginRequest("ayub@gmail.com", "12345678", "1q2w3e")
        val actual = apiServices.login(loginRequest)

        val expected = LoginResponse(
            code = 200,
            message = "OK",
            data = LoginData(
                userName = "ayub",
                userImage = "image_ayub",
                accessToken = "qwerty",
                refreshToken = "asdfgh",
                expiresAt = 600
            )
        )
        TestCase.assertEquals(expected, actual)
    }

    @Test
    fun register() = runTest {
        val mockResponse = ConvertJSON.createMockResponse("register.json")
        mockWebServer.enqueue(mockResponse)

        val registerRequest = RegisterRequest("ayub@gmail.com", "12345678", "1q2w3e")
        val actual = apiServices.register(registerRequest)

        val expected = RegisterResponse(
            code = 200,
            message = "OK",
            data = RegisterData(
                accessToken = "qwerty",
                refreshToken = "asdfgh",
                expiresAt = 600
            )
        )
        TestCase.assertEquals(expected, actual)
    }

    @Test
    fun refresh() = runTest {
        val mockResponse = ConvertJSON.createMockResponse("refresh.json")
        mockWebServer.enqueue(mockResponse)

        val refreshRequest = RefreshTokenRequest("asdfgh")
        val actual = apiServices.refresh(refreshRequest)

        val expected = RefreshTokenResponse(
            code = 200,
            message = "OK",
            data = RefreshTokenData(
                accessToken = "ytrewq",
                refreshToken = "asdfgh",
                expiresAt = 600
            )
        )
        TestCase.assertEquals(expected, actual)
    }

    @Test
    fun profile() = runTest {
        val mockResponse = ConvertJSON.createMockResponse("profile.json")
        mockWebServer.enqueue(mockResponse)

        val actual = apiServices.profile(
            MultipartBody.Part.createFormData("userName", "Ayub"),
            MultipartBody.Part.createFormData("userImage", "imageAyub.jpg")
        )

        val expected = ProfileResponse(
            code = 200,
            message = "OK",
            data = ProfileData("imageAyub.jpg", "Ayub")
        )
        TestCase.assertEquals(expected, actual)
    }

    @Test
    fun products() = runTest {
        val mockResponse = ConvertJSON.createMockResponse("products.json")
        mockWebServer.enqueue(mockResponse)

        val actual = apiServices.products(mapOf("" to ""))

        val expected = ProductResponse(
            code = 200,
            message = "OK",
            data = ProductData(
                1,
                10,
                10,
                3,
                listOf(
                    Items(
                        "image1",
                        2,
                        "601bb59a-4170-4b0a-bd96-f34538922c7c",
                        "LenovoStore",
                        4.0,
                        "Lenovo",
                        "Lenovo Legion 3",
                        10000000
                    ),
                    Items(
                        "image1",
                        4,
                        "3134a179-dff6-464f-b76e-d7507b06887b",
                        "LenovoStore",
                        4.0,
                        "Lenovo",
                        "Lenovo Legion 5",
                        15000000
                    )
                )
            )
        )
        TestCase.assertEquals(expected, actual)
    }

    @Test
    fun searchProduct() = runTest {
        val mockResponse = ConvertJSON.createMockResponse("search.json")
        mockWebServer.enqueue(mockResponse)

        val actual = apiServices.searchProducts("")

        val expected = SearchProductResponse(
            code = 200,
            message = "OK",
            data = listOf(
                "Lenovo Legion 3",
                "Lenovo Legion 5",
                "Lenovo Legion 7",
                "Lenovo Ideapad 3",
                "Lenovo Ideapad 5",
                "Lenovo Ideapad 7"
            )
        )
        TestCase.assertEquals(expected, actual)
    }

    @Test
    fun detailProduct() = runTest {
        val mockResponse = ConvertJSON.createMockResponse("detail.json")
        mockWebServer.enqueue(mockResponse)

        val actual = apiServices.detailProduct("17b4714d-527a-4be2-84e2-e4c37c2b3292")

        val expected = DetailProductResponse(
            code = 200,
            message = "OK",
            data = DetailProductData(
                productId = "17b4714d-527a-4be2-84e2-e4c37c2b3292",
                productName = "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray",
                productPrice = 24499000,
                image = listOf(
                    "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/0a49c399-cf6b-47f5-91c9-8cbd0b86462d.jpg",
                    "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/0cc3d06c-b09d-4294-8c3f-1c37e60631a6.jpg",
                    "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/33a06657-9f88-4108-8676-7adafaa94921.jpg"
                ),
                brand = "Asus",
                description = "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray [AMD Ryzen™ 7 6800H / NVIDIA® GeForce RTX™ 3060 / 8G*2 / 512GB / 17.3inch / WIN11 / OHS]\n\nCPU : AMD Ryzen™ 7 6800H Mobile Processor (8-core/16-thread, 20MB cache, up to 4.7 GHz max boost)\nGPU : NVIDIA® GeForce RTX™ 3060 Laptop GPU\nGraphics Memory : 6GB GDDR6\nDiscrete/Optimus : MUX Switch + Optimus\nTGP ROG Boost : 1752MHz* at 140W (1702MHz Boost Clock+50MHz OC, 115W+25W Dynamic Boost)\nPanel : 17.3-inch FHD (1920 x 1080) 16:9 360Hz IPS-level 300nits sRGB % 100.00%",
                store = "AsusStore",
                sale = 12,
                stock = 2,
                totalRating = 7,
                totalReview = 5,
                totalSatisfaction = 100,
                productRating = (5.0).toFloat(),
                productVariant = listOf(
                    ProductVariantItem(
                        variantName = "RAM 16GB",
                        variantPrice = 0
                    ),
                    ProductVariantItem(
                        variantName = "RAM 32GB",
                        variantPrice = 1000000
                    )
                )
            )
        )
        TestCase.assertEquals(expected, actual)
    }

    @Test
    fun reviewProduct() = runTest {
        val mockResponse = ConvertJSON.createMockResponse("review.json")
        mockWebServer.enqueue(mockResponse)

        val actual = apiServices.reviewProduct("17b4714d-527a-4be2-84e2-e4c37c2b3292")

        val expected = ProductReviewResponse(
            code = 200,
            message = "OK",
            data = listOf(
                ReviewData(
                    userName = "John",
                    userImage = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTQM4VpzpVw8mR2j9_gDajEthwY3KCOWJ1tOhcv47-H9o1a-s9GRPxdb_6G9YZdGfv0HIg&usqp=CAU",
                    userRating = 4,
                    userReview = "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
                ),
                ReviewData(
                    userName = "Doe",
                    userImage = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTR3Z6PN8QNVhH0e7rEINu_XJS0qHIFpDT3nwF5WSkcYmr3znhY7LOTkc8puJ68Bts-TMc&usqp=CAU",
                    userRating = 5,
                    userReview = "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
                )
            )
        )
        TestCase.assertEquals(expected, actual)
    }

    @Test
    fun payment() = runTest {
        val mockResponse = ConvertJSON.createMockResponse("payment.json")
        mockWebServer.enqueue(mockResponse)

        val actual = apiServices.payment()

        val expected = PaymentResponse(
            code = 200,
            message = "OK",
            data = listOf(
                PaymentTypeItem(
                    title = "Transfer Virtual Account",
                    item = listOf(
                        PaymentTypeItem.PaymentItem(
                            label = "BNI Virtual Account",
                            image = "bni.com",
                            status = true
                        ),
                        PaymentTypeItem.PaymentItem(
                            label = "BRI Virtual Account",
                            image = "bri.com",
                            status = true
                        ),
                    )
                ),
                PaymentTypeItem(
                    title = "Transfer Bank",
                    item = listOf(
                        PaymentTypeItem.PaymentItem(
                            label = "Bank BCA",
                            image = "bca.com",
                            status = true
                        ),
                        PaymentTypeItem.PaymentItem(
                            label = "Bank BNI",
                            image = "bni.com",
                            status = true
                        ),
                    )
                ),
                PaymentTypeItem(
                    title = "Pembayaran Instan",
                    item = listOf(
                        PaymentTypeItem.PaymentItem(
                            label = "GoPay",
                            image = "gopay.com",
                            status = true
                        ),
                        PaymentTypeItem.PaymentItem(
                            label = "OVO",
                            image = "ovo.com",
                            status = true
                        ),
                    )
                ),
            )
        )
        TestCase.assertEquals(expected, actual)
    }

    @Test
    fun fulfillment() = runTest {
        val mockResponse = ConvertJSON.createMockResponse("fulfillment.json")
        mockWebServer.enqueue(mockResponse)

        val actual = apiServices.fulfillment(
            FulfillmentRequest(
                "Bank BCA",
                listOf(
                    Checkout(
                        "5152a3dc-5c2f-4535-a06b-a9768a6c2add",
                        "RAM 16GB",
                        2
                    )
                )
            )
        )

        val expected = FulfillmentResponse(
            code = 200,
            message = "OK",
            data = FulfillmentData(
                invoiceId = "ba47402c-d263-49d3-a1f8-759ae59fa4a1",
                status = true,
                date = "09 Jun 2023",
                time = "08:53",
                payment = "Bank BCA",
                total = 48998000
            )
        )
        TestCase.assertEquals(expected, actual)
    }

    @Test
    fun rating() = runTest {
        val mockResponse = ConvertJSON.createMockResponse("rating.json")
        mockWebServer.enqueue(mockResponse)

        val actual = apiServices.rating(
            RatingRequest(
                "6d2ed50f-0c87-4a57-b873-8a4addd68949",
                0,
                ""
            )
        )

        val expected = RatingResponse(
            200,
            "Fulfillment rating and review success"
        )
        TestCase.assertEquals(expected, actual)
    }

    @Test
    fun transaction() = runTest {
        val mockResponse = ConvertJSON.createMockResponse("transaction.json")
        mockWebServer.enqueue(mockResponse)

        val actual = apiServices.transaction()

        val expected = TransactionResponse(
            code = 200,
            message = "OK",
            data = listOf(
                TransactionData(
                    "09 Jun 2023",
                    "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/0a49c399-cf6b-47f5-91c9-8cbd0b86462d.jpg",
                    48998000,
                    "LGTM",
                    4,
                    "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray",
                    "8cad85b1-a28f-42d8-9479-72ce4b7f3c7d",
                    "Bank BCA",
                    "09:05",
                    listOf(
                        ItemsItem(
                            2,
                            "bee98108-660c-4ac0-97d3-63cdc1492f53",
                            "RAM 16GB"
                        )
                    ),
                    true,
                )
            )
        )

        TestCase.assertEquals(expected, actual)
    }
}
