package com.ayub.core

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ayub.core.datasource.local.SharedPref
import com.ayub.core.datasource.local.room.AppDatabase
import com.ayub.core.datasource.local.room.dao.CartDao
import com.ayub.core.datasource.local.room.dao.NotificationDao
import com.ayub.core.datasource.local.room.dao.WishlistDao
import com.ayub.core.datasource.local.room.entity.Cart
import com.ayub.core.datasource.local.room.entity.Notification
import com.ayub.core.datasource.local.room.entity.WishlistProduct
import com.ayub.core.datasource.network.ApiServices
import com.ayub.core.datasource.network.request_response.BaseResponse
import com.ayub.core.datasource.network.request_response.Checkout
import com.ayub.core.datasource.network.request_response.DetailProductData
import com.ayub.core.datasource.network.request_response.DetailProductResponse
import com.ayub.core.datasource.network.request_response.FulfillmentData
import com.ayub.core.datasource.network.request_response.FulfillmentRequest
import com.ayub.core.datasource.network.request_response.FulfillmentResponse
import com.ayub.core.datasource.network.request_response.ItemsItem
import com.ayub.core.datasource.network.request_response.LoginData
import com.ayub.core.datasource.network.request_response.LoginRequest
import com.ayub.core.datasource.network.request_response.LoginResponse
import com.ayub.core.datasource.network.request_response.PaymentResponse
import com.ayub.core.datasource.network.request_response.PaymentTypeItem
import com.ayub.core.datasource.network.request_response.ProductReviewResponse
import com.ayub.core.datasource.network.request_response.ProductVariantItem
import com.ayub.core.datasource.network.request_response.ProfileData
import com.ayub.core.datasource.network.request_response.ProfileResponse
import com.ayub.core.datasource.network.request_response.RatingRequest
import com.ayub.core.datasource.network.request_response.RatingResponse
import com.ayub.core.datasource.network.request_response.RegisterData
import com.ayub.core.datasource.network.request_response.RegisterRequest
import com.ayub.core.datasource.network.request_response.RegisterResponse
import com.ayub.core.datasource.network.request_response.ReviewData
import com.ayub.core.datasource.network.request_response.SearchProductResponse
import com.ayub.core.datasource.network.request_response.TransactionData
import com.ayub.core.datasource.network.request_response.TransactionResponse
import com.ayub.core.repository.IRepository
import com.ayub.core.repository.Repository
import com.ayub.core.utils.LiveDataTestUtil
import com.ayub.core.utils.MainDispatcherRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.test.runTest
import okhttp3.MultipartBody
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import retrofit2.HttpException

@RunWith(JUnit4::class)
class RepositoryTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private lateinit var repo: IRepository
    private lateinit var sharedPref: SharedPref
    private lateinit var appDatabase: AppDatabase
    private lateinit var apiServices: ApiServices
    private lateinit var wishlistDao: WishlistDao
    private lateinit var cartDao: CartDao
    private lateinit var notificationDao: NotificationDao
    private lateinit var httpException: HttpException
    private lateinit var exception: Exception

    @Before
    fun setup() {
        sharedPref = mock()
        appDatabase = mock()
        apiServices = mock()
        wishlistDao = mock()
        cartDao = mock()
        notificationDao = mock()
        httpException = mock()
        exception = mock()
        repo = Repository(sharedPref, apiServices, appDatabase)
        whenever(appDatabase.wishlistDao()).thenReturn(wishlistDao)
        whenever(appDatabase.cartDao()).thenReturn(cartDao)
        whenever(appDatabase.notificationDao()).thenReturn(notificationDao)
    }

    @Test
    fun login() = runTest {
        val expected = LoginData(
            userName = "ayub",
            userImage = "image_ayub",
            accessToken = "qwerty",
            refreshToken = "asdfgh",
            expiresAt = 600
        )

        whenever(
            apiServices.login(
                LoginRequest(
                    "ayub@gmail.com",
                    "12345678",
                    "1q2w3e"
                )
            )
        ).thenReturn(
            LoginResponse(
                200,
                message = "OK",
                data = expected
            )
        )

        repo.login(
            "ayub@gmail.com",
            "12345678",
            "1q2w3e"
        ).observeForever {
            if (it is BaseResponse.Success) {
                assertEquals(expected, it.data)
            }
        }
    }

    @Test
    fun register() = runTest {
        val expected = RegisterData(
            accessToken = "qwerty",
            refreshToken = "asdfgh",
            expiresAt = 600
        )

        whenever(
            apiServices.register(
                RegisterRequest(
                    "ayub@gmail.com",
                    "12345678",
                    "1q2w3e"
                )
            )
        ).thenReturn(
            RegisterResponse(
                200,
                message = "OK",
                data = expected
            )
        )

        repo.register(
            "ayub@gmail.com",
            "12345678",
            "1q2w3e"
        ).observeForever {
            if (it is BaseResponse.Success) {
                assertEquals(expected, it.data)
            }
        }
    }

    @Test
    fun saveProfile() = runTest {
        val expected = ProfileData(
            userName = "ayub",
            userImage = "image_ayub",
        )

        val profileMethod = apiServices.profile(
            MultipartBody.Part.createFormData("userName", "Ayub"),
            MultipartBody.Part.createFormData("userImage", "imageAyub.jpg")
        )

        whenever(profileMethod).thenReturn(
            ProfileResponse(
                200,
                message = "OK",
                data = expected
            )
        )

        repo.saveProfile(
            MultipartBody.Part.createFormData("userName", "Ayub"),
            MultipartBody.Part.createFormData("userImage", "imageAyub.jpg")
        ).observeForever {
            if (it is BaseResponse.Success) {
                assertEquals(expected, it.data)
            }
        }
    }

    @Test
    fun logout() = runTest {
        clearData()
        appDatabase.clearAllTables()
        verify(appDatabase).clearAllTables()
    }

    @Test
    fun searchProduct() = runTest {
        val expected = SearchProductResponse(
            200,
            message = "OK",
            data = listOf(
                "Lenovo Legion 3",
                "Lenovo Legion 5",
                "Lenovo Legion 7",
                "Lenovo Ideapad 3",
                "Lenovo Ideapad 5",
                "Lenovo Ideapad 7"
            )
        )

        whenever(apiServices.searchProducts("")).thenReturn(expected)

        repo.searchProducts("").observeForever {
            if (it is BaseResponse.Success) {
                assertEquals(expected, it.data)
            }
        }
    }

    @Test
    fun getDetailProduct() = runTest {
        val expected = dummyDetailProduct

        whenever(apiServices.detailProduct("17b4714d-527a-4be2-84e2-e4c37c2b3292")).thenReturn(
            DetailProductResponse(
                code = 200,
                message = "OK",
                data = expected
            )
        )

        repo.getDetailProduct("").observeForever {
            if (it is BaseResponse.Success) {
                assertEquals(expected, it.data)
            }
        }
    }

    @Test
    fun getProductReview() = runTest {
        val expected = dummyReviewProduct

        whenever(apiServices.reviewProduct("17b4714d-527a-4be2-84e2-e4c37c2b3292")).thenReturn(
            ProductReviewResponse(
                200,
                message = "OK",
                data = expected
            )
        )

        repo.getProductReview("17b4714d-527a-4be2-84e2-e4c37c2b3292").observeForever {
            if (it is BaseResponse.Success) {
                assertEquals(expected, it.data)
            }
        }
    }

    @Test
    fun saveToWishlist() = runTest {
        val wishlistProduct = dummyDataWishlist

        repo.saveToWishlist(wishlistProduct)
        verify(wishlistDao).insertProduct(wishlistProduct)
    }

    @Test
    fun getWishlistProducts() = runTest {
        val testData = listOf(dummyDataWishlist)
        val liveDataTestUtil = LiveDataTestUtil<List<WishlistProduct>>()
        val mockedLiveData: LiveData<List<WishlistProduct>> =
            MutableLiveData<List<WishlistProduct>>().apply {
                value = testData
            }

        whenever(wishlistDao.getAllProducts()).thenReturn(mockedLiveData)

        val resultLiveData = repo.getWishlistProducts()

        val result = liveDataTestUtil.getValue(resultLiveData)
        assertEquals(testData.size, result?.size)
    }

    @Test
    fun checkProductInWishlist() = runTest {
        val productId = "17b4714d-527a-4be2-84e2-e4c37c2b3292"
        whenever(wishlistDao.checkProductIsExist(productId)).thenReturn(true)
        val result = repo.checkProductInWishlist(productId)
        assertEquals(true, result)
    }

    @Test
    fun deleteFromWishlist() = runTest {
        val wishlistProduct = dummyDataWishlist

        repo.deleteFromWishlist(wishlistProduct)
        verify(wishlistDao).deleteProduct(wishlistProduct)
    }

    @Test
    fun addToCart() = runTest {
        val testData = dummyDataCart

        repo.addToCart(testData)
        verify(cartDao).insertProductToCart(testData)
    }

    @Test
    fun getCartProducts() = runTest {
        val testData = listOf(dummyDataCart)
        val liveDataTestUtil = LiveDataTestUtil<List<Cart>>()
        val mockedLiveData: LiveData<List<Cart>> = MutableLiveData<List<Cart>>().apply {
            value = testData
        }

        whenever(cartDao.getAllProducts()).thenReturn(mockedLiveData)

        val resultLiveData = repo.getCartProducts()

        val result = liveDataTestUtil.getValue(resultLiveData)
        assertEquals(testData.size, result?.size)
    }

    @Test
    fun getCartProductById() = runTest {
        val productId = "17b4714d-527a-4be2-84e2-e4c37c2b3292"
        whenever(cartDao.getProductById(productId)).thenReturn(dummyDataCart)
        val result = repo.getCartProductById(productId)
        assertEquals(dummyDataCart, result)
    }

    @Test
    fun checkProductInCart() = runTest {
        val productId = "17b4714d-527a-4be2-84e2-e4c37c2b3292"
        whenever(cartDao.checkExistData(productId)).thenReturn(true)
        val result = repo.checkProductInCart(productId)
        assertEquals(true, result)
    }

    @Test
    fun deleteProductInCart() = runTest {
        val testData = dummyDataCart
        repo.deleteProductInCart(testData)
        verify(cartDao).deleteProductFromCart(testData)
    }

    @Test
    fun deleteAllProductInCart() = runTest {
        val testData = listOf(dummyDataCart.productId)
        repo.deleteAllProductInCart(testData)
        verify(cartDao).deleteAllProduct(testData)
    }

    @Test
    fun updateCartItemQuantity() = runTest {
        val testData = dummyDataCart
        repo.updateCartItemQuantity(testData.productId, 2)
        verify(cartDao).updateCartItemQuantity(testData.productId, 2)
    }

    @Test
    fun updateCartItemSelected() = runTest {
        val testData = listOf(dummyDataCart.productId)
        repo.updateCartItemSelected(testData, true)
        verify(cartDao).updateCartItemCheckbox(testData, true)
    }

    @Test
    fun getPayment() = runTest {
        val expected = dummyPaymentMethod.data
        whenever(apiServices.payment()).thenReturn(dummyPaymentMethod)
        repo.getPayment().observeForever {
            if (it is BaseResponse.Success) {
                assertEquals(expected, it.data)
            }
        }
    }

    @Test
    fun fulfillment() = runTest {
        val expected = dummyFulfilmentResponse.data
        whenever(apiServices.fulfillment(dummyFulfillmentRequest)).thenReturn(
            dummyFulfilmentResponse
        )
        repo.fulfillment(dummyFulfillmentRequest.payment!!, listOf(dummyDataCart)).observeForever {
            if (it is BaseResponse.Success) {
                assertEquals(expected, it.data)
            }
        }
    }

    @Test
    fun rating() = runTest {
        val expected = RatingResponse(
            200,
            "Fulfillment rating and review success"
        )

        whenever(apiServices.rating(dummyRatingRequest)).thenReturn(expected)
        repo.rating(
            dummyRatingRequest.invoiceId,
            dummyRatingRequest.rating,
            dummyRatingRequest.review
        ).observeForever {
            if (it is BaseResponse.Success) {
                assertEquals(true, it.data)
            }
        }
    }

    @Test
    fun transaction() = runTest {
        val expected = dummyTransactionResponse
        whenever(apiServices.transaction()).thenReturn(expected)
        repo.transaction().observeForever {
            if (it is BaseResponse.Success) {
                assertEquals(expected.data, it.data)
            }
        }
    }

    @Test
    fun getAllNotification() = runTest {
        val testData = listOf(dummyNotification)
        val liveDataTestUtil = LiveDataTestUtil<List<Notification>>()
        val mockedLiveData: LiveData<List<Notification>> =
            MutableLiveData<List<Notification>>().apply {
                value = testData
            }

        whenever(notificationDao.getNotification()).thenReturn(mockedLiveData)

        val resultLiveData = repo.getAllNotification()

        val result = liveDataTestUtil.getValue(resultLiveData!!)
        assertEquals(testData.size, result?.size)
    }

    @Test
    fun addToNotification() = runTest {
        val data = dummyNotification
        repo.addToNotification(data)
        verify(notificationDao).addToNotification(data)
    }

    @Test
    fun updateNotification() = runTest {
        val data = dummyNotification
        repo.updateNotification(data.copy(isRead = true))
        verify(notificationDao).updateNotification(data.copy(isRead = true))
    }

    @Test
    fun isFirstLaunch() = runTest {
        whenever(sharedPref.isFirstLaunch()).thenReturn(true)
        val result = repo.isFirstLaunch()
        assertEquals(true, result)
    }

    @Test
    fun setIsFirstLaunchToFalse() = runTest {
        repo.setIsFirstLaunchToFalse()
        verify(sharedPref).setIsFirstLaunchToFalse()
    }

    @Test
    fun isLangId() = runTest {
        whenever(sharedPref.isLangID()).thenReturn(true)
        val result = repo.isLangId()
        assertEquals(true, result)
    }

    @Test
    fun setLangId() = runTest {
        val langId = true
        repo.setLangId(langId)
        verify(sharedPref).setLanguageToId(langId)
    }

    @Test
    fun isDarkMode() = runTest {
        whenever(sharedPref.isDarkModeEnabled()).thenReturn(true)
        val result = repo.isDarkModeEnabled()
        assertEquals(true, result)
    }

    @Test
    fun setDarkMode() = runTest {
        val darkMode = true
        repo.setDarkMode(darkMode)
        verify(sharedPref).setDarkMode(darkMode)
    }

    @Test
    fun saveAccessToken() = runTest {
        val accessToken = "qwerty"
        repo.saveAccessToken(accessToken)
        verify(sharedPref).saveAccessToken(accessToken)
    }

    @Test
    fun saveRefreshToken() = runTest {
        val refreshToken = "asdfgh"
        repo.saveRefreshToken(refreshToken)
        verify(sharedPref).saveRefreshToken(refreshToken)
    }

    @Test
    fun saveUsername() = runTest {
        val username = "Ayub"
        repo.saveUsername(username)
        verify(sharedPref).saveUsername(username)
    }

    @Test
    fun getAccessToken() = runTest {
        val accessToken = "qwerty"
        whenever(sharedPref.getAccessToken()).thenReturn(accessToken)
        val result = repo.getAccessToken()
        assertEquals(accessToken, result)
    }

    @Test
    fun getRefreshToken() = runTest {
        val refreshToken = "asdfgh"
        whenever(sharedPref.getRefreshToken()).thenReturn(refreshToken)
        val result = repo.getRefreshToken()
        assertEquals(refreshToken, result)
    }

    @Test
    fun getUsername() = runTest {
        val username = "Ayub"
        whenever(sharedPref.getUsername()).thenReturn(username)
        val result = repo.getUsername()
        assertEquals(username, result)
    }

    @Test
    fun clearData() = runTest {
        repo.clearData()
        verify(sharedPref).clearData()
    }

    companion object {
        val dummyDetailProduct = DetailProductData(
            productId = "17b4714d-527a-4be2-84e2-e4c37c2b3292",
            productName = "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray",
            productPrice = 24499000,
            image = listOf(
                "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/0a49c399-cf6b-47f5-91c9-8cbd0b86462d.jpg",
                "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/0cc3d06c-b09d-4294-8c3f-1c37e60631a6.jpg",
                "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/33a06657-9f88-4108-8676-7adafaa94921.jpg"
            ),
            brand = "Asus",
            description = "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray [AMD Ryzen™ 7 6800H / NVIDIA® GeForce RTX™ 3060 / 8G*2 / 512GB / 17.3inch / WIN11 / OHS]\n\nCPU : AMD Ryzen™ 7 6800H Mobile Processor (8-core/16-thread, 20MB cache, up to 4.7 GHz max boost)\nGPU : NVIDIA® GeForce RTX™ 3060 Laptop GPU\nGraphics Memory : 6GB GDDR6\nDiscrete/Optimus : MUX Switch + Optimus\nTGP ROG Boost : 1752MHz* at 140W (1702MHz Boost Clock+50MHz OC, 115W+25W Dynamic Boost)\nPanel : 17.3-inch FHD (1920 x 1080) 16:9 360Hz IPS-level 300nits sRGB % 100.00%",
            store = "AsusStore",
            sale = 12,
            stock = 2,
            totalRating = 7,
            totalReview = 5,
            totalSatisfaction = 100,
            productRating = (5.0).toFloat(),
            productVariant = listOf(
                ProductVariantItem(
                    variantName = "RAM 16GB",
                    variantPrice = 0
                ),
                ProductVariantItem(
                    variantName = "RAM 32GB",
                    variantPrice = 1000000
                )
            )
        )

        val dummyReviewProduct = listOf(
            ReviewData(
                userName = "John",
                userImage = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTQM4VpzpVw8mR2j9_gDajEthwY3KCOWJ1tOhcv47-H9o1a-s9GRPxdb_6G9YZdGfv0HIg&usqp=CAU",
                userRating = 4,
                userReview = "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
            ),
            ReviewData(
                userName = "Doe",
                userImage = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTR3Z6PN8QNVhH0e7rEINu_XJS0qHIFpDT3nwF5WSkcYmr3znhY7LOTkc8puJ68Bts-TMc&usqp=CAU",
                userRating = 5,
                userReview = "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
            )
        )

        val dummyDataWishlist = WishlistProduct(
            productId = "17b4714d-527a-4be2-84e2-e4c37c2b3292",
            productName = "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray",
            productPrice = 24499000,
            image = "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/0a49c399-cf6b-47f5-91c9-8cbd0b86462d.jpg",
            brand = "Asus",
            description = "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray [AMD Ryzen™ 7 6800H / NVIDIA® GeForce RTX™ 3060 / 8G*2 / 512GB / 17.3inch / WIN11 / OHS]\n\nCPU : AMD Ryzen™ 7 6800H Mobile Processor (8-core/16-thread, 20MB cache, up to 4.7 GHz max boost)\nGPU : NVIDIA® GeForce RTX™ 3060 Laptop GPU\nGraphics Memory : 6GB GDDR6\nDiscrete/Optimus : MUX Switch + Optimus\nTGP ROG Boost : 1752MHz* at 140W (1702MHz Boost Clock+50MHz OC, 115W+25W Dynamic Boost)\nPanel : 17.3-inch FHD (1920 x 1080) 16:9 360Hz IPS-level 300nits sRGB % 100.00%",
            store = "AsusStore",
            sale = 12,
            stock = 2,
            totalRating = 7,
            totalReview = 5,
            totalSatisfaction = 100,
            productRating = (5.0).toFloat(),
            variantName = "RAM 32GB",
            variantPrice = 1000000
        )

        val dummyDataCart = Cart(
            productId = "17b4714d-527a-4be2-84e2-e4c37c2b3292",
            productName = "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray",
            productPrice = 24499000,
            image = "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/0a49c399-cf6b-47f5-91c9-8cbd0b86462d.jpg",
            brand = "Asus",
            description = "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray [AMD Ryzen™ 7 6800H / NVIDIA® GeForce RTX™ 3060 / 8G*2 / 512GB / 17.3inch / WIN11 / OHS]\n\nCPU : AMD Ryzen™ 7 6800H Mobile Processor (8-core/16-thread, 20MB cache, up to 4.7 GHz max boost)\nGPU : NVIDIA® GeForce RTX™ 3060 Laptop GPU\nGraphics Memory : 6GB GDDR6\nDiscrete/Optimus : MUX Switch + Optimus\nTGP ROG Boost : 1752MHz* at 140W (1702MHz Boost Clock+50MHz OC, 115W+25W Dynamic Boost)\nPanel : 17.3-inch FHD (1920 x 1080) 16:9 360Hz IPS-level 300nits sRGB % 100.00%",
            store = "AsusStore",
            sale = 12,
            stock = 2,
            totalRating = 7,
            totalReview = 5,
            totalSatisfaction = 100,
            productRating = (5.0).toFloat(),
            variantName = "RAM 32GB",
            variantPrice = 1000000
        )

        val dummyPaymentMethod = PaymentResponse(
            code = 200,
            message = "OK",
            data = listOf(
                PaymentTypeItem(
                    title = "Transfer Virtual Account",
                    item = listOf(
                        PaymentTypeItem.PaymentItem(
                            label = "BNI Virtual Account",
                            image = "bni.com",
                            status = true
                        ),
                        PaymentTypeItem.PaymentItem(
                            label = "BRI Virtual Account",
                            image = "bri.com",
                            status = true
                        ),
                    )
                ),
                PaymentTypeItem(
                    title = "Transfer Bank",
                    item = listOf(
                        PaymentTypeItem.PaymentItem(
                            label = "Bank BCA",
                            image = "bca.com",
                            status = true
                        ),
                        PaymentTypeItem.PaymentItem(
                            label = "Bank BNI",
                            image = "bni.com",
                            status = true
                        ),
                    )
                ),
                PaymentTypeItem(
                    title = "Pembayaran Instan",
                    item = listOf(
                        PaymentTypeItem.PaymentItem(
                            label = "GoPay",
                            image = "gopay.com",
                            status = true
                        ),
                        PaymentTypeItem.PaymentItem(
                            label = "OVO",
                            image = "ovo.com",
                            status = true
                        ),
                    )
                ),
            )
        )

        val dummyFulfillmentRequest = FulfillmentRequest(
            "Bank BCA",
            listOf(
                Checkout(
                    "5152a3dc-5c2f-4535-a06b-a9768a6c2add",
                    "RAM 16GB",
                    2
                )
            )
        )

        val dummyFulfilmentResponse = FulfillmentResponse(
            code = 200,
            message = "OK",
            data = FulfillmentData(
                invoiceId = "ba47402c-d263-49d3-a1f8-759ae59fa4a1",
                status = true,
                date = "09 Jun 2023",
                time = "08:53",
                payment = "Bank BCA",
                total = 48998000
            )
        )

        val dummyRatingRequest = RatingRequest(
            "6d2ed50f-0c87-4a57-b873-8a4addd68949",
            0,
            ""
        )

        val dummyTransactionResponse = TransactionResponse(
            code = 200,
            message = "OK",
            data = listOf(
                TransactionData(
                    "09 Jun 2023",
                    "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/0a49c399-cf6b-47f5-91c9-8cbd0b86462d.jpg",
                    48998000,
                    "LGTM",
                    4,
                    "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray",
                    "8cad85b1-a28f-42d8-9479-72ce4b7f3c7d",
                    "Bank BCA",
                    "09:05",
                    listOf(
                        ItemsItem(
                            2,
                            "bee98108-660c-4ac0-97d3-63cdc1492f53",
                            "RAM 16GB"
                        )
                    ),
                    true,
                )
            )
        )

        val dummyNotification = Notification(
            "dummyId",
            "dummyType",
            "dummyDate",
            "dummyTime",
            "dummyTitle",
            "Dummy",
            "dummyImage",
            false,
        )
    }
}
