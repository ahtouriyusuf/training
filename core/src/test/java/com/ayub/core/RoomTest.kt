package com.ayub.core

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.ayub.core.datasource.local.room.AppDatabase
import com.ayub.core.datasource.local.room.dao.CartDao
import com.ayub.core.datasource.local.room.dao.NotificationDao
import com.ayub.core.datasource.local.room.dao.WishlistDao
import com.ayub.core.datasource.local.room.entity.Cart
import com.ayub.core.datasource.local.room.entity.Notification
import com.ayub.core.datasource.local.room.entity.WishlistProduct
import junit.framework.TestCase
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(manifest = Config.NONE)
class RoomTest {
    private lateinit var context: Context
    private lateinit var appDatabase: AppDatabase
    private lateinit var wishlistDao: WishlistDao
    private lateinit var cartDao: CartDao
    private lateinit var notificationDao: NotificationDao

    @Before
    fun setup() {
        context = ApplicationProvider.getApplicationContext()
        appDatabase = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java
        ).build()
        wishlistDao = appDatabase.wishlistDao()
        cartDao = appDatabase.cartDao()
        notificationDao = appDatabase.notificationDao()
    }

    @After
    fun close() {
        appDatabase.close()
    }

    // Wishlist Dao Test
    @Test
    fun insertWishlistProduct() = runTest {
        wishlistDao.insertProduct(dataWishlist1)
        wishlistDao.insertProduct(dataWishlist2)
        wishlistDao.getAllProducts().observeForever {
            TestCase.assertEquals(listOf(dataWishlist1, dataWishlist2), it)
        }
    }

    @Test
    fun checkWishlistProductIsExist() = runTest {
        wishlistDao.insertProduct(dataWishlist1)
        val actual = wishlistDao.checkProductIsExist(dataWishlist1.productId)
        TestCase.assertEquals(true, actual)
    }

    @Test
    fun deleteWishlistProduct() = runTest {
        wishlistDao.insertProduct(dataWishlist1)
        wishlistDao.deleteProduct(dataWishlist1)
        wishlistDao.getAllProducts().observeForever {
            TestCase.assertEquals(0, it.size)
        }
    }

    // Cart Dao Test
    @Test
    fun insertToCart() = runTest {
        cartDao.insertProductToCart(dataCart1)
        cartDao.insertProductToCart(dataCart2)
        cartDao.getAllProducts().observeForever {
            TestCase.assertEquals(listOf(dataCart1, dataCart2), it)
        }
    }

    @Test
    fun getCartProductById() = runTest {
        cartDao.insertProductToCart(dataCart1)
        val actual = cartDao.getProductById(dataCart1.productId)
        TestCase.assertEquals(dataCart1, actual)
    }

    @Test
    fun checkExistDataInCart() = runTest {
        cartDao.insertProductToCart(dataCart1)
        val actual = cartDao.checkExistData(dataCart1.productId)
        TestCase.assertEquals(true, actual)
    }

    @Test
    fun updateQuantity() = runTest {
        cartDao.insertProductToCart(dataCart1)
        cartDao.updateCartItemQuantity(dataCart1.productId, 2)
        val cartAfterAdded = cartDao.getProductById(dataCart1.productId)
        TestCase.assertEquals(cartAfterAdded, dataCartAddQuantity)
    }

    @Test
    fun updateProductSelected() = runTest {
        cartDao.insertProductToCart(dataCart2)
        cartDao.insertProductToCart(dataCart1)
        cartDao.updateCartItemCheckbox(listOf(dataCart2.productId, dataCart1.productId), true)
        val cart2AfterSelected = cartDao.getProductById(dataCart2.productId)
        val cart1AfterSelected = cartDao.getProductById(dataCart1.productId)
        TestCase.assertEquals(cart2AfterSelected, dataCartSelected)
        TestCase.assertEquals(cart1AfterSelected, dataCart1Selected)
    }

    @Test
    fun deleteCartProduct() = runTest {
        cartDao.insertProductToCart(dataCart1)
        cartDao.deleteProductFromCart(dataCart1)
        cartDao.getAllProducts().observeForever {
            TestCase.assertEquals(0, it.size)
        }
    }

    @Test
    fun deleteSelectedCartProduct() = runTest {
        cartDao.insertProductToCart(dataCart1)
        cartDao.insertProductToCart(dataCart2)
        cartDao.updateCartItemCheckbox(listOf(dataCart1.productId), true)
        cartDao.getAllProducts().observeForever {
            val selectedId = it.filter { it.selected }.map { it.productId }
            cartDao.deleteAllProduct(selectedId)
            TestCase.assertEquals(listOf(dataCart2), it)
        }
    }

    @Test
    fun deleteAllCartProduct() = runTest {
        cartDao.insertProductToCart(dataCart1)
        cartDao.insertProductToCart(dataCart2)
        cartDao.getAllProducts().observeForever {
            cartDao.deleteAllCart()
            TestCase.assertEquals(0, it.size)
        }
    }

    // Notification Dao Test
    @Test
    fun addNotification() = runTest {
        notificationDao.addToNotification(dataNotification)
        notificationDao.getNotification().observeForever {
            TestCase.assertEquals(listOf(dataNotification), it)
        }
    }

    @Test
    fun setNotificationIsRead() = runTest {
        notificationDao.addToNotification(dataNotification)
        notificationDao.updateNotification(notificationisRead)
        notificationDao.getNotification().observeForever {
            TestCase.assertEquals(listOf(notificationisRead), it)
        }
    }

    companion object {
        val dataWishlist1 = WishlistProduct(
            productId = "dummyId1",
            productName = "dummyName",
            productPrice = 1,
            image = "dummyPicture",
            brand = "dummyBrand",
            description = "Dummy",
            store = "dummyStore",
            sale = 1,
            stock = 1,
            totalRating = 1,
            totalReview = 1,
            totalSatisfaction = 1,
            productRating = 1f,
            variantName = "dummyVariant",
            variantPrice = 1,
            quantity = 1,
            selected = true
        )

        val dataWishlist2 = WishlistProduct(
            productId = "dummyId2",
            productName = "dummyName",
            productPrice = 2,
            image = "dummyPicture",
            brand = "dummyBrand",
            description = "Dummy",
            store = "dummyStore",
            sale = 2,
            stock = 2,
            totalRating = 2,
            totalReview = 2,
            totalSatisfaction = 2,
            productRating = 2f,
            variantName = "dummyVariant",
            variantPrice = 2,
            quantity = 2,
            selected = false
        )

        val dataCart1 = Cart(
            "dummyId1",
            "dummyName",
            1,
            "dummyImage",
            "dummyBrand",
            "dummy",
            "dummyStore",
            1,
            1,
            1,
            1,
            1,
            1f,
            "dummyVariant",
            1,
            1,
            false,
        )

        val dataCartAddQuantity = dataCart1.copy(quantity = 2)

        val dataCart1Selected = dataCart1.copy(selected = true)

        val dataCart2 = Cart(
            "dummyId2",
            "dummyName",
            2,
            "dummyImage",
            "dummyBrand",
            "dummy",
            "dummyStore",
            2,
            2,
            2,
            2,
            2,
            2f,
            "dummyVariant",
            2,
            2,
            false,
        )

        val dataCartSelected = dataCart2.copy(selected = true)

        val dataNotification = Notification(
            "dummyId",
            "dummyType",
            "dummyDate",
            "dummyTime",
            "dummyTitle",
            "Dummy",
            "dummyImage",
            false,
        )

        val notificationisRead = dataNotification.copy(isRead = true)
    }
}
