package com.ayub.core

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import com.ayub.core.datasource.local.SharedPref
import junit.framework.TestCase
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(manifest = Config.NONE)
class SharedPreferencesTest {
    private lateinit var context: Context
    private lateinit var sharedPref: SharedPref

    @Before
    fun setup() {
        context = ApplicationProvider.getApplicationContext()
        sharedPref = SharedPref(context)
    }

    @Test
    fun setToOnboarding() {
        sharedPref.setIsFirstLaunchToFalse()
        val actual = sharedPref.isFirstLaunch()
        TestCase.assertEquals(false, actual)
    }

    @Test
    fun setLang() {
        val langId = true
        sharedPref.setLanguageToId(langId)
        val actual = sharedPref.isLangID()
        TestCase.assertEquals(langId, actual)
    }

    @Test
    fun setDarkMode() {
        val darkMode = true
        sharedPref.setDarkMode(darkMode)
        val actual = sharedPref.isDarkModeEnabled()
        TestCase.assertEquals(darkMode, actual)
    }

    @Test
    fun setAccessToken() {
        val accessToken = "qwertyuio"
        sharedPref.saveAccessToken(accessToken)
        val actual = sharedPref.getAccessToken()
        TestCase.assertEquals(accessToken, actual)
    }

    @Test
    fun setRefreshToken() {
        val refreshToken = "asdfghjkl"
        sharedPref.saveRefreshToken(refreshToken)
        val actual = sharedPref.getRefreshToken()
        TestCase.assertEquals(refreshToken, actual)
    }

    @Test
    fun setUsername() {
        val username = "Ayub"
        sharedPref.saveUsername(username)
        val actual = sharedPref.getUsername()
        TestCase.assertEquals(username, actual)
    }

    @Test
    fun clearData() {
        val setAccessToken = "1234567890"
        val setRefreshToken = "qwertyuiop"
        val username = "Ayub"

        sharedPref.saveAccessToken(setAccessToken)
        sharedPref.saveRefreshToken(setRefreshToken)
        sharedPref.saveUsername(username)

        sharedPref.clearData()

        val actualAccessToken = sharedPref.getAccessToken()
        val actualRefreshToken = sharedPref.getRefreshToken()
        val actualUsername = sharedPref.getUsername()

        TestCase.assertEquals(null, actualAccessToken)
        TestCase.assertEquals(null, actualRefreshToken)
        TestCase.assertEquals(null, actualUsername)
    }
}
