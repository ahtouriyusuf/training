package com.ayub.core.utils

import androidx.lifecycle.LiveData
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

class LiveDataTestUtil<T> {
    @Throws(InterruptedException::class)
    fun getValue(liveData: LiveData<T>): T? {
        var data: T? = null
        val latch = CountDownLatch(1)

        val observer = { value: T ->
            data = value
            latch.countDown()
        }

        liveData.observeForever(observer)

        latch.await(2, TimeUnit.SECONDS)

        liveData.removeObserver(observer)
        return data
    }
}
