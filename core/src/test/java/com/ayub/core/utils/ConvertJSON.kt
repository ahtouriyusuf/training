package com.ayub.core.utils

import okhttp3.mockwebserver.MockResponse
import java.io.InputStreamReader

object ConvertJSON {
    fun readStringFromFile(fileName: String): String {
        val inputStream = javaClass.classLoader?.getResourceAsStream(fileName)
        val input = InputStreamReader(inputStream).readText()

        return input
    }

    fun createMockResponse(fileName: String) = MockResponse()
        .setResponseCode(200)
        .setBody(readStringFromFile(fileName))
}
